const a = /* @__PURE__ */ location.pathname.split("/").slice(0, -1).join("/"), i = [
  a + "/_app/immutable/assets/track-and-trust-logo.1bbcf6bd.png",
  a + "/_app/immutable/assets/package.eac420a0.svg",
  a + "/_app/immutable/assets/add-package.c414e7aa.svg",
  a + "/_app/immutable/assets/status.96caf68a.svg",
  a + "/_app/immutable/assets/success.c3801f8f.svg",
  a + "/_app/immutable/assets/chat_icon.467be9d7.svg",
  a + "/_app/immutable/assets/pick_up_icon.83e63501.svg",
  a + "/_app/immutable/assets/drop_off_icon.423293f7.svg",
  a + "/_app/immutable/assets/edit-logo.466e6312.svg",
  a + "/_app/immutable/assets/success-logo.58d531f8.svg",
  a + "/_app/immutable/assets/user_icon.8ba0ffd6.svg",
  a + "/_app/immutable/assets/back-arrow.f8d21c48.svg",
  a + "/_app/immutable/assets/hamburger-menu.4fddd363.svg",
  a + "/_app/immutable/assets/fira-mono-cyrillic-ext-400-normal.3df7909e.woff2",
  a + "/_app/immutable/assets/fira-mono-cyrillic-400-normal.c7d433fd.woff2",
  a + "/_app/immutable/assets/fira-mono-greek-ext-400-normal.9e2fe623.woff2",
  a + "/_app/immutable/assets/fira-mono-greek-400-normal.a8be01ce.woff2",
  a + "/_app/immutable/assets/fira-mono-latin-ext-400-normal.6bfabd30.woff2",
  a + "/_app/immutable/assets/fira-mono-latin-400-normal.e43b3538.woff2",
  a + "/_app/immutable/assets/fira-mono-all-400-normal.1e3b098c.woff",
  a + "/_app/immutable/assets/_page.66dc7161.css",
  a + "/_app/immutable/assets/_page.d11b46e8.css",
  a + "/_app/immutable/assets/SubmitButton.222ab591.css",
  a + "/_app/immutable/assets/_page.b56d638e.css",
  a + "/_app/immutable/assets/_page.07feb29f.css",
  a + "/_app/immutable/assets/_page.cf55ca49.css",
  a + "/_app/immutable/assets/_page.1cbe119a.css",
  a + "/_app/immutable/assets/_page.b706d7da.css",
  a + "/_app/immutable/assets/_page.9ec167bd.css",
  a + "/_app/immutable/assets/_page.00e4a96d.css",
  a + "/_app/immutable/assets/_page.bcbe6248.css",
  a + "/_app/immutable/assets/_page.a58373ae.css",
  a + "/_app/immutable/assets/NextButton.26db99c0.css",
  a + "/_app/immutable/assets/_page.79a96834.css",
  a + "/_app/immutable/assets/_page.f9632948.css",
  a + "/_app/immutable/assets/_page.52c4ee66.css",
  a + "/_app/immutable/assets/UserImage.82847e4e.css",
  a + "/_app/immutable/assets/_layout.a23cca4e.css",
  a + "/_app/immutable/assets/_page.b0bd929f.css",
  a + "/_app/immutable/assets/BackButton.96dea476.css",
  a + "/_app/immutable/assets/_page.517a04bf.css",
  a + "/_app/immutable/chunks/singletons.97871f98.js",
  a + "/_app/immutable/chunks/index.3160bb21.js",
  a + "/_app/immutable/entry/start.8cd01641.js",
  a + "/_app/immutable/chunks/control.e7f5239e.js",
  a + "/_app/immutable/chunks/stores.add1b93d.js",
  a + "/_app/immutable/chunks/AppConstants.860e6e75.js",
  a + "/_app/immutable/entry/driver-organization-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/driver-phone-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/chunks/index.8d2fc517.js",
  a + "/_app/immutable/entry/container-registration-layout.svelte.796b3d24.js",
  a + "/_app/immutable/entry/driver-role-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/faq-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/driver-name-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/scan-shipment-qr-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/install-pwa-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/send-unit-status-_slug_-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/success-_slug_-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/chunks/user_icon.4a6feae7.js",
  a + "/_app/immutable/entry/user-profile-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/chunks/paths.4b784c92.js",
  a + "/_app/immutable/entry/scan-qr-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/unit-status-registration-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/unit-type-registration-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/finish-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/app.b427dd05.js",
  a + "/_app/immutable/entry/send-user-status-_slug_-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/entry/error.svelte.c20d8e77.js",
  a + "/_app/immutable/entry/scan-shipment-layout.svelte.09e0c40a.js",
  a + "/_app/immutable/chunks/20.e74d94f0.js",
  a + "/_app/immutable/chunks/21.2915a52a.js",
  a + "/_app/immutable/chunks/22.a0fe228c.js",
  a + "/_app/immutable/chunks/24.eace205b.js",
  a + "/_app/immutable/chunks/25.eb103f43.js",
  a + "/_app/immutable/chunks/26.b8975a15.js",
  a + "/_app/immutable/chunks/0.80b968a2.js",
  a + "/_app/immutable/chunks/30.87af7728.js",
  a + "/_app/immutable/chunks/5.077ce146.js",
  a + "/_app/immutable/chunks/31.9cbfa45a.js",
  a + "/_app/immutable/chunks/33.fff6d775.js",
  a + "/_app/immutable/chunks/32.d3217a2e.js",
  a + "/_app/immutable/chunks/34.47312e57.js",
  a + "/_app/immutable/chunks/7.8c8274c6.js",
  a + "/_app/immutable/chunks/35.ca02e51c.js",
  a + "/_app/immutable/chunks/8.8631b390.js",
  a + "/_app/immutable/chunks/36.1862fcbf.js",
  a + "/_app/immutable/entry/send-unit-status-_slug_-page.ts.e4fedd6c.js",
  a + "/_app/immutable/chunks/11.3a848367.js",
  a + "/_app/immutable/entry/send-user-status-_slug_-page.ts.e4fedd6c.js",
  a + "/_app/immutable/entry/success-_slug_-page.ts.e4fedd6c.js",
  a + "/_app/immutable/chunks/12.bfa58ea5.js",
  a + "/_app/immutable/chunks/13.1f61e0fa.js",
  a + "/_app/immutable/chunks/14.977cd6e8.js",
  a + "/_app/immutable/chunks/15.dd04f067.js",
  a + "/_app/immutable/chunks/16.cc6fed4d.js",
  a + "/_app/immutable/chunks/17.cea9156c.js",
  a + "/_app/immutable/chunks/23.a5f35a96.js",
  a + "/_app/immutable/chunks/18.e92740b8.js",
  a + "/_app/immutable/chunks/19.d4ac2059.js",
  a + "/_app/immutable/chunks/qr_code_scanner.6788b988.js",
  a + "/_app/immutable/chunks/navigation.d3c3fd5d.js",
  a + "/_app/immutable/chunks/UserStateUpdate.08501057.js",
  a + "/_app/immutable/chunks/UnitStateUpdate.00a96735.js",
  a + "/_app/immutable/chunks/29.41072524.js",
  a + "/_app/immutable/chunks/add-package.e2ba556c.js",
  a + "/_app/immutable/chunks/UpdateUnit.fbde9fa7.js",
  a + "/_app/immutable/chunks/UpdateUser.2da33ba1.js",
  a + "/_app/immutable/chunks/ActionConstants.54d4e5ed.js",
  a + "/_app/immutable/entry/install-pwa-page.svelte.2763b119.js",
  a + "/_app/immutable/chunks/FormatDate.5b073640.js",
  a + "/_app/immutable/chunks/_page.f5db9d9d.js",
  a + "/_app/immutable/chunks/index.2defaa64.js",
  a + "/_app/immutable/chunks/_page.1336a878.js",
  a + "/_app/immutable/chunks/chat_icon.84832ee5.js",
  a + "/_app/immutable/chunks/_page.49b53e73.js",
  a + "/_app/immutable/chunks/6.fc222d72.js",
  a + "/_app/immutable/chunks/9.da509913.js",
  a + "/_app/immutable/chunks/10.a0e4b193.js",
  a + "/_app/immutable/chunks/28.2c36c2c8.js",
  a + "/_app/immutable/entry/_page.svelte.00b6c9a5.js",
  a + "/_app/immutable/entry/unit-status-registration-page.svelte.f3d7b0ff.js",
  a + "/_app/immutable/entry/finish-page.svelte.1dcd5339.js",
  a + "/_app/immutable/chunks/27.c788ea5c.js",
  a + "/_app/immutable/entry/scan-qr-page.svelte.5de77220.js",
  a + "/_app/immutable/entry/success-_slug_-page.svelte.8c8f37fc.js",
  a + "/_app/immutable/entry/scan-shipment-page.svelte.a9951bd2.js",
  a + "/_app/immutable/entry/scan-shipment-qr-page.svelte.8b326191.js",
  a + "/_app/immutable/entry/send-unit-status-_slug_-page.svelte.131b6acb.js",
  a + "/_app/immutable/chunks/2.a6fdc13a.js",
  a + "/_app/immutable/entry/send-user-status-_slug_-page.svelte.97c8ef6f.js",
  a + "/_app/immutable/entry/driver-name-page.svelte.34852f5f.js",
  a + "/_app/immutable/entry/driver-organization-page.svelte.5f09d06d.js",
  a + "/_app/immutable/entry/driver-phone-page.svelte.2e566f42.js",
  a + "/_app/immutable/chunks/UserImage.82afd673.js",
  a + "/_app/immutable/entry/_layout.svelte.6ddcfeac.js",
  a + "/_app/immutable/entry/container-registration-page.svelte.87a51e85.js",
  a + "/_app/immutable/chunks/BackButton.989eef7f.js",
  a + "/_app/immutable/entry/driver-role-page.svelte.31802ca9.js",
  a + "/_app/immutable/chunks/SubmitButton.ce2586f8.js",
  a + "/_app/immutable/entry/faq-page.svelte.94aef09d.js",
  a + "/_app/immutable/chunks/NextButton.9d57f276.js",
  a + "/_app/immutable/chunks/3.7d5806e4.js",
  a + "/_app/immutable/entry/unit-type-registration-page.svelte.33130527.js",
  a + "/_app/immutable/entry/user-profile-page.svelte.e8fb38ac.js",
  a + "/_app/immutable/chunks/1.b6e7696b.js",
  a + "/_app/immutable/chunks/4.af6e9a8c.js",
  a + "/_app/immutable/chunks/fp.esm.0efa6759.js",
  a + "/_app/immutable/chunks/db.90b2e8bb.js",
  a + "/_app/immutable/chunks/html5-qrcode-scanner.059837ce.js"
], n = [
  a + "/android-chrome-192x192.png",
  a + "/android-chrome-512x512.png",
  a + "/favicon.png",
  a + "/manifest.webmanifest",
  a + "/robots.txt"
], u = "1683298424557", c = `cache${u}`, b = i.concat(n);
self.addEventListener("install", (e) => {
  console.log("The install function"), e.waitUntil(
    caches.open(c).then((s) => s.addAll(b)).then(() => {
      self.skipWaiting();
    })
  );
});
self.addEventListener("activate", (e) => {
  console.log("The activate function"), e.waitUntil(
    caches.keys().then(async (s) => {
      for (const t of s)
        t !== c && await caches.delete(t);
      await self.clients.claim();
    })
  );
});
async function _(e) {
  const s = await caches.open(`offline${u}`);
  try {
    const t = await fetch(e);
    return s.put(e, t.clone()), t;
  } catch (t) {
    const p = await s.match(e);
    if (p)
      return p;
    throw t;
  }
}
self.addEventListener("fetch", (e) => {
  if (e.request.method !== "GET" || e.request.headers.has("range"))
    return;
  const s = new URL(e.request.url), t = s.protocol.startsWith("http"), p = s.hostname === self.location.hostname && s.port !== self.location.port, m = s.host === self.location.host, l = e.request.cache === "only-if-cached" && !m;
  t && !p && !l && e.respondWith(
    (async () => m && await caches.match(e.request) || _(e.request))()
  );
});
