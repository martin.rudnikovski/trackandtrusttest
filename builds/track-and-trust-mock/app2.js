const express = require('express');
const cors = require('cors');
const https = require("https");

const app = express();

app.use(cors());

app.use(express.json())

app.get('/capi/v1/node/node_info', function (req, res) {
    const responseObject = {
        mesh_id: "606c269a-fcdc-4376-b93d-54fa3beeed94",
        uptime: "0:00:27:12",
        services: [
            "ue",
            "battery",
            "wmq"
        ]
    }
    res.json(responseObject);
});

app.get('/capi/v1/battery/get_life', function (req, res) {
    const responseObject = {
        "value": 58.3,
        "denomination": "percent"
    }
    res.json(responseObject);
});
app.get('/capi/v1/wmq/receive', function (req, res) {
    const responseObject = [
        {
            "meshId": "a6f92ae1-c8b2-41f9-8733-d3d5b1a0597e",
            "timestamp": "2023-03-01T20:00:00.129Z",
            "location": {
                "latitude": 42.014344,
                "longitude": 21.4465818
            },
            "actionType": "UNIT_ACTION",
            "action": "UNIT_SCANNED",
            "dataset": {
                "type": "Buffer",
                "data": [
                    120,
                    218,
                    171,
                    86,
                    42,
                    205,
                    203,
                    44,
                    241,
                    76,
                    81,
                    178,
                    82,
                    74,
                    73,
                    77,
                    180,
                    52,
                    48,
                    77,
                    53,
                    212,
                    77,
                    76,
                    181,
                    52,
                    209,
                    53,
                    49,
                    54,
                    72,
                    213,
                    77,
                    52,
                    49,
                    48,
                    213,
                    53,
                    74,
                    50,
                    178,
                    52,
                    53,
                    74,
                    77,
                    77,
                    54,
                    77,
                    49,
                    82,
                    210,
                    81,
                    42,
                    45,
                    78,
                    45,
                    2,
                    171,
                    7,
                    49,
                    140,
                    148,
                    106,
                    1,
                    124,
                    159,
                    19,
                    12
                ]
            },
            "id": "c22f5e210fae6d03eb9427d77f77c9fae820cca0ea7c2823dcc21482fb3a48da",
            "signature": "meh"
        }
    ]
    res.json(responseObject);
});


app.post('/capi/v1/wmq/send', async function (req, res) {
    const options = {
        host: 'track-and-trust.dev.opsmobix.net',
        path: '/api/action-event/encrypted/bulk',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    const req1 = https.request(options, (res) => {
        console.log(`Status Code: ${res.statusCode}`);
        // Handle response data
        res.on('data', (chunk) => {
            console.log(`Response Data: ${chunk}`);
        });
    });

    // Handle request errors
    req1.on('error', (error) => {
        console.error(`Error: ${error.message}`);
    });


    req1.write(JSON.stringify(req.body));
    req1.end();
    // await fetch('https://track-and-trust.dev.opsmobix.net/api/action-event/encrypted', {
    //     method: 'POST',
    //     body: JSON.stringify(req.body[0]),
    //     headers: {
    //         'content-type': 'application/json',
    //         accept: 'application/json',
    //     }
    // })

    const responseObject = {
        "status": "success",
        "data": "Message sent (ID: fcc4f108-e3ec-457c-9831-78ebe08bd4e2)"
    }
    res.json(responseObject); // Send response to client
});

app.listen(7019, function () {
    console.log('Server listening on port 7019');
});