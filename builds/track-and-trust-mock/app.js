const express = require('express');
const http = require("http");
const app = express();

/*
This is the action event (payload inside the messages) that needs to be sent to the main backend,
you will be receiving a group of such payloads from wmq's receive function,I'm aware that there are some limitations to the payload sizes,
so I'm guessing you would be sending them individually.
*/
const messagePayload = {
    event: {
        meshId: "3e528549-b9e0-4a28-82c6-842a821b2c19",
        timestamp: "2023-03-28T20:04:24.064Z",
        location: {
            "latitude": 42.0317703,
            "longitude": 21.4361242
        },
        actionType: "USER_ACTION",
        action: "USER_CREATED",
        dataset: {
            type: "Buffer",
            data: [
                120,
                218,
                5,
                193,
                203,
                17,
                128,
                48,
                8,
                5,
                192,
                94,
                168,
                128,
                207,
                68,
                193,
                14,
                44,
                35,
                132,
                71,
                1,
                58,
                158,
                28,
                123,
                119,
                247,
                165,
                231,
                198,
                117,
                22,
                29,
                164,
                93,
                206,
                3,
                130,
                29,
                91,
                74,
                167,
                203,
                136,
                5,
                155,
                101,
                202,
                48,
                32,
                123,
                234,
                0,
                164,
                150,
                166,
                73,
                56,
                115,
                180,
                27,
                207,
                200,
                53,
                162,
                149,
                233,
                251,
                1,
                129,
                109,
                22,
                96
            ]
        },
        id: "c55206d029d6763078429aa368d3075194a4f5936e1666a783f00c1a45937529"
    },
    signature: "MEUCIQDtLYl8lPp5E867CS0+zkABW4zEFWuM7hyk9oR3fiNicgIgA3wRgZJXRiS4OhePQTbvEi9BNRVwdo1Up1zKAkT8Ee0="
}

app.get('/', function (req, res) {
    const data = JSON.stringify(messagePayload);
    //I'm running the main backend locally.
    let responseData
    const options = {
        host: 'localhost',
        port: 3001,
        path: '/api/action-event/encrypted',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    const httpRequestToMainBackend = http.request(options, function (response) {
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
            responseData = chunk
        });
        /*The response here (if the request is successfully stored on the backend) would be the following,
        we return an array regardless the number of elements that you are sending,
         body: [{
		"id": "c55206d029d6763078429aa368d3075194a4f5936e1666a783f00c1a45937529",
		"actionType": "USER_ACTION",
		"timestamp": "2023-03-29T19:45:04.164Z",
		"action": "DELETE_ACTION_EVENT"
		}]
        */
        response.on('end', function () {
            const wmqRequest = JSON.parse(responseData)[0]

            const options2 = {
                host: 'localhost',
                port: 7019,
                path: '/capi/v1/wmq/send',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                }
            };

            const wmqServiceRequest = http.request(options2, function (wmqResponse) {
                //You have all of this documented in the cell-api doc, you'd just be sending a post request to the cell service.
                //https://github.com/weaver-lab/esa-integration/blob/weaver-labs/cell/docs/cell-api.adoc
                wmqResponse.setEncoding('utf8');
                wmqResponse.on('data', function (chunk2) {
                    console.log('body: ' + chunk2);
                });
                wmqResponse.on('end', function () {
                    res.send('ok');
                });
            });

            const wmqStringifiedRequest = JSON.stringify(wmqRequest);
            wmqServiceRequest.write(wmqStringifiedRequest);
            wmqServiceRequest.end();

            res.send('ok');
        })
    });

    httpRequestToMainBackend.write(data);
    httpRequestToMainBackend.end();
});

app.listen(8090, () => {
    console.log('Listening to port 8090')
});
