const express = require('express');
const http = require('http');

// Create Express app
const app = express();
const port = 7020; // Choose the desired port number

// Define route for handling POST request
app.post('/send-data', (req, res) => {
    // Data to send in the request body
    const postData = JSON.stringify({
        'data': 'this is the data to send'
    });

    const tempData =  {'data': 'this is the data to send'}
    const stringifiedTempData = JSON.stringify(tempData)
    // Options for the HTTP request
    const options = {
        host: 'localhost',
        port: 7019,
        path: '/capi/v1/wmq/send',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'User-Agent': 'cell-mesh-alpha',
            'CELL-API-SIGN': 'sig',
            'Content-Length': postData.length
        }
    };

    // Send HTTP request
    const req1 = http.request(options, (res) => {
        console.log(`Status Code: ${res.statusCode}`);
        // Handle response data
        res.on('data', (chunk) => {
            console.log(`Response Data: ${chunk}`);
        });
    });

    // Handle request errors
    req1.on('error', (error) => {
        console.error(`Error: ${error.message}`);
    });

    // Write request data to the request body and end the request
    // console.log(typeof postData === 'string')
    req1.write(stringifiedTempData);
    req1.end();

    res.send('POST request sent'); // Send response to client
});

// Start the Express app
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});