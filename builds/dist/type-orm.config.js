"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("typeorm");
const _1668594831968_InitUser_1 = require("./migrations/1668594831968-InitUser");
const _1668702116242_InitEvents_1 = require("./migrations/1668702116242-InitEvents");
const _1670422408427_InitPhoto_1 = require("./migrations/1670422408427-InitPhoto");
const _1670331688939_InitUnit_1 = require("./migrations/1670331688939-InitUnit");
const _1676923915818_initConfiguration_1 = require("./migrations/1676923915818-initConfiguration");
const _1678363836440_initDeletionActionEvent_1 = require("./migrations/1678363836440-initDeletionActionEvent");
const _1678457677012_initMemoryAvailableEvent_1 = require("./migrations/1678457677012-initMemoryAvailableEvent");
(0, dotenv_1.config)();
const configService = new config_1.ConfigService();
exports.default = new typeorm_1.DataSource({
    type: 'better-sqlite3',
    database: configService.get('DATABASE'),
    entities: [],
    migrations: [
        _1668594831968_InitUser_1.InitUser1668594831968,
        _1668702116242_InitEvents_1.InitEvents1668702116242,
        _1670331688939_InitUnit_1.InitUnit1670331688939,
        _1670422408427_InitPhoto_1.InitPhoto1670422408427,
        _1676923915818_initConfiguration_1.initConfiguration1676923915818,
        _1678363836440_initDeletionActionEvent_1.initDeletionActionEvent1678363836440,
        _1678457677012_initMemoryAvailableEvent_1.initMemoryAvailableEvent1678457677012
    ],
});
//# sourceMappingURL=type-orm.config.js.map