import { HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
import { ConfigurationSaveService } from "../../configuration/services/configuration-save.service";
import { ConfigurationFetchService } from "../../configuration/services/configuration-fetch.service";
import { ConfigurationUpdateService } from "../../configuration/services/configuration-update.service";
export declare class KeyManagementService {
    private configurationSaveService;
    private fetchKeyService;
    private httpService;
    private configService;
    private updateConfigurationService;
    private readonly logger;
    constructor(configurationSaveService: ConfigurationSaveService, fetchKeyService: ConfigurationFetchService, httpService: HttpService, configService: ConfigService, updateConfigurationService: ConfigurationUpdateService);
    exchangePublicKeys: () => Promise<void>;
}
