"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateKeys = void 0;
const crypto_1 = require("crypto");
function generateKeys(passphrase) {
    const { privateKey, publicKey } = (0, crypto_1.generateKeyPairSync)('ec', {
        namedCurve: 'secp256k1',
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        },
    });
    return {
        publicKey: publicKey,
        privateKey: privateKey
    };
}
exports.generateKeys = generateKeys;
//# sourceMappingURL=generate-keys.js.map