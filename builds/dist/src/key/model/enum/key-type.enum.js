"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeyTypeEnum = void 0;
var KeyTypeEnum;
(function (KeyTypeEnum) {
    KeyTypeEnum["PublicKey"] = "PUBLIC_KEY";
    KeyTypeEnum["PrivateKey"] = "PRIVATE_KEY";
})(KeyTypeEnum = exports.KeyTypeEnum || (exports.KeyTypeEnum = {}));
//# sourceMappingURL=key-type.enum.js.map