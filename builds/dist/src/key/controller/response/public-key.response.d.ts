export interface PublicKeyResponse {
    publicKey: string;
}
