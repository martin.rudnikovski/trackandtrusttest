import { UpdateUserService } from '../service/update-user.service';
import { CreateUserRequest } from '../domain/request/create-user-dto.request';
import { User } from '../domain/model/user.entity';
import { CreateUserService } from '../service/create-user.service';
import { UpdateUserRequest } from '../domain/request/update-user-dto.request';
export declare class UsersController {
    private readonly updateUserService;
    private readonly createUserService;
    constructor(updateUserService: UpdateUserService, createUserService: CreateUserService);
    createNewUser(createUserDto: CreateUserRequest): Promise<User>;
    updateUser(userId: string, updateUser: UpdateUserRequest): Promise<User>;
}
