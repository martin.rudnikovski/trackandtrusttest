import { Repository } from 'typeorm';
import { User } from '../domain/model/user.entity';
import { CreateUserRequest } from '../domain/request/create-user-dto.request';
import { I18nService } from 'nestjs-i18n';
import { Action } from '../../event/domain/actions/action.enum';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { CreateActionEventManagerService } from "../../event/service/action-event/create-action-event-manager.service";
export declare class CreateUserService implements Synchronizable<User> {
    private readonly createActionEventManagerService;
    private readonly language;
    private readonly userRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, language: I18nService, userRepository: Repository<User>);
    createUserId: (id: string, phoneNumber: string | undefined) => string;
    createUser(createUserDto: CreateUserRequest): Promise<User>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User>;
}
