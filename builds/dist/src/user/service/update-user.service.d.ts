import { Repository } from 'typeorm';
import { User } from '../domain/model/user.entity';
import { I18nService } from 'nestjs-i18n';
import { LocationRequest } from '../../shared/request/location.request';
import { Action } from '../../event/domain/actions/action.enum';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';
import { UpdateUserRequest } from "../domain/request/update-user-dto.request";
import { CreateActionEventManagerService } from "../../event/service/action-event/create-action-event-manager.service";
export declare class UpdateUserService implements Synchronizable<User> {
    private readonly createActionEventManagerService;
    private readonly language;
    private readonly userRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, language: I18nService, userRepository: Repository<User>);
    updateUser(id: string, meshId: string, location: LocationRequest, updateUserDto: UpdateUserRequest): Promise<User>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User>;
}
