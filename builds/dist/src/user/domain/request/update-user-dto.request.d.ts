import { CreateUserRequest } from './create-user-dto.request';
import { Action } from "../../../event/domain/actions/action.enum";
declare const UpdateUserRequest_base: import("@nestjs/common").Type<Omit<CreateUserRequest, "id">>;
export declare class UpdateUserRequest extends UpdateUserRequest_base {
    action?: Action;
    id: string;
    timeCreated: Date;
    timeUpdated: Date;
}
export {};
