import { BaseRequest } from '../../../shared/request/base.request';
export declare class CreateUserRequest extends BaseRequest {
    id: string;
    role?: string;
    organisation?: string;
    name?: string;
    phoneNumber?: string;
    editableFields(): string[];
}
