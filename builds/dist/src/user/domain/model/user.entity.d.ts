import { BaseAuditedEntity } from '../../../shared/model/base-audited-entity.class';
export declare class User extends BaseAuditedEntity<string> {
    role: string;
    organisation: string;
    name: string;
    phoneNumber: string;
    message: string;
    constructor(id: string, role: string, organisation: string, name: string, phoneNumber: string);
}
