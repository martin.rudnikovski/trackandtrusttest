"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserActionEventType = void 0;
var UserActionEventType;
(function (UserActionEventType) {
    UserActionEventType["PersonalInformationUpdated"] = "PERSONAL_INFORMATION_UPDATED";
    UserActionEventType["StatusUpdate"] = "USER_STATUS_UPDATE";
})(UserActionEventType = exports.UserActionEventType || (exports.UserActionEventType = {}));
//# sourceMappingURL=user-action-event-type.enum.js.map