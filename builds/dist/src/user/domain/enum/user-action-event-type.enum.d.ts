export declare enum UserActionEventType {
    PersonalInformationUpdated = "PERSONAL_INFORMATION_UPDATED",
    StatusUpdate = "USER_STATUS_UPDATE"
}
