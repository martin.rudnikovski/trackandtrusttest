import { GossipSynchronizationService } from "../service/gossip-synchronization.service";
export declare class GossipController {
    private readonly gossipSynchronizationService;
    constructor(gossipSynchronizationService: GossipSynchronizationService);
    initSynchronization(): Promise<any>;
}
