export declare class ConfigurationEntity {
    id: string;
    value: string;
    timeCreated: Date;
    timeUpdated: Date;
    constructor(id: string, value: string);
}
