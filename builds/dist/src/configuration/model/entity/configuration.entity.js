"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigurationEntity = void 0;
const typeorm_1 = require("typeorm");
let ConfigurationEntity = class ConfigurationEntity {
    constructor(id, value) {
        this.id = id;
        this.value = value;
    }
};
__decorate([
    (0, typeorm_1.PrimaryColumn)({
        name: 'id',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], ConfigurationEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'value',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], ConfigurationEntity.prototype, "value", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({
        type: 'datetime',
        name: 'time_created'
    }),
    __metadata("design:type", Date)
], ConfigurationEntity.prototype, "timeCreated", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({
        type: 'datetime',
        name: 'time_updated',
    }),
    __metadata("design:type", Date)
], ConfigurationEntity.prototype, "timeUpdated", void 0);
ConfigurationEntity = __decorate([
    (0, typeorm_1.Entity)({
        name: 'configuration',
    }),
    __metadata("design:paramtypes", [String, String])
], ConfigurationEntity);
exports.ConfigurationEntity = ConfigurationEntity;
//# sourceMappingURL=configuration.entity.js.map