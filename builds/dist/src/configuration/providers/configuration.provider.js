"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configurationProvider = void 0;
const generate_keys_1 = require("../../key/functions/generate-keys");
const process = require("process");
const file_service_1 = require("../services/file.service");
const configuration_save_service_1 = require("../services/configuration-save.service");
const configuration_entity_1 = require("../model/entity/configuration.entity");
const configuration_fetch_service_1 = require("../services/configuration-fetch.service");
const configuration_constants_1 = require("../../shared/constants/configuration.constants");
const uuid_1 = require("uuid");
const key_management_service_1 = require("../../key/service/key-management.service");
const encryption_service_1 = require("../../encryption/service/encryption.service");
const config_1 = require("@nestjs/config");
const application_constants_1 = require("../../shared/constants/application.constants");
exports.configurationProvider = {
    provide: 'KEYS',
    inject: [
        file_service_1.FileService,
        configuration_save_service_1.ConfigurationSaveService,
        configuration_fetch_service_1.ConfigurationFetchService,
        key_management_service_1.KeyManagementService,
        encryption_service_1.EncryptionService,
        config_1.ConfigService
    ],
    useFactory: async (fileService, configurationSaveService, configurationFetchService, keyManagementService, encryptionService, configService) => {
        const keysDirectory = `${process.env.HOME}/.mesh_node/keys`;
        await registerMeshNodeId(configurationFetchService, configurationSaveService);
        const privateKeyExists = fileService.readKeysFromFile(keysDirectory);
        if (privateKeyExists === '') {
            const passphrase = configService.get(application_constants_1.ApplicationConstants.PASS_PHRASE);
            const keys = (0, generate_keys_1.generateKeys)(passphrase);
            const encryptedPrivateKey = encryptionService.encrypt(keys.privateKey);
            fileService.writeKeysToFile(keysDirectory, encryptedPrivateKey);
            await configurationSaveService.saveConfigurationEntity(new configuration_entity_1.ConfigurationEntity(configuration_constants_1.ConfigurationConstants.MESH_NODE_PUBLIC_KEY, keys.publicKey));
        }
        await keyManagementService.exchangePublicKeys();
    },
};
const registerMeshNodeId = async (fetchConfigurationService, saveConfigurationService) => {
    const meshNodeId = configuration_constants_1.ConfigurationConstants.MESH_NODE_ID;
    const meshNodeIdExists = await fetchConfigurationService.findConfigurationEntityById(meshNodeId);
    if (!meshNodeIdExists)
        await saveConfigurationService.saveConfigurationEntity(new configuration_entity_1.ConfigurationEntity(meshNodeId, (0, uuid_1.v4)()));
    return meshNodeIdExists !== undefined;
};
//# sourceMappingURL=configuration.provider.js.map