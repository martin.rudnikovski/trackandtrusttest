export declare class FileService {
    private readonly logger;
    writeKeysToFile(path: string, data: string): boolean;
    private writeToFile;
    readKeysFromFile(path: string): string;
    private readFromFile;
}
