import { ConfigurationEntity } from "../model/entity/configuration.entity";
import { Repository } from "typeorm";
export declare class ConfigurationFetchService {
    private readonly configurationEntityRepository;
    constructor(configurationEntityRepository: Repository<ConfigurationEntity>);
    findConfigurationEntityById: (id: string) => Promise<ConfigurationEntity | null>;
}
