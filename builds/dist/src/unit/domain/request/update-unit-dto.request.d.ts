import { CreateUnitDtoRequest } from './create-unit-dto.request';
import { Action } from '../../../event/domain/actions/action.enum';
import { UnitType } from '../enum/unit.type.enum';
declare const UpdateUnitDtoRequest_base: import("@nestjs/common").Type<Omit<CreateUnitDtoRequest, "id">>;
export declare class UpdateUnitDtoRequest extends UpdateUnitDtoRequest_base {
    action?: Action;
    message?: string;
    sealNumber?: string;
    unitType?: UnitType;
}
export {};
