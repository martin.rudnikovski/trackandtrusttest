"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Unit = void 0;
const typeorm_1 = require("typeorm");
const base_audited_entity_class_1 = require("../../../shared/model/base-audited-entity.class");
const unit_type_enum_1 = require("../enum/unit.type.enum");
let Unit = class Unit extends base_audited_entity_class_1.BaseAuditedEntity {
    constructor(id, userID, meshId, unitType, sealNumber) {
        super(id);
        this.userId = userID;
        this.meshId = meshId;
        this.unitType = unitType;
        this.sealNumber = sealNumber;
    }
};
__decorate([
    (0, typeorm_1.Column)({
        name: 'user_id',
    }),
    __metadata("design:type", String)
], Unit.prototype, "userId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'mesh_id',
    }),
    __metadata("design:type", String)
], Unit.prototype, "meshId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'unit_type'
    }),
    __metadata("design:type", String)
], Unit.prototype, "unitType", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Unit.prototype, "message", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'seal_number'
    }),
    __metadata("design:type", String)
], Unit.prototype, "sealNumber", void 0);
Unit = __decorate([
    (0, typeorm_1.Entity)('units'),
    __metadata("design:paramtypes", [String, String, String, String, String])
], Unit);
exports.Unit = Unit;
//# sourceMappingURL=unit.entity.js.map