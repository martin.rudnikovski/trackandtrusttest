import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Action } from '../../event/domain/actions/action.enum';
import { CreateUnitService } from "./create-unit.service";
import { Unit } from "../domain/model/unit.entity";
import { UpdateUnitService } from "./update-unit.service";
export declare class UnitSynchronizationService implements Synchronizable<Unit> {
    private readonly createUnitService;
    private readonly updateUnitService;
    constructor(createUnitService: CreateUnitService, updateUnitService: UpdateUnitService);
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Unit>;
}
