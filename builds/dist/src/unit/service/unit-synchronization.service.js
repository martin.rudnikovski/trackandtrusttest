"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitSynchronizationService = void 0;
const common_1 = require("@nestjs/common");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const create_unit_service_1 = require("./create-unit.service");
const update_unit_service_1 = require("./update-unit.service");
const invalid_unit_action_error_1 = require("../domain/error/invalid-unit-action.error");
let UnitSynchronizationService = class UnitSynchronizationService {
    constructor(createUnitService, updateUnitService) {
        this.createUnitService = createUnitService;
        this.updateUnitService = updateUnitService;
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        switch (action) {
            case action_enum_1.Action.UnitScanned: {
                return await this.createUnitService.synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature);
            }
            case action_enum_1.Action.UnitDefined:
            case action_enum_1.Action.UnitPickedUp:
            case action_enum_1.Action.UnitDroppedOff:
            case action_enum_1.Action.UnitPickedUpMessage:
            case action_enum_1.Action.UnitStatusUpdate:
            case action_enum_1.Action.UnitMessageUpdate:
                return await this.updateUnitService.synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature);
            default:
                throw new invalid_unit_action_error_1.InvalidUnitActionError(`Invalid unit action specified for: meshID: ${meshId}, Action: ${action}`);
        }
    }
};
UnitSynchronizationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [create_unit_service_1.CreateUnitService,
        update_unit_service_1.UpdateUnitService])
], UnitSynchronizationService);
exports.UnitSynchronizationService = UnitSynchronizationService;
//# sourceMappingURL=unit-synchronization.service.js.map