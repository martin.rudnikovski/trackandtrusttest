import { Repository } from 'typeorm';
import { I18nService } from 'nestjs-i18n';
import { Action } from '../../event/domain/actions/action.enum';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { CreateActionEventManagerService } from '../../event/service/action-event/create-action-event-manager.service';
import { Unit } from "../domain/model/unit.entity";
import { CreateUnitDtoRequest } from "../domain/request/create-unit-dto.request";
export declare class CreateUnitService implements Synchronizable<Unit> {
    private readonly createActionEventManagerService;
    private readonly language;
    private readonly unitRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, language: I18nService, unitRepository: Repository<Unit>);
    createUnit(createUnitDto: CreateUnitDtoRequest): Promise<Unit>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Unit>;
}
