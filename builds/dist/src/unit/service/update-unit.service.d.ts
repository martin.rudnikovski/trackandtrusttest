import { Synchronizable } from "../../shared/interface/synchronizable.interface";
import { Unit } from "../domain/model/unit.entity";
import { CreateActionEventManagerService } from "../../event/service/action-event/create-action-event-manager.service";
import { I18nService } from "nestjs-i18n";
import { Repository } from "typeorm";
import { UpdateUnitDtoRequest } from "../domain/request/update-unit-dto.request";
import { Action } from '../../event/domain/actions/action.enum';
import { Dataset } from "../../event/domain/model/dataset.model";
export declare class UpdateUnitService implements Synchronizable<Unit> {
    private readonly createActionEventManagerService;
    private readonly language;
    private readonly unitRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, language: I18nService, unitRepository: Repository<Unit>);
    updateUnit(unitId: string, updateUnitDto: UpdateUnitDtoRequest): Promise<Unit>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Unit>;
}
