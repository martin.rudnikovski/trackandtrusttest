"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitModule = void 0;
const common_1 = require("@nestjs/common");
const event_module_1 = require("../event/event.module");
const typeorm_1 = require("@nestjs/typeorm");
const create_unit_service_1 = require("./service/create-unit.service");
const unit_entity_1 = require("./domain/model/unit.entity");
const unit_controller_1 = require("./controller/unit.controller");
const unit_synchronization_service_1 = require("./service/unit-synchronization.service");
const update_unit_service_1 = require("./service/update-unit.service");
const services = [
    create_unit_service_1.CreateUnitService,
    update_unit_service_1.UpdateUnitService,
    unit_synchronization_service_1.UnitSynchronizationService
];
let UnitModule = class UnitModule {
};
UnitModule = __decorate([
    (0, common_1.Module)({
        imports: [
            event_module_1.EventModule,
            typeorm_1.TypeOrmModule.forFeature([
                unit_entity_1.Unit,
            ]),
        ],
        providers: [
            ...services,
        ],
        controllers: [
            unit_controller_1.UnitController,
        ],
        exports: [
            unit_synchronization_service_1.UnitSynchronizationService,
        ],
    })
], UnitModule);
exports.UnitModule = UnitModule;
//# sourceMappingURL=unit.module.js.map