import { CreatePhotoRequest } from './request/create-photo.request';
import { Photo } from '../domain/model/photo.entity';
import { CreatePhotoService } from '../service/create-photo.service';
export declare class PhotoController {
    private readonly createPhotoService;
    constructor(createPhotoService: CreatePhotoService);
    createNewPhoto(createPhotoRequest: CreatePhotoRequest): Promise<Photo>;
}
