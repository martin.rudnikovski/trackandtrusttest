"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhotoController = void 0;
const common_1 = require("@nestjs/common");
const create_photo_request_1 = require("./request/create-photo.request");
const create_photo_service_1 = require("../service/create-photo.service");
let PhotoController = class PhotoController {
    constructor(createPhotoService) {
        this.createPhotoService = createPhotoService;
    }
    createNewPhoto(createPhotoRequest) {
        return this.createPhotoService.createPhoto({
            photo: createPhotoRequest.photo,
            userId: createPhotoRequest.userId,
            unitId: createPhotoRequest.unitId,
            message: createPhotoRequest.message,
            meshId: createPhotoRequest.meshId,
            latitude: createPhotoRequest.location.latitude,
            longitude: createPhotoRequest.location.longitude,
        });
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_photo_request_1.CreatePhotoRequest]),
    __metadata("design:returntype", Promise)
], PhotoController.prototype, "createNewPhoto", null);
PhotoController = __decorate([
    (0, common_1.Controller)('/photo'),
    (0, common_1.UseInterceptors)(common_1.ClassSerializerInterceptor),
    (0, common_1.UsePipes)(new common_1.ValidationPipe({ transform: true })),
    __metadata("design:paramtypes", [create_photo_service_1.CreatePhotoService])
], PhotoController);
exports.PhotoController = PhotoController;
//# sourceMappingURL=photo.controller.js.map