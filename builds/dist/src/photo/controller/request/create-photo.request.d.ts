/// <reference types="node" />
import { BaseRequest } from '../../../shared/request/base.request';
export declare class CreatePhotoRequest extends BaseRequest {
    userId: string;
    photo: Buffer;
    unitId?: string;
    message?: string;
}
