"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhotoModule = void 0;
const common_1 = require("@nestjs/common");
const create_photo_service_1 = require("./service/create-photo.service");
const photo_controller_1 = require("./controller/photo.controller");
const event_module_1 = require("../event/event.module");
const typeorm_1 = require("@nestjs/typeorm");
const photo_entity_1 = require("./domain/model/photo.entity");
const photo_synchronization_service_1 = require("./service/photo-synchronization.service");
let PhotoModule = class PhotoModule {
};
PhotoModule = __decorate([
    (0, common_1.Module)({
        imports: [event_module_1.EventModule, typeorm_1.TypeOrmModule.forFeature([photo_entity_1.Photo])],
        controllers: [photo_controller_1.PhotoController],
        providers: [create_photo_service_1.CreatePhotoService, photo_synchronization_service_1.PhotoSynchronizationService],
        exports: [photo_synchronization_service_1.PhotoSynchronizationService],
    })
], PhotoModule);
exports.PhotoModule = PhotoModule;
//# sourceMappingURL=photo.module.js.map