"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Photo = void 0;
const typeorm_1 = require("typeorm");
const base_audited_entity_class_1 = require("../../../shared/model/base-audited-entity.class");
const uuid_1 = require("uuid");
let Photo = class Photo extends base_audited_entity_class_1.BaseAuditedEntity {
    constructor(photo, userId, unitId, message, id = (0, uuid_1.v4)()) {
        super(id);
        this.userId = userId;
        this.photo = photo;
        this.unitId = unitId;
        this.message = message;
    }
};
__decorate([
    (0, typeorm_1.Column)('blob'),
    __metadata("design:type", Buffer)
], Photo.prototype, "photo", void 0);
__decorate([
    (0, typeorm_1.Column)({
        name: 'user_id',
    }),
    __metadata("design:type", String)
], Photo.prototype, "userId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        nullable: true,
        name: 'unit_id',
    }),
    __metadata("design:type", String)
], Photo.prototype, "unitId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        nullable: true,
    }),
    __metadata("design:type", String)
], Photo.prototype, "message", void 0);
Photo = __decorate([
    (0, typeorm_1.Entity)('photos'),
    __metadata("design:paramtypes", [Buffer, String, String, String, String])
], Photo);
exports.Photo = Photo;
//# sourceMappingURL=photo.entity.js.map