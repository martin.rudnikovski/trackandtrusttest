"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidPhotoActionError = void 0;
const common_1 = require("@nestjs/common");
class InvalidPhotoActionError extends common_1.BadRequestException {
    constructor(message = 'Invalid photo action send.') {
        super(message);
    }
}
exports.InvalidPhotoActionError = InvalidPhotoActionError;
//# sourceMappingURL=invalid-photo-action.error.js.map