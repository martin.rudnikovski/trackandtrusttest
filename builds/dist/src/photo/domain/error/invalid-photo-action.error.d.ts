import { BadRequestException } from '@nestjs/common';
export declare class InvalidPhotoActionError extends BadRequestException {
    constructor(message?: string);
}
