import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Action } from '../../event/domain/actions/action.enum';
import { CreatePhotoService } from './create-photo.service';
import { Photo } from '../domain/model/photo.entity';
export declare class PhotoSynchronizationService implements Synchronizable<Photo> {
    private readonly createPhotoService;
    constructor(createPhotoService: CreatePhotoService);
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Photo>;
}
