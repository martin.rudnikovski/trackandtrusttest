import { Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';
export declare class FetchMeshNodeStatusEventService {
    private readonly meshNodeStatusEventRepository;
    constructor(meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>);
    fetchMeshNodeStatusEvents(): Promise<MeshNodeStatusEvent[]>;
}
