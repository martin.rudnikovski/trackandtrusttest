import { Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';
export declare class SaveMeshNodeStatusEventService {
    private readonly meshNodeStatusEventRepository;
    private readonly logger;
    constructor(meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>);
    saveMeshNodeStatusEvent: (meshNodeStatusEvent: MeshNodeStatusEvent) => Promise<MeshNodeStatusEvent>;
    saveAllMeshNodeStatusEvents: (meshNodeStatusEvents: MeshNodeStatusEvent[]) => Promise<MeshNodeStatusEvent[]>;
}
