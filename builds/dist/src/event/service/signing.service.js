"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SigningService = void 0;
const common_1 = require("@nestjs/common");
const encryption_service_1 = require("../../encryption/service/encryption.service");
const file_service_1 = require("../../configuration/services/file.service");
const process = require("process");
let SigningService = class SigningService {
    constructor(encryptionService, fileService) {
        this.encryptionService = encryptionService;
        this.fileService = fileService;
    }
    sign(data) {
        const privateKeyPath = `${process.env.HOME}/.mesh_node/keys`;
        const encryptedPrivateKey = this.fileService.readKeysFromFile(privateKeyPath);
        const privateKey = this.encryptionService.decrypt(encryptedPrivateKey);
        return this.encryptionService.sign(data, privateKey);
    }
};
SigningService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [encryption_service_1.EncryptionService,
        file_service_1.FileService])
], SigningService);
exports.SigningService = SigningService;
//# sourceMappingURL=signing.service.js.map