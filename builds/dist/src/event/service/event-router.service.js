"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var EventRouterService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRouterService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const axios_1 = require("@nestjs/axios");
const deletion_event_mapper_service_1 = require("./deletion-event/deletion-event-mapper.service");
const deletion_event_manager_service_1 = require("./deletion-event/deletion-event-manager.service");
const wmq_service_1 = require("./wmq.service");
const application_constants_1 = require("../../shared/constants/application.constants");
let EventRouterService = EventRouterService_1 = class EventRouterService {
    constructor(configService, httpService, deletionMapper, deletionEventManagerService, wmqService) {
        this.configService = configService;
        this.httpService = httpService;
        this.deletionMapper = deletionMapper;
        this.deletionEventManagerService = deletionEventManagerService;
        this.wmqService = wmqService;
        this.logger = new common_1.Logger(EventRouterService_1.name);
    }
    postEvents(events) {
        const mainBackendUrl = this.configService.get(application_constants_1.ApplicationConstants.MAIN_SERVER_URL);
        this.httpService
            .post(`${mainBackendUrl}/api/action-event/encrypted`, events)
            .subscribe({
            next: (next) => {
                const deletionEvents = this.deletionMapper.mapDeletionEvent(next.data);
                this.deletionEventManagerService
                    .executeAndSaveDeletionEvents(deletionEvents)
                    .then(async (executedDeletionEvents) => await this.wmqService.sendEvents(executedDeletionEvents));
            },
            error: (error) => {
                var _a, _b, _c;
                this.logger.error((_c = (_b = (_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.message) !== null && _c !== void 0 ? _c : error);
            },
        });
    }
};
EventRouterService = EventRouterService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService,
        axios_1.HttpService,
        deletion_event_mapper_service_1.DeletionEventMapper,
        deletion_event_manager_service_1.DeletionEventManagerService,
        wmq_service_1.WmqService])
], EventRouterService);
exports.EventRouterService = EventRouterService;
//# sourceMappingURL=event-router.service.js.map