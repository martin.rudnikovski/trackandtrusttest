"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateActionEventManagerService = void 0;
const common_1 = require("@nestjs/common");
const fetch_deletion_event_service_1 = require("../deletion-event/fetch-deletion-event.service");
const wmq_service_1 = require("../wmq.service");
const signing_service_1 = require("../signing.service");
const save_action_event_service_1 = require("./save-action-event.service");
const action_event_mapper_service_1 = require("./action-event-mapper.service");
let CreateActionEventManagerService = class CreateActionEventManagerService {
    constructor(saveActionEventService, actionEventMapper, signingService, fetchDeletionEventService, wmqService) {
        this.saveActionEventService = saveActionEventService;
        this.actionEventMapper = actionEventMapper;
        this.signingService = signingService;
        this.fetchDeletionEventService = fetchDeletionEventService;
        this.wmqService = wmqService;
    }
    createActionEvent(payload) {
        const actionEvent = this.actionEventMapper.mapActionEvent(payload);
        return this.fetchDeletionEventService
            .fetchDeletionEventById(actionEvent.id)
            .then(async (it) => {
            var _a;
            if (it)
                return;
            actionEvent.signature =
                (_a = payload.signature) !== null && _a !== void 0 ? _a : this.signingService.sign(JSON.stringify(actionEvent));
            return this.saveActionEventService.saveActionEvent(actionEvent);
        });
    }
};
CreateActionEventManagerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [save_action_event_service_1.SaveActionEventService,
        action_event_mapper_service_1.ActionEventMapper,
        signing_service_1.SigningService,
        fetch_deletion_event_service_1.FetchDeletionEventService,
        wmq_service_1.WmqService])
], CreateActionEventManagerService);
exports.CreateActionEventManagerService = CreateActionEventManagerService;
//# sourceMappingURL=create-action-event-manager.service.js.map