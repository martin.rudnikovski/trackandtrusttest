import { ActionEvent } from '../../domain/entities/action-event.entity';
import { Repository } from 'typeorm';
export declare class SaveActionEventService {
    private readonly actionEventRepository;
    private readonly logger;
    constructor(actionEventRepository: Repository<ActionEvent>);
    saveActionEvent: (actionEvent: ActionEvent) => Promise<ActionEvent>;
}
