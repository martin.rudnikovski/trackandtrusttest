import { ConfigService } from '@nestjs/config';
import { EventRequest } from '../domain/dto/event.request';
import { HttpService } from '@nestjs/axios';
import { DeletionEventMapper } from './deletion-event/deletion-event-mapper.service';
import { DeletionEventManagerService } from './deletion-event/deletion-event-manager.service';
import { WmqService } from './wmq.service';
export declare class EventRouterService {
    private configService;
    private httpService;
    private deletionMapper;
    private deletionEventManagerService;
    private wmqService;
    private readonly logger;
    constructor(configService: ConfigService, httpService: HttpService, deletionMapper: DeletionEventMapper, deletionEventManagerService: DeletionEventManagerService, wmqService: WmqService);
    postEvents(events: EventRequest[]): void;
}
