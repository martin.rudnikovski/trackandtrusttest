import { FetchActionEventService } from './action-event/fetch-action-event.service';
import { FetchMeshNodeStatusEventService } from './mesh-node-status-event/fetch-mesh-node-status-event.service';
import { EventRouterService } from './event-router.service';
import { EventRequestMapper } from './event-request-mapper.service';
export declare class EventManagementService {
    private fetchActionEventService;
    private fetchMeshNodeStatusEventService;
    private eventRouterService;
    private eventRequestMapper;
    constructor(fetchActionEventService: FetchActionEventService, fetchMeshNodeStatusEventService: FetchMeshNodeStatusEventService, eventRouterService: EventRouterService, eventRequestMapper: EventRequestMapper);
    sendEventsToMainServer: () => Promise<void>;
    sendEventsToMainServerInBulk: () => Promise<void>;
}
