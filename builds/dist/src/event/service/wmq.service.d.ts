import { HttpService } from '@nestjs/axios';
import { NodeToNodeEvent } from '../domain/dto/event.dto';
export declare class WmqService {
    private readonly httpService;
    constructor(httpService: HttpService);
    fetchEvents(): Promise<NodeToNodeEvent[]>;
    sendEvents(events: NodeToNodeEvent[]): Promise<any>;
    sendEvent(event: NodeToNodeEvent): Promise<any>;
    getBatteryLife(): Promise<number>;
}
