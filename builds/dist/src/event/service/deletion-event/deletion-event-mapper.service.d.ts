import { CreateEventResponse } from '../../domain/payload/create-event.response';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
export declare class DeletionEventMapper {
    mapDeletionEvent(responses: CreateEventResponse[]): DeletionEvent[];
}
