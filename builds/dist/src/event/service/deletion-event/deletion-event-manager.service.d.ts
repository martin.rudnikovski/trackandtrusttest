import { RemoveMeshNodeStatusEventService } from '../mesh-node-status-event/remove-mesh-node-status-event.service';
import { RemoveActionEventService } from '../action-event/remove-action-event.service';
import { SaveDeletionEventService } from './save-deletion-event.service';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
export declare class DeletionEventManagerService {
    private readonly removeMeshNodeStatusEventService;
    private readonly removeActionEventService;
    private readonly saveDeletionEventService;
    constructor(removeMeshNodeStatusEventService: RemoveMeshNodeStatusEventService, removeActionEventService: RemoveActionEventService, saveDeletionEventService: SaveDeletionEventService);
    executeAndSaveDeletionEvent(event: DeletionEvent): Promise<DeletionEvent>;
    executeAndSaveDeletionEvents(events: DeletionEvent[]): Promise<DeletionEvent[]>;
}
