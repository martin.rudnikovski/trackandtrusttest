import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
import { Repository } from 'typeorm';
export declare class SaveDeletionEventService {
    private readonly deletionEventRepository;
    private readonly logger;
    constructor(deletionEventRepository: Repository<DeletionEvent>);
    saveDeletionEvent: (deletionEvent: DeletionEvent) => Promise<DeletionEvent>;
    saveAllDeletionEvents: (deletionEvents: DeletionEvent[]) => Promise<DeletionEvent[]>;
}
