"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeletionEventMapper = void 0;
const common_1 = require("@nestjs/common");
const deletion_event_entity_1 = require("../../domain/entities/deletion-event.entity");
const action_type_enum_1 = require("../../domain/actions/action-type.enum");
const action_enum_1 = require("../../domain/actions/action.enum");
let DeletionEventMapper = class DeletionEventMapper {
    mapDeletionEvent(responses) {
        return responses.map((response) => {
            const deletionEvent = new deletion_event_entity_1.DeletionEvent(response.id, new Date(), action_type_enum_1.ActionType.Delete);
            switch (response.actionType) {
                case action_type_enum_1.ActionType.MeshNodeStatus: {
                    deletionEvent.action = action_enum_1.Action.DeleteMeshNodeStatusEvent;
                    break;
                }
                case action_type_enum_1.ActionType.Unit:
                case action_type_enum_1.ActionType.User: {
                    deletionEvent.action = action_enum_1.Action.DeleteActionEvent;
                    break;
                }
            }
            return deletionEvent;
        });
    }
};
DeletionEventMapper = __decorate([
    (0, common_1.Injectable)()
], DeletionEventMapper);
exports.DeletionEventMapper = DeletionEventMapper;
//# sourceMappingURL=deletion-event-mapper.service.js.map