"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CleanUpStorageService = void 0;
const common_1 = require("@nestjs/common");
const remove_deletion_event_service_1 = require("../deletion-event/remove-deletion-event.service");
const event_emitter_1 = require("@nestjs/event-emitter");
const event_constants_1 = require("../../../shared/constants/event.constants");
const fetch_deletion_event_service_1 = require("../deletion-event/fetch-deletion-event.service");
let CleanUpStorageService = class CleanUpStorageService {
    constructor(removeDeletionEventService, fetchDeletionEventService) {
        this.removeDeletionEventService = removeDeletionEventService;
        this.fetchDeletionEventService = fetchDeletionEventService;
    }
    cleanUpStorage(diskSize) {
        this.fetchDeletionEventService
            .fetchDeletionEvents()
            .then(async (deletionEvents) => {
            const deletionEventsSize = JSON.stringify(deletionEvents).length;
            const twentyPercentage = diskSize / 0.2;
            const tenPercentage = diskSize / 0.1;
            if (deletionEventsSize > twentyPercentage) {
                const percentageDecrease = ((deletionEventsSize - tenPercentage) / deletionEventsSize) * 100;
                if (percentageDecrease > 0 && percentageDecrease < 100)
                    await this.removeDeletionEventService.removeLastNPercentageDeletionEvents(percentageDecrease);
            }
        });
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(event_constants_1.EventConstants.STORAGE_CLEAN_UP),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], CleanUpStorageService.prototype, "cleanUpStorage", null);
CleanUpStorageService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [remove_deletion_event_service_1.RemoveDeletionEventService,
        fetch_deletion_event_service_1.FetchDeletionEventService])
], CleanUpStorageService);
exports.CleanUpStorageService = CleanUpStorageService;
//# sourceMappingURL=clean-up-storage.service.js.map