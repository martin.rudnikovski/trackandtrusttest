"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventManagementService = void 0;
const common_1 = require("@nestjs/common");
const fetch_action_event_service_1 = require("./action-event/fetch-action-event.service");
const rxjs_1 = require("rxjs");
const fetch_mesh_node_status_event_service_1 = require("./mesh-node-status-event/fetch-mesh-node-status-event.service");
const event_router_service_1 = require("./event-router.service");
const event_request_mapper_service_1 = require("./event-request-mapper.service");
let EventManagementService = class EventManagementService {
    constructor(fetchActionEventService, fetchMeshNodeStatusEventService, eventRouterService, eventRequestMapper) {
        this.fetchActionEventService = fetchActionEventService;
        this.fetchMeshNodeStatusEventService = fetchMeshNodeStatusEventService;
        this.eventRouterService = eventRouterService;
        this.eventRequestMapper = eventRequestMapper;
        this.sendEventsToMainServer = async () => {
            (0, rxjs_1.forkJoin)([
                this.fetchActionEventService.fetchActionEvents(),
                this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
            ]).subscribe(([actionEvents, memoryEvents]) => {
                [...actionEvents, ...memoryEvents].forEach((it) => {
                    const eventRequest = this.eventRequestMapper.mapEventRequest(it);
                    this.eventRouterService.postEvents([eventRequest]);
                });
            });
        };
        this.sendEventsToMainServerInBulk = async () => {
            (0, rxjs_1.forkJoin)([
                this.fetchActionEventService.fetchActionEvents(),
                this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
            ]).subscribe(([actionEvents, memoryEvents]) => {
                const mappedActionEvents = this.eventRequestMapper.mapEventRequests(actionEvents);
                const mappedMemoryEvents = this.eventRequestMapper.mapEventRequests(memoryEvents);
                const combinedEvents = mappedActionEvents.concat(mappedMemoryEvents);
                this.eventRouterService.postEvents(combinedEvents);
            });
        };
    }
};
EventManagementService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [fetch_action_event_service_1.FetchActionEventService,
        fetch_mesh_node_status_event_service_1.FetchMeshNodeStatusEventService,
        event_router_service_1.EventRouterService,
        event_request_mapper_service_1.EventRequestMapper])
], EventManagementService);
exports.EventManagementService = EventManagementService;
//# sourceMappingURL=event-management.service.js.map