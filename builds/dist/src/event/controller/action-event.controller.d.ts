import { ActionEvent } from '../domain/entities/action-event.entity';
import { EventManagementService } from '../service/event-management.service';
import { FetchActionEventService } from '../service/action-event/fetch-action-event.service';
export declare class ActionEventController {
    private fetchActionEventService;
    private eventManagementService;
    constructor(fetchActionEventService: FetchActionEventService, eventManagementService: EventManagementService);
    getEventActions(): Promise<ActionEvent[]>;
    sendActionEvents(): Promise<void>;
    sendActionEventsInBulk(): Promise<void>;
}
