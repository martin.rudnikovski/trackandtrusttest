"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionEventController = void 0;
const common_1 = require("@nestjs/common");
const event_management_service_1 = require("../service/event-management.service");
const fetch_action_event_service_1 = require("../service/action-event/fetch-action-event.service");
let ActionEventController = class ActionEventController {
    constructor(fetchActionEventService, eventManagementService) {
        this.fetchActionEventService = fetchActionEventService;
        this.eventManagementService = eventManagementService;
    }
    getEventActions() {
        return this.fetchActionEventService.fetchActionEvents();
    }
    sendActionEvents() {
        return this.eventManagementService.sendEventsToMainServer();
    }
    sendActionEventsInBulk() {
        return this.eventManagementService.sendEventsToMainServerInBulk();
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ActionEventController.prototype, "getEventActions", null);
__decorate([
    (0, common_1.Post)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ActionEventController.prototype, "sendActionEvents", null);
__decorate([
    (0, common_1.Post)('/bulk'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ActionEventController.prototype, "sendActionEventsInBulk", null);
ActionEventController = __decorate([
    (0, common_1.Controller)('/api/action'),
    __metadata("design:paramtypes", [fetch_action_event_service_1.FetchActionEventService,
        event_management_service_1.EventManagementService])
], ActionEventController);
exports.ActionEventController = ActionEventController;
//# sourceMappingURL=action-event.controller.js.map