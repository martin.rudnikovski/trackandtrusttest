"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Action = void 0;
var Action;
(function (Action) {
    Action["MeshNodeAccess"] = "MESH_NODE_ACCESS";
    Action["UserCreated"] = "USER_CREATED";
    Action["UserInformationUpdated"] = "USER_INFORMATION_UPDATED";
    Action["UserStatusUpdate"] = "USER_STATUS_UPDATE";
    Action["UserMessageUpdate"] = "USER_MESSAGE_UPDATE";
    Action["UnitStatusUpdate"] = "UNIT_STATUS_UPDATE";
    Action["UnitScanned"] = "UNIT_SCANNED";
    Action["UnitDefined"] = "UNIT_DEFINED";
    Action["UnitPickedUp"] = "UNIT_PICKED_UP";
    Action["UnitDroppedOff"] = "UNIT_DROPPED_OFF";
    Action["UnitPickedUpMessage"] = "UNIT_PICKED_UP_MESSAGE";
    Action["UnitDroppedOffMessage"] = "UNIT_DROPPED_OFF_MESSAGE";
    Action["UnitMessageUpdate"] = "UNIT_MESSAGE_UPDATE";
    Action["UnitDefinedContainerSeal"] = "UNIT_DEFINED_CONTAINER_SEAL";
    Action["PhotoTaken"] = "PHOTO_TAKEN";
    Action["PhotoMessage"] = "PHOTO_MESSAGE";
    Action["PhotoUnitSet"] = "PHOTO_UNIT_SET";
    Action["DeleteActionEvent"] = "DELETE_ACTION_EVENT";
    Action["DeleteMeshNodeStatusEvent"] = "DELETE_MESH_NODE_STATUS_EVENT";
})(Action = exports.Action || (exports.Action = {}));
//# sourceMappingURL=action.enum.js.map