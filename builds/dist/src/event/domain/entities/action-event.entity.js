"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionEvent = void 0;
const typeorm_1 = require("typeorm");
const location_class_1 = require("../../../shared/model/location.class");
const action_type_enum_1 = require("../actions/action-type.enum");
const action_enum_1 = require("../actions/action.enum");
const sha256_map_function_1 = require("../../../shared/functions/sha256-map.function");
let ActionEvent = class ActionEvent {
    constructor(meshId, timestamp, location, actionType, action, dataset, signature) {
        this.meshId = meshId;
        this.timestamp = timestamp;
        this.location = location;
        this.actionType = actionType;
        this.action = action;
        this.dataset = dataset;
        this.id = this.generateId();
        if (signature)
            this.signature = signature;
    }
    generateId() {
        const object = {
            meshId: this.meshId,
            timestamp: this.timestamp,
            location: this.location,
            actionType: this.actionType,
            action: this.action,
            dataset: this.dataset,
        };
        return (0, sha256_map_function_1.sha256Hash)(object);
    }
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    __metadata("design:type", String)
], ActionEvent.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'mesh_id',
    }),
    __metadata("design:type", String)
], ActionEvent.prototype, "meshId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'datetime',
        name: 'timestamp',
    }),
    __metadata("design:type", Date)
], ActionEvent.prototype, "timestamp", void 0);
__decorate([
    (0, typeorm_1.Column)(() => location_class_1.GpsLocation, {
        prefix: false,
    }),
    __metadata("design:type", location_class_1.GpsLocation)
], ActionEvent.prototype, "location", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'action_type',
    }),
    __metadata("design:type", String)
], ActionEvent.prototype, "actionType", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'action',
    }),
    __metadata("design:type", String)
], ActionEvent.prototype, "action", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'blob',
        name: 'dataset',
    }),
    __metadata("design:type", Buffer)
], ActionEvent.prototype, "dataset", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'signature',
    }),
    __metadata("design:type", String)
], ActionEvent.prototype, "signature", void 0);
ActionEvent = __decorate([
    (0, typeorm_1.Entity)('action_events'),
    __metadata("design:paramtypes", [String, Date,
        location_class_1.GpsLocation, String, String, Buffer, String])
], ActionEvent);
exports.ActionEvent = ActionEvent;
//# sourceMappingURL=action-event.entity.js.map