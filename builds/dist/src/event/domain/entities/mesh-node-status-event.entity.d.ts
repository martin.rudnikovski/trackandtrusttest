import { ActionType } from '../actions/action-type.enum';
export declare class MeshNodeStatusEvent {
    id: string;
    timestamp: Date;
    actionType: ActionType;
    memory: number;
    battery: number;
    meshId: string;
    signature: string;
    constructor(timestamp: Date, actionType: ActionType, memory: number, battery: number, meshId: string, signature?: string);
}
