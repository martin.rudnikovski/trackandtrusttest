import { ActionType } from '../actions/action-type.enum';
import { Action } from '../actions/action.enum';
export declare class DeletionEvent {
    id: string;
    timestamp: Date;
    actionType: ActionType;
    action: Action;
    constructor(id: string, timestamp: Date, actionType: ActionType, action?: Action);
}
