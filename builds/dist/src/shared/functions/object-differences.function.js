"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectDifferences = void 0;
function objectDifferences(object1, object2) {
    const differenceObj = new Map();
    for (const key in object1) {
        if (Object.prototype.hasOwnProperty.call(object2, key)) {
            if (object1[key] !== object2[key]) {
                differenceObj.set(key, object1[key]);
            }
        }
    }
    return differenceObj;
}
exports.objectDifferences = objectDifferences;
//# sourceMappingURL=object-differences.function.js.map