export declare const ApplicationConstants: {
    readonly MAIN_SERVER_URL: "MAIN_SERVER_URL";
    readonly PASS_PHRASE: "PASS_PHRASE";
    readonly INIT_VECTOR: "INIT_VECTOR";
};
