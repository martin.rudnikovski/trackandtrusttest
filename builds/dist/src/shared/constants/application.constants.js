"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationConstants = void 0;
exports.ApplicationConstants = {
    MAIN_SERVER_URL: 'MAIN_SERVER_URL',
    PASS_PHRASE: 'PASS_PHRASE',
    INIT_VECTOR: 'INIT_VECTOR'
};
//# sourceMappingURL=application.constants.js.map