import { BaseRequest } from './base.request';
export declare class BaseUserRequest extends BaseRequest {
    userId: string;
}
