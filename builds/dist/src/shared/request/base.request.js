"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRequest = void 0;
const class_validator_1 = require("class-validator");
const location_request_1 = require("./location.request");
const class_transformer_1 = require("class-transformer");
class BaseRequest {
    editableFields() {
        return Object.getOwnPropertyNames(this)
            .filter(it => !['meshId', 'location'].includes(it));
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], BaseRequest.prototype, "meshId", void 0);
__decorate([
    (0, class_validator_1.IsDefined)(),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => location_request_1.LocationRequest),
    __metadata("design:type", location_request_1.LocationRequest)
], BaseRequest.prototype, "location", void 0);
exports.BaseRequest = BaseRequest;
//# sourceMappingURL=base.request.js.map