import { LocationRequest } from './location.request';
export declare abstract class BaseRequest {
    meshId: string;
    location: LocationRequest;
    editableFields(): string[];
}
