export declare abstract class Copier {
    copyWith(modifyObject: {
        [P in keyof this]?: this[P];
    }): this;
}
