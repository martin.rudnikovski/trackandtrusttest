import { Copier } from './copier.class';
export declare abstract class BaseEntity<ID> extends Copier {
    id: ID;
    timeCreated: Date;
    protected constructor(id: ID);
}
