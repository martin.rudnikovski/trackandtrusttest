"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InitUnit1670331688939 = void 0;
const node_sql_reader_1 = require("node-sql-reader");
const insertQuery = `
    create table unit_types
    (
        id text primary key
    );

    insert into unit_types(id)
    values ('PALLET'),
           ('CONTAINER'),
           ('OTHER');

    create table units
    (
        id           text primary key,
        user_id      text
            constraint units_user_id_fk
                references users
                on update cascade
                on delete restrict,
        mesh_id      text,
        unit_type    text
            constraint units_unit_type_id_fk
                references unit_types
                on update cascade
                on delete restrict,
        message      text,
        seal_number  text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`;
const removeQuery = `
    drop table unit_types;
    drop table units;
`;
class InitUnit1670331688939 {
    async up(queryRunner) {
        const queries = node_sql_reader_1.SqlReader.parseSqlString(insertQuery);
        for (const query of queries) {
            await queryRunner.query(query);
        }
    }
    async down(queryRunner) {
        return await queryRunner.query(removeQuery);
    }
}
exports.InitUnit1670331688939 = InitUnit1670331688939;
//# sourceMappingURL=1670331688939-InitUnit.js.map