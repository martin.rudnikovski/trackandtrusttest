"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initDeletionActionEvent1678363836440 = void 0;
const typeorm_1 = require("typeorm");
class initDeletionActionEvent1678363836440 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'deletion_events',
            columns: [
                {
                    name: 'id',
                    type: 'text',
                    isPrimary: true,
                },
                {
                    name: 'timestamp',
                    type: 'datetime',
                },
                {
                    name: 'action_type',
                    type: 'text',
                    foreignKeyConstraintName: 'deletion_action_event_action_type_fk',
                },
                {
                    name: 'action',
                    type: 'text',
                    foreignKeyConstraintName: 'deletion_action_event_action_fk',
                },
            ],
        }));
        await queryRunner.createForeignKey('deletion_events', new typeorm_1.TableForeignKey({
            columnNames: ['action_type'],
            referencedColumnNames: ['id'],
            referencedTableName: 'action_types',
            onUpdate: 'CASCADE',
        }));
        await queryRunner.createForeignKey('deletion_events', new typeorm_1.TableForeignKey({
            columnNames: ['action'],
            referencedColumnNames: ['id'],
            referencedTableName: 'actions',
            onUpdate: 'CASCADE',
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('deletion_events');
    }
}
exports.initDeletionActionEvent1678363836440 = initDeletionActionEvent1678363836440;
//# sourceMappingURL=1678363836440-initDeletionActionEvent.js.map