"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initMemoryAvailableEvent1678457677012 = void 0;
const typeorm_1 = require("typeorm");
class initMemoryAvailableEvent1678457677012 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'mesh_node_status_events',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    default: 'gen_random_uuid()',
                    isPrimary: true,
                },
                {
                    name: 'timestamp',
                    type: 'datetime',
                },
                {
                    name: 'action_type',
                    type: 'text',
                    foreignKeyConstraintName: 'mesh_node_status_event_action_type_fk',
                },
                {
                    name: 'memory',
                    type: 'integer',
                },
                {
                    name: 'battery',
                    type: 'float',
                },
                {
                    name: 'mesh_id',
                    type: 'text',
                },
                {
                    name: 'signature',
                    type: 'text',
                    isNullable: false,
                },
            ],
        }));
        await queryRunner.createForeignKey('mesh_node_status_events', new typeorm_1.TableForeignKey({
            columnNames: ['action_type'],
            referencedColumnNames: ['id'],
            referencedTableName: 'action_types',
            onUpdate: 'CASCADE',
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('mesh_node_status_events');
    }
}
exports.initMemoryAvailableEvent1678457677012 = initMemoryAvailableEvent1678457677012;
//# sourceMappingURL=1678457677012-initMemoryAvailableEvent.js.map