"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InitPhoto1670422408427 = void 0;
const node_sql_reader_1 = require("node-sql-reader");
const sqlQuery = `
    create table photos
    (
        id           text primary key,
        photo        blob                               not null,
        user_id      text                               not null
            constraint photos_user_id_fk
                references users (id)
                on update cascade
                on delete restrict,
        unit_id      text,
        message      text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`;
class InitPhoto1670422408427 {
    async up(queryRunner) {
        const queries = node_sql_reader_1.SqlReader.parseSqlString(sqlQuery);
        for (const query of queries) {
            await queryRunner.query(query);
        }
    }
    async down(queryRunner) {
    }
}
exports.InitPhoto1670422408427 = InitPhoto1670422408427;
//# sourceMappingURL=1670422408427-InitPhoto.js.map