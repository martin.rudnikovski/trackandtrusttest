import json
import urllib
import time
import requests

BASE_SCHEMA = "http"


class MeshClient():
    """ Client side API to cell mesh.

    This is a simple python implementation of cell-mesh node.
    """
    def __init__(self, ip: str, port: str, ep: str, k: str = "", p: str = ""):
        self._ip = ip
        self._port = port
        self._endpoint = ep
        self._api_key = k
        self._passphrase = p

    def _api_call(self, endpoint: str, method: str = None, params: dict = {}, req="GET"):
        """ Use to call remote service.
        """
        # params = urllib.parse.urlencode(params)
        headers = self.generate_headers()
        # url = self._url(endpoint, method, params)

        if req == "GET":
            params = urllib.parse.urlencode(params)
            url = self._url(endpoint, method, params)
            req = requests.get(url, headers=headers)
        elif req == "POST":
            url = self._url(endpoint, method)
            req = requests.post(url, headers=headers, json=json.dumps(params))
        return self._handle_response(req)

    def _handle_response(self, req: object):
        """ Response handler. """

        if str(req.status_code).startswith("200"):
            return req.json()
        else:
            # Handle errors
            # Handle them some more
            # Now return
            pass

        return req

    def generate_headers(self):
        headers = {'Content-Type'        : "application/json",
                   'User-Agent'          : "cell-mesh-alpha",
                   "CELL-API-SIGN"       : "sig",
                   "CELL-API-TIMESTAMP"  : self._timestamp(),
                   "CELL-API-KEY"        : self._api_key,
                   "CELL-API-PASSPHRASE" : self._passphrase}
        return headers

    def _url(self, endpoint, method, params=None):
        """ Create the url string.
        """

        url = "{}://{}:{}/{}/{}/{}".format(
            BASE_SCHEMA,
            self._ip,
            self._port,
            self._endpoint,
            endpoint,
            method
        )
        if params:
            url += "?{}".format(params)

        return url

    def _timestamp(self):
        """ Get the timestamp now. Used for nonce.
        """
        return str(int(time.time() * 1000))

    def send(self, payload={}, req="POST"):
        """ Send a message.
        """
        return self._api_call('wmq', 'send', payload, req)

    def receive(self, req="GET"):
        """ Send a message.
        """
        return self._api_call('wmq', 'receive')

    def message_history(self, req="GET"):
        """ Send a message.
        """
        return self._api_call('wmq', 'get_history', {"num": 10})

    def mesh_info(self, req="GET"):
        """ Get the mesh info.
        """
        return self._api_call('node', 'node_info')

    def battery_life(self, req="GET"):
        """ Get the battery life remaining.
        """
        return self._api_call('battery', 'get_life')

    def battery_voltage(self, req="GET"):
        """ Get voltage.
        """
        return self._api_call('battery', 'get_voltage')

    def battery_current(self, req="POST"):
        """ Get consumed current.
        """
        return self._api_call('battery', 'get_current')

    def battery_power(self, req="POST"):
        """ Get current power draw.
        """
        return self._api_call('battery', 'get_power')

    def ue_unpair(self, ue_id: str = "", req="POST"):
        """ Unpair currently paired user equiptment.
        """
        return self._api_call('ue', 'ue_unpair', {"ue_id": ue_id}, req)

    def ue_paired(self, req="GET"):
        """ Get the paired device information.
        """
        return self._api_call('ue', 'ue_paired')
