# esa-integration


This repo holds code for cell-mesh. Describe the details for installing and
running a `cell-mesh` node. It will also contain a sample client code base
for development purposes.


## Installing

Clone the github repo and find the binaries are provided in the `bin/` directory.

Before running the binaries, open a terminal and create the following
directories;

```
[user@term]$ mkdir $HOME/.wiremq
[user@term]$ mkdir $HOME/.wiremq/domain_sockets
[user@term]$ mkdir $HOME/.wiremq/message_db
```
This creates directories for the domain sockets used by `wiremq` and `sqlite`.

## Deploying

Deploy `cell` mesh node and `mesh-client` instructions below.

### Cell-Node
To deploy an instance of the cellmesh (the is the server) node, navigate to `bin/` in the cloned
repo, and run the following command:

```
./cellmesh.bin
```
This node will bind to all addresses (`0.0.0.0`) and port `7019`. This is the
temporary staged environment.


### Client

This directory holds python based client side implementation of cell
services. This is the client-side code - i.e. it is consuming the services
offered by cell.

To run the client navigate to the directory `tests`. Run the client side driver
by command

```
python -m client_test
```

This calls the client code `meshclient.py` in the directory `client/`.

NOTE: Depending on your environment, you may need to move the client_test.py
into the same directory as the `meshclient.py` and execute from there.

## Test

This directory contains a python client side test code. This impliments the
services offered by cell.

For API documentation see `docs/` directory.
