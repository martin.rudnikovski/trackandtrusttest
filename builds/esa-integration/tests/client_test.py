from client import meshclient


def main():

    IP = "localhost"
    PORT = "7019"
    ENDPOINT = "capi/v1"
    client = meshclient.MeshClient(IP, PORT, ENDPOINT)

    payload = {
        "data": "this is the data to send"
    }

    _r = client.send(payload)
    print(f"Send return : {_r} ")

    _r = client.receive()
    print(f"Receive  : {_r} ")

    _r = client.message_history()
    print(f"Message History  : {_r} ")

    _r = client.mesh_info()
    print(f"Mesh Info  : {_r} ")

    _r = client.battery_life()
    print(f"Battery Life  : {_r} ")

    _r = client.battery_voltage()
    print(f"Battery Voltage  : {_r} ")

    _r = client.battery_current()
    print(f"Battery Current  : {_r} ")

    _r = client.battery_power()
    print(f"Battery Power  : {_r} ")

    _r = client.ue_paired()
    print(f"Paired UE info  : {_r} ")

    # ue_id = _r[]

    _r = client.ue_unpair("12345")
    print(f"Unpair UE    : {_r} ")


if __name__ == "__main__":
    main()
