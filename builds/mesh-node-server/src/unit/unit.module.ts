import { Module } from '@nestjs/common';
import { EventModule } from '../event/event.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreateUnitService } from './service/create-unit.service';
import { Unit } from './domain/model/unit.entity';
import { UnitController } from './controller/unit.controller';
import { UnitSynchronizationService } from './service/unit-synchronization.service';
import { UpdateUnitService } from "./service/update-unit.service";


const services = [
    CreateUnitService,
    UpdateUnitService,
    UnitSynchronizationService
];

@Module({
    imports: [
        EventModule,
        TypeOrmModule.forFeature([
            Unit,
        ]),
    ],
    providers: [
        ...services,
    ],
    controllers: [
        UnitController,
    ],
    exports: [
        UnitSynchronizationService,
    ],
})
export class UnitModule {
}