import {BadRequestException} from "@nestjs/common";

export class InvalidUnitActionError extends BadRequestException {
    constructor(message = 'Invalid unit action send.') {
        super(message);
    }
}