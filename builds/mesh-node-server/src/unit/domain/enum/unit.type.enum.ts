import {BadRequestException} from "@nestjs/common";

export enum UnitType {
    PALLET = 'PALLET',
    CONTAINER = 'CONTAINER',
    OTHER = 'OTHER',
}

export class UnitTypeUtil{
    static parseString(unitTypeValue: string): UnitType{
        // @ts-ignore
        const unitType = UnitType[unitTypeValue.toUpperCase()]

        if(!unitType)
            throw new BadRequestException('Invalid unit type')

        return unitType
    }
}