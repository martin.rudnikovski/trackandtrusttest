import {IsNotEmpty, IsOptional, IsString} from 'class-validator';
import {BaseRequest} from '../../../shared/request/base.request';

export class CreateUnitDtoRequest extends BaseRequest {

    id: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    userId?: string;
}