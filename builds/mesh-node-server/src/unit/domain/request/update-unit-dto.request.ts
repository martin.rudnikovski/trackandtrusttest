import {CreateUnitDtoRequest} from './create-unit-dto.request';
import {Action} from '../../../event/domain/actions/action.enum';
import {IsNotEmpty, IsOptional, IsString} from 'class-validator';
import {OmitType} from '@nestjs/swagger';
import {UnitType} from '../enum/unit.type.enum';

export class UpdateUnitDtoRequest extends OmitType(CreateUnitDtoRequest, ['id']) {

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    action?: Action;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    message?: string;

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    sealNumber?: string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    unitType?: UnitType
}