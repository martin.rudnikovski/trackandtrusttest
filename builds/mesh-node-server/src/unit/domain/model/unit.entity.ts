import {Column, Entity} from 'typeorm';
import {BaseAuditedEntity} from '../../../shared/model/base-audited-entity.class';
import {UnitType} from '../enum/unit.type.enum';

@Entity('units')
export class Unit extends BaseAuditedEntity<string> {

    @Column({
        name: 'user_id',
    })
    userId: string;

    @Column({
        name: 'mesh_id',
    })
    meshId: string;

    @Column({
        type: 'text',
        name: 'unit_type'
    })
    unitType: UnitType;

    @Column()
    message: string;

    @Column({
        name: 'seal_number'
    })
    sealNumber?: string

    constructor(id: string, userID: string, meshId: string, unitType: UnitType, sealNumber?: string) {
        super(id);
        this.userId = userID;
        this.meshId = meshId;
        this.unitType = unitType;
        this.sealNumber = sealNumber
    }
}