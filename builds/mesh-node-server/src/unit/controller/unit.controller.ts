import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Param,
    Patch,
    Post,
    UseInterceptors,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import {UpdateUnitDtoRequest} from '../domain/request/update-unit-dto.request';
import {CreateUnitService} from '../service/create-unit.service';
import {CreateUnitDtoRequest} from '../domain/request/create-unit-dto.request';
import {Unit} from '../domain/model/unit.entity';
import {UpdateUnitService} from "../service/update-unit.service";

@Controller('/unit')
@UseInterceptors(ClassSerializerInterceptor)
@UsePipes(new ValidationPipe({ transform: true }))
export class UnitController {

  constructor(
    private readonly createUnitService: CreateUnitService,
    private readonly updateUnitService: UpdateUnitService
  ) {
  }

  @Post()
  createNewUnit(@Body() createUnitDtoRequest: CreateUnitDtoRequest): Promise<Unit> {
    return this.createUnitService.createUnit(createUnitDtoRequest);
  }

  @Patch(':id')
  updateUnitType(@Param('id') unitId: string, @Body() updateUnitDto: UpdateUnitDtoRequest): Promise<Unit> {
    return this.updateUnitService.updateUnit(unitId,updateUnitDto)
  }

}