import {Injectable, Logger} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {Action} from '../../event/domain/actions/action.enum';
import {Dataset} from '../../event/domain/model/dataset.model';
import {Synchronizable} from '../../shared/interface/synchronizable.interface';
import {CreateActionEventManagerService} from '../../event/service/action-event/create-action-event-manager.service';
import {ActionType} from '../../event/domain/actions/action-type.enum';
import {Unit} from "../domain/model/unit.entity";
import {CreateUnitDtoRequest} from "../domain/request/create-unit-dto.request";

@Injectable()
export class CreateUnitService implements Synchronizable<Unit> {

    private readonly logger = new Logger(CreateUnitService.name);

    constructor(
        private readonly createActionEventManagerService: CreateActionEventManagerService,
        @InjectRepository(Unit) private readonly unitRepository: Repository<Unit>,
    ) {
    }

    async createUnit(createUnitDto: CreateUnitDtoRequest): Promise<Unit> {
        const existingUnit = await this.unitRepository.findOneBy({
            id: createUnitDto.id,
        });

        if (existingUnit && existingUnit.userId === createUnitDto.userId) {
            return existingUnit;
        }

        const currentTimestamp = new Date();
        return await this.unitRepository.save({
            id: createUnitDto.id,
            userId: createUnitDto.userId,
            meshId: createUnitDto.meshId,
            timeCreated: currentTimestamp,
            timeUpdated: currentTimestamp,
        })
            .then(async newUnit => {
                await this.createActionEventManagerService.createActionEvent({
                        meshId: createUnitDto.meshId,
                        timestamp: currentTimestamp,
                        action: Action.UnitScanned,
                        actionType: ActionType.Unit,
                        latitude: createUnitDto.location.latitude,
                        longitude: createUnitDto.location.longitude,
                        dataset: {
                            unitId: newUnit.id,
                            userId: newUnit.userId
                        },
                    },
                );

                return newUnit;
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }


    async synchronize(
        meshId: string,
        latitude: number,
        longitude: number,
        timestamp: Date,
        action: Action,
        dataset: Dataset,
        signature: string
    ): Promise<Unit> {
        return await this.unitRepository.save({
            id: dataset.unitId,
            userId: dataset.userId,
        })
            .then(async unit => {
                await this.createActionEventManagerService.createActionEvent({
                    meshId,
                    actionType: ActionType.Unit,
                    action,
                    dataset,
                    timestamp,
                    latitude,
                    longitude,
                    signature
                });
                return unit;
            });
    }

}
