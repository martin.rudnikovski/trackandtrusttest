import {Injectable, Logger} from "@nestjs/common";
import {Synchronizable} from "../../shared/interface/synchronizable.interface";
import {Unit} from "../domain/model/unit.entity";
import {CreateActionEventManagerService} from "../../event/service/action-event/create-action-event-manager.service";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {UpdateUnitDtoRequest} from "../domain/request/update-unit-dto.request";
import {EntityByIdNotFound} from "../../shared/errors/entity-by-id-not-found.error";
import {objectDifferences} from "../../shared/functions/object-differences.function";
import {objectToString} from "../../shared/functions/object-to-string.function";
import {ActionType} from "../../event/domain/actions/action-type.enum";
import { Action } from '../../event/domain/actions/action.enum';
import {Dataset} from "../../event/domain/model/dataset.model";
import { UnitTypeUtil } from '../domain/enum/unit.type.enum';

@Injectable()
export class UpdateUnitService implements Synchronizable<Unit> {

    private readonly logger = new Logger(UpdateUnitService.name);

    constructor(
        private readonly createActionEventManagerService: CreateActionEventManagerService,
        @InjectRepository(Unit) private readonly unitRepository: Repository<Unit>,
    ) {
    }


    async updateUnit(unitId: string, updateUnitDto: UpdateUnitDtoRequest): Promise<Unit> {
        const unit = await this.unitRepository.findOneBy({id: unitId});

        if (!unit) {
            throw new EntityByIdNotFound(Unit.name, unitId);
        }

        const changedProperties = objectDifferences(updateUnitDto, unit);

        if (changedProperties.size == 0 && updateUnitDto.action == undefined) return unit;

        if(updateUnitDto.unitType != undefined)
            updateUnitDto.unitType = UnitTypeUtil.parseString(updateUnitDto.unitType)

        return await this.unitRepository
            .save({
                ...unit,
                ...updateUnitDto,
            })
            .then((updatedUnit) => {
                this.logger.log(
                    `Updated Unit action with ID [${unitId}] with information [${objectToString(
                        updateUnitDto,
                    )}]`,
                );

                const unitAction = updateUnitDto.action ?? Action.UnitDefined

                this.createActionEventManagerService.createActionEvent({
                    meshId: updateUnitDto.meshId,
                    latitude: updateUnitDto.location.latitude,
                    longitude: updateUnitDto.location.longitude,
                    timestamp: new Date(),
                    action: unitAction,
                    actionType: ActionType.Unit,
                    dataset: {
                        unitId: unitId,
                        value: Array.from(changedProperties).reduce((obj, [key, value]) => (
                            Object.assign(obj, {[key]: value})
                        ), {}),
                    },
                });

                return updatedUnit;
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }

    async synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Unit> {
      const unit = await this.unitRepository.findOneBy({ id: dataset.unitId });

      if (!unit) {
        throw new EntityByIdNotFound(Unit.name, dataset.unitId);
      }

      return await this.unitRepository
        .save({
          ...unit,
          ...dataset.value,
        })
        .then(async (unit) => {
          await this.createActionEventManagerService.createActionEvent({
            meshId,
            actionType: ActionType.Unit,
            action,
            dataset,
            timestamp,
            latitude,
            longitude,
            signature,
          });
          return unit;
        });
    }


}