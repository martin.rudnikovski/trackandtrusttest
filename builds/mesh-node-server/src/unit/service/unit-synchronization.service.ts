import {Injectable} from '@nestjs/common';
import {Synchronizable} from '../../shared/interface/synchronizable.interface';
import {Dataset} from '../../event/domain/model/dataset.model';
import {Action} from '../../event/domain/actions/action.enum';
import {CreateUnitService} from "./create-unit.service";
import {Unit} from "../domain/model/unit.entity";
import {UpdateUnitService} from "./update-unit.service";
import { InvalidUnitActionError } from '../domain/error/invalid-unit-action.error';

@Injectable()
export class UnitSynchronizationService implements Synchronizable<Unit> {
    constructor(
        private readonly createUnitService: CreateUnitService,
        private readonly updateUnitService: UpdateUnitService,
    ) {
    }

    async synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Unit> {
        switch (action) {
            case Action.UnitScanned: {
                    return await this.createUnitService.synchronize(
                      meshId,
                      latitude,
                      longitude,
                      timestamp,
                      action,
                      dataset,
                      signature,
                    );
            }
            case Action.UnitDefined:
            case Action.UnitPickedUp:
            case Action.UnitDroppedOff:
            case Action.UnitPickedUpMessage:
            case Action.UnitStatusUpdate:
            case Action.UnitMessageUpdate:
                return await this.updateUnitService.synchronize(
                    meshId,
                    latitude,
                    longitude,
                    timestamp,
                    action,
                    dataset,
                    signature
                );
            default:
              throw new InvalidUnitActionError(
                `Invalid unit action specified for: meshID: ${meshId}, Action: ${action}`,
              );
        }
    }


}
