import {Module} from '@nestjs/common';
import {KeyManagementService} from "../key/service/key-management.service";
import {SaveConfigurationService} from "../configuration/services/save-configuration.service";
import {FetchConfigurationService} from "../configuration/services/fetch-configuration.service";
import {HttpModule, HttpService} from "@nestjs/axios";
import {ConfigModule} from "@nestjs/config";
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigurationEntity} from "../configuration/model/entity/configuration.entity";
import {UpdateConfigurationService} from "../configuration/services/update-configuration.service";
import {FileService} from "../configuration/services/file.service";

const services = [
    KeyManagementService,
    SaveConfigurationService,
    FetchConfigurationService,
    UpdateConfigurationService,
    FileService,
]


@Module({
    providers: [...services],
    imports: [
        HttpModule,
        ConfigModule,
        TypeOrmModule.forFeature([ConfigurationEntity])
    ],
    exports: [...services]
})
export class KeyConfigurationModule {
}
