import {NestFactory} from '@nestjs/core';
import {MeshNodeModule} from './mesh-node.module';
import {ApplicationLogger} from './logger/service/application-logger.service';

async function bootstrap() {

    const app = await NestFactory.create(MeshNodeModule, {
        bufferLogs: true,
        cors: true
    });

    app.useLogger(app.get(ApplicationLogger));

    await app.listen(3000);

}

bootstrap();
