export const EventConstants = {
    CHECK_NETWORK_STATUS: 'network-status',
    STORAGE_CLEAN_UP: 'storage-clean-up',
    PULL_MESSAGES: 'pull-messages',
    PUSH_MESSAGES: 'push-messages',
} as const;