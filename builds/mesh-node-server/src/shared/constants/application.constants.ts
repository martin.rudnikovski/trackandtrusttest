export const ApplicationConstants = {
    MAIN_SERVER_URL: 'MAIN_SERVER_URL',
    CELL_SERVICE_URL: 'CELL_SERVICE_URL',
    PASS_PHRASE: 'PASS_PHRASE',
    INIT_VECTOR: 'INIT_VECTOR'
}as const;