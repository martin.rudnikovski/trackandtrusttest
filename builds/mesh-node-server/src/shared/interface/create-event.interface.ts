export interface CreateEvent {
  userId: string;
  meshId: string;
  timestamp: Date;
  location: {
    latitude: number;
    longitude: number;
  },
  action: string;
}