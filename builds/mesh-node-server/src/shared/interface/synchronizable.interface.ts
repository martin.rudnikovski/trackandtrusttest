import {Dataset} from '../../event/domain/model/dataset.model';
import {Action} from '../../event/domain/actions/action.enum';

export interface Synchronizable<Entity> {
    synchronize(meshId: string,
                latitude: number,
                longitude: number,
                timestamp: Date,
                action: Action,
                dataset: Dataset,
                signature: string): Promise<Entity>;
}