export function objectToString(object: any): string {
  return Object.entries(object)
    .map(([key, value]) => `${key}=[${JSON.stringify(value)}]`)
    .join(',');
}