export function objectDifferences(object1: any, object2: any): Map<string, any> {
  const differenceObj: Map<string, any> = new Map();

  for (const key in object1) {
    if (Object.prototype.hasOwnProperty.call(object2, key)) {
      if (object1[key] !== object2[key]) {
        differenceObj.set(key, object1[key]);
      }

    }
  }

  return differenceObj;
}