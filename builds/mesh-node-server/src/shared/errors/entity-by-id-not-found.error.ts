import { NotFoundException } from '@nestjs/common';

export class EntityByIdNotFound extends NotFoundException {
  constructor(entity: string, id: any) {
    super(`Not found entity ${entity} by ID [${id.toString()}]`);
  }
}