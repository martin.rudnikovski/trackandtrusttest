import { Copier } from './copier.class';
import { CreateDateColumn, PrimaryColumn } from 'typeorm';

export abstract class BaseEntity<ID> extends Copier {

  @PrimaryColumn({
    type: 'text',
    name: 'id'
  })
  id: ID;

  @CreateDateColumn({
    type: 'datetime',
    name: 'time_created'
  })
  timeCreated: Date;

  protected constructor(id: ID) {
    super();
    this.id = id;
  }
}