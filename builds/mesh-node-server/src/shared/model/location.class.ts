import { Column } from 'typeorm';

export class GpsLocation {
  @Column({
    type: 'real',
    name: 'latitude',
  })
  latitude: number;

  @Column({
    type: 'real',
    name: 'longitude',
  })
  longitude: number;

  constructor(latitude: number, longitude: number) {
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
