import { BaseEntity } from './base-entity.class';
import { UpdateDateColumn } from 'typeorm';

export abstract class BaseAuditedEntity<ID> extends BaseEntity<ID> {

  @UpdateDateColumn({
    type: 'datetime',
    name: 'time_updated',
  })
  timeUpdated: Date;

  protected constructor(id: ID) {
    super(id);
  }

}