import { IsDecimal, IsNumber } from 'class-validator';

export class LocationRequest {

  @IsNumber()
  latitude: number;

  @IsNumber()
  longitude: number;

}