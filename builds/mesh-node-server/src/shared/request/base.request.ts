import { IsDefined, IsNotEmptyObject, IsString, ValidateNested } from 'class-validator';
import { LocationRequest } from './location.request';
import { Type } from 'class-transformer';

export abstract class BaseRequest {

  @IsString()
  meshId: string;

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => LocationRequest)
  location: LocationRequest;

  editableFields(): string[] {
    return Object.getOwnPropertyNames(this)
      .filter(it => !['meshId', 'location'].includes(it));
  }

}