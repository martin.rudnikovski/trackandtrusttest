import { BaseRequest } from './base.request';

export class BaseUserRequest extends BaseRequest {

  userId: string;

}