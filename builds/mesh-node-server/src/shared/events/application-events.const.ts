export const ApplicationEvents = {
  User: 'event.user',
  Mesh: 'event.mesh',
  All: 'event.*'
}