import {Injectable, Logger} from "@nestjs/common";
import {HttpService} from "@nestjs/axios";
import {ConfigService} from "@nestjs/config";
import {ApplicationConstants} from "../../shared/constants/application.constants";
import {catchError, firstValueFrom, forkJoin, from, map, of, switchMap} from "rxjs";
import {PublicKeyResponse} from "../controller/response/public-key.response";
import {SaveConfigurationService} from "../../configuration/services/save-configuration.service";
import {FetchConfigurationService} from "../../configuration/services/fetch-configuration.service";
import {ConfigurationEntity} from "../../configuration/model/entity/configuration.entity";
import {ConfigurationConstants} from "../../shared/constants/configuration.constants";
import {ConfigurationDto} from "../controller/request/configuration.dto";
import {UpdateConfigurationService} from "../../configuration/services/update-configuration.service";

@Injectable()
export class KeyManagementService {
    private readonly logger = new Logger(KeyManagementService.name)

    constructor(private configurationSaveService: SaveConfigurationService,
                private fetchKeyService: FetchConfigurationService,
                private httpService: HttpService,
                private configService: ConfigService,
                private updateConfigurationService: UpdateConfigurationService) {
    }

    exchangePublicKeys = async () => {
        const mainServerUrl = this.configService.get<string>(ApplicationConstants.MAIN_SERVER_URL)

        const meshNodePublicKey = await this.fetchKeyService.findConfigurationEntityById(ConfigurationConstants.MESH_NODE_PUBLIC_KEY)
        const meshNodeId = await this.fetchKeyService.findConfigurationEntityById(ConfigurationConstants.MESH_NODE_ID)

        const meshNodeRequest = <ConfigurationDto>{
            id: meshNodeId?.value,
            value: meshNodePublicKey?.value
        }

        try {
            await firstValueFrom(
                this.httpService.post<PublicKeyResponse>(`${mainServerUrl}/api/key`, meshNodeRequest)
                    .pipe(
                        map(axiosResponse => axiosResponse.data),
                        switchMap(publicKeyResponse =>
                            forkJoin([
                                from(this.fetchKeyService.findConfigurationEntityById(
                                    ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY)),
                                of(publicKeyResponse)]
                            )
                        ),
                        switchMap(([configurationEntity, publicKeyResponse]) => {
                            if (!configurationEntity)
                                return this.configurationSaveService.saveConfigurationEntity(
                                    new ConfigurationEntity(
                                        ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY,
                                        publicKeyResponse.publicKey
                                    )
                                )
                            else
                                return this.updateConfigurationService.updateConfigurationEntity(
                                    new ConfigurationEntity(
                                        ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY,
                                        publicKeyResponse.publicKey
                                    )
                                )
                        }),
                        catchError(() => {
                            this.logger.error('Error occurred during exchangement of keys between mesh-node and main database.')
                            return of([])
                        })
                    )
            )
        } catch (e) {
            this.logger.error(e)
        }
    }

}