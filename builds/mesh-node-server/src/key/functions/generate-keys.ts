import {generateKeyPairSync} from 'crypto';
import {KeyPairInterface} from "../model/interface/key-pair.interface";

export function generateKeys(passphrase: string): KeyPairInterface {
    const {privateKey, publicKey} = generateKeyPairSync('ec', {
        namedCurve: 'secp256k1',
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        },
    });
    return <KeyPairInterface>{
        publicKey: publicKey,
        privateKey: privateKey
    };
}