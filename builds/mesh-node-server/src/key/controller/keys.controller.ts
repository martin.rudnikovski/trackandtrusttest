import {Controller, Post} from "@nestjs/common";
import {KeyManagementService} from "../service/key-management.service";

@Controller('/api/key')
export class KeysController {
    constructor(private keyManagementService: KeyManagementService) {
    }

    @Post()
    async exchangePublicKeysWithExternalServer() {
        return this.keyManagementService.exchangePublicKeys()
    }
}
