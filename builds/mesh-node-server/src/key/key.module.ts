import {Module} from '@nestjs/common';
import {KeysController} from "./controller/keys.controller";
import {KeyConfigurationModule} from "../key-configuration/key-configuration.module";
import {KeyManagementService} from "./service/key-management.service";

const controllers = [KeysController]

@Module({
    controllers: [...controllers],
    imports: [KeyConfigurationModule],
})
export class KeyModule {
}
