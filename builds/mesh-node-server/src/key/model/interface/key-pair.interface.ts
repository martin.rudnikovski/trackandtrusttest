export interface KeyPairInterface {
    publicKey: string,
    privateKey: string
}