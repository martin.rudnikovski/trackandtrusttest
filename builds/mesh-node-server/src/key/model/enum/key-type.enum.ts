export enum KeyTypeEnum{
    PublicKey = 'PUBLIC_KEY',
    PrivateKey = 'PRIVATE_KEY'
}