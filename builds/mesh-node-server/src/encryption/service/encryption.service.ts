import {Injectable} from "@nestjs/common";
import * as crypto from "crypto";
import {createCipheriv, createDecipheriv, createHash} from "crypto";
import {ConfigService} from "@nestjs/config";
import {ApplicationConstants} from "../../shared/constants/application.constants";
import * as constants from "constants";

@Injectable()
export class EncryptionService {
    private readonly algorithm = 'aes-256-cbc'
    private readonly initVector: Buffer
    private readonly key: Buffer

    private readonly passphrase: string

    constructor(private configService: ConfigService) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        this.passphrase = this.configService.get<string>(ApplicationConstants.PASS_PHRASE)!
        this.initVector = Buffer.from(this.configService.get<string>(ApplicationConstants.INIT_VECTOR)!, 'base64')
        this.key = createHash('sha256').update(this.passphrase).digest()
    }

    encrypt = (data: string): string => {
        const cipher = createCipheriv(this.algorithm, this.key, this.initVector)
        let encryptedData = cipher.update(data, 'utf-8', 'hex')
        encryptedData += cipher.final('hex')
        return encryptedData
    }

    decrypt = (encryptedData: string): string => {
        const decipher = createDecipheriv(this.algorithm, this.key, this.initVector);
        let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
        decryptedData += decipher.final("utf8");
        return decryptedData
    }

    sign = (data: string, key: string): string => {
        const sign = crypto.createSign('SHA256');
        sign.write(data);
        sign.end();
        return sign.sign(key, 'base64');
    }

    publicEncrypt = (key: string, data: Buffer) =>
        crypto.publicEncrypt({
            key: key,
            padding: constants.RSA_NO_PADDING
        }, Buffer.from(data))

    privateDecrypt = (key: string, data: Buffer) =>
        crypto.privateDecrypt({
                key,
                passphrase: this.passphrase,
                padding: crypto.constants.RSA_PKCS1_PADDING
            }, data
        )

    privateEncrypt = (key: string, data: Buffer) =>
        crypto.privateEncrypt({
                key,
                passphrase: this.passphrase,
                // padding: crypto.constants.RSA_PKCS1_PADDING
            },
            data)
}