import { Controller, Get, Post } from '@nestjs/common';
import { ActionEvent } from '../domain/entities/action-event.entity';
import { EventManagementService } from '../service/event-management.service';
import { FetchActionEventService } from '../service/action-event/fetch-action-event.service';

@Controller('/api/action')
export class ActionEventController {
  constructor(
    private fetchActionEventService: FetchActionEventService,
    private eventManagementService: EventManagementService,
  ) {}

  @Get()
  getEventActions(): Promise<ActionEvent[]> {
    return this.fetchActionEventService.fetchActionEvents();
  }

  @Post()
  sendActionEvents() {
    return this.eventManagementService.sendEventsToMainServer();
  }
  @Post('/bulk')
  sendActionEventsInBulk() {
    return this.eventManagementService.sendEventsToMainServerInBulk();
  }
}
