export enum ActionType {
  Mesh = 'MESH_ACTION',
  User = 'USER_ACTION',
  Unit = 'UNIT_ACTION',
  Photo = 'PHOTO_ACTION',
  Status = 'STATUS_ACTION',
  Delete = 'DELETE_ACTION',
  MeshNodeStatus = 'MESH_NODE_STATUS_ACTION',
}
