export class Dataset {
  userId?: string;
  unitId?: string;
  photoId?: string;
  value?: {
    [key: string]: any
  }
}
