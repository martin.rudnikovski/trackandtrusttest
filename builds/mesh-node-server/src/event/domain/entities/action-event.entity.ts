import { Column, Entity, PrimaryColumn } from 'typeorm';
import { GpsLocation } from '../../../shared/model/location.class';
import { ActionType } from '../actions/action-type.enum';
import { Action } from '../actions/action.enum';
import {sha256Hash} from "../../../shared/functions/sha256-map.function";

@Entity('action_events')
export class ActionEvent {
  @PrimaryColumn()
  id: string;

  @Column({
    type: 'text',
    name: 'mesh_id',
  })
  meshId: string;

  @Column({
    type: 'datetime',
    name: 'timestamp',
  })
  timestamp: Date;

  @Column(() => GpsLocation, {
    prefix: false,
  })
  location: GpsLocation;

  @Column({
    type: 'text',
    name: 'action_type',
  })
  actionType: ActionType;

  @Column({
    type: 'text',
    name: 'action',
  })
  action: Action;

  @Column({
    type: 'blob',
    name: 'dataset',
  })
  dataset: Buffer;

  @Column({
    type: 'text',
    name: 'signature',
  })
  signature: string;

  constructor(
    meshId: string,
    timestamp: Date,
    location: GpsLocation,
    actionType: ActionType,
    action: Action,
    dataset: Buffer,
    signature?: string,
  ) {
    this.meshId = meshId;
    this.timestamp = timestamp;
    this.location = location;
    this.actionType = actionType;
    this.action = action;
    this.dataset = dataset;
    this.id = this.generateId();
    if (signature) this.signature = signature;
  }

  private generateId(): string {
    const object = {
      meshId: this.meshId,
      timestamp: this.timestamp,
      location: this.location,
      actionType: this.actionType,
      action: this.action,
      dataset: this.dataset,
    };
    return sha256Hash(object)
  }
}
