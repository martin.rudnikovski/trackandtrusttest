import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ActionType } from '../actions/action-type.enum';
import { Action } from '../actions/action.enum';

@Entity('deletion_events')
export class DeletionEvent {
  @PrimaryColumn()
  id: string;

  @Column({
    type: 'datetime',
    name: 'timestamp',
  })
  timestamp: Date;

  @Column({
    type: 'text',
    name: 'action_type',
  })
  actionType: ActionType;

  @Column({
    type: 'text',
    name: 'action',
  })
  action: Action;

  constructor(
    id: string,
    timestamp: Date,
    actionType: ActionType,
    action?: Action,
  ) {
    this.id = id;
    this.timestamp = timestamp;
    this.actionType = actionType;
    if (action) this.action = action;
  }
}
