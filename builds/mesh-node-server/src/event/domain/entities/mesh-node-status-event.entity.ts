import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ActionType } from '../actions/action-type.enum';
import * as crypto from "crypto";

@Entity('mesh_node_status_events')
export class MeshNodeStatusEvent {
  @PrimaryGeneratedColumn('uuid', {
    name: 'id',
  })
  id: string;

  @Column({
    type: 'datetime',
    name: 'timestamp',
  })
  timestamp: Date;

  @Column({
    type: 'text',
    name: 'action_type',
  })
  actionType: ActionType;

  @Column({
    type: 'integer',
    name: 'memory',
  })
  memory: number;

  @Column({
    type: 'float',
    name: 'battery',
  })
  battery: number;

  @Column({
    type: 'text',
    name: 'mesh_id',
  })
  meshId: string;

  @Column({
    type: 'text',
    name: 'signature',
  })
  signature: string;

  constructor(
    timestamp: Date,
    actionType: ActionType,
    memory: number,
    battery: number,
    meshId: string,
    signature?: string,
  ) {
    this.id = crypto.randomUUID()
    this.timestamp = timestamp;
    this.actionType = actionType;
    this.memory = memory;
    this.battery = battery;
    this.meshId = meshId;
    if (signature) this.signature = signature;
  }
}
