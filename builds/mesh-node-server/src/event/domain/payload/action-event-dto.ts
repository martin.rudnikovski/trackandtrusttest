import { ActionType } from '../actions/action-type.enum';
import { Dataset } from '../model/dataset.model';
import { Action } from '../actions/action.enum';

export class ActionEventDTO {
  readonly meshId: string;
  readonly latitude: number;
  readonly longitude: number;
  readonly timestamp: Date;
  readonly actionType: ActionType;
  readonly action: Action;
  readonly dataset: Dataset;
  readonly signature?: string
}
