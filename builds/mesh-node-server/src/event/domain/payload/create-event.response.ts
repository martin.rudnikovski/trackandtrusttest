import { ActionType } from '../actions/action-type.enum';

export class CreateEventResponse {
  id: string;
  actionType: ActionType;
}
