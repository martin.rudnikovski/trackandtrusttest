import { ActionEvent } from '../entities/action-event.entity';
import { MeshNodeStatusEvent } from '../entities/mesh-node-status-event.entity';
import { DeletionEvent } from '../entities/deletion-event.entity';

export type Event = ActionEvent | MeshNodeStatusEvent;
export type NodeToNodeEvent = ActionEvent | DeletionEvent | MeshNodeStatusEvent;