import { Event } from './event.dto';

export interface EventRequest {
  signature: string;
  event: Event;
}
