import {Injectable} from "@nestjs/common";
import {Event} from "../domain/dto/event.dto";
import {EventRequest} from "../domain/dto/event.request";

@Injectable()
export class EventRequestMapper {
    mapEventRequest(event: Event): EventRequest {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { signature: _, ...eventWithoutSignature } = event;
        return <EventRequest>{
            signature: event.signature,
            event: eventWithoutSignature as Event,
        };
    }

    mapEventRequests(events: Event[]): EventRequest[] {
        return events.map((event) => {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const { signature: _, ...eventWithoutSignature } = event;
            return <EventRequest>{
                signature: event.signature,
                event: eventWithoutSignature as Event,
            };
        });
    }
}