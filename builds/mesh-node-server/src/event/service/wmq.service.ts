import {Injectable, Logger} from '@nestjs/common';
import {HttpService} from '@nestjs/axios';
import {catchError, firstValueFrom, map, of} from 'rxjs';
import {GossipConst} from '../gossip.const';
import {Event, NodeToNodeEvent} from '../domain/dto/event.dto';

@Injectable()
export class WmqService {
    private readonly logger = new Logger(WmqService.name)

    constructor(private readonly httpService: HttpService) {
    }

    fetchEvents(): Promise<NodeToNodeEvent[][]> {
        return firstValueFrom(
            this.httpService
                .get<NodeToNodeEvent[][]>(GossipConst.BaseUrl + '/capi/v1/wmq/receive')
                .pipe(map((it) => it.data),
                    catchError(() => {
                        this.logger.error('Error occurred during fetching events from wmq service.')
                        return [[]]
                    })),
        );
    }

    sendEvents(events: NodeToNodeEvent[]): Promise<any> {
        return firstValueFrom(
            this.httpService
                .post(GossipConst.BaseUrl + '/capi/v1/wmq/send', events)
                .pipe(map((it) => it.data),
                    catchError(() => {
                        this.logger.error('Error occurred during sending events to wmqService.')
                        return of([])
                    })),
        );
    }

    sendSignedActionEvent(event: Event): Promise<any> {
        return firstValueFrom(
            this.httpService
                .post(GossipConst.BaseUrl + '/capi/v1/wmq/send', [event])
                .pipe(map((it) => it.data),
                    catchError(err => {
                        this.logger.error('Error occurred during sending event to wmqService.')
                        return of([])
                    }))
        );
    }

    sendSignedActionEvents(events: Event[]): Promise<any> {
        return firstValueFrom(
            this.httpService
                .post(GossipConst.BaseUrl + '/capi/v1/wmq/send', events)
                .pipe(map((it) => it.data),
                    catchError(err => {
                        this.logger.error('Error occurred during sending event to wmqService.')
                        return of([])
                    }))
        );
    }

    getBatteryLife(): Promise<number> {
        return firstValueFrom(
            this.httpService
                .post(GossipConst.BaseUrl + '/capi/v1/battery/get_life')
                .pipe(map((it) => it.data.value),
                    catchError(() => {
                        this.logger.error('Error occurred during fetching of battery life info from wmqService')
                        return of([])
                    })),
        );
    }
}
