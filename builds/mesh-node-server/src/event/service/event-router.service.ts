import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { DeletionEventManagerService } from './deletion-event/deletion-event-manager.service';
import { WmqService } from './wmq.service';
import { ApplicationConstants } from '../../shared/constants/application.constants';
import { DeletionEvent } from '../domain/entities/deletion-event.entity';
import { Event } from '../domain/dto/event.dto';

@Injectable()
export class EventRouterService {
  private readonly logger = new Logger(EventRouterService.name);

  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
    private deletionEventManagerService: DeletionEventManagerService,
    private wmqService: WmqService,
  ) {}

  postEvents(events: Event[]) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const mainBackendUrl = this.configService.get<string>(
      ApplicationConstants.MAIN_SERVER_URL,
    )!;
    this.httpService
      .post<DeletionEvent[]>(
        `http://localhost:3000/api/action/bulk`,
        events,
      )
      .subscribe({
        next: (next) => {
          this.deletionEventManagerService
            .executeAndSaveDeletionEvents(next.data)
            .then(
              async (executedDeletionEvents) =>
                await this.wmqService.sendEvents(executedDeletionEvents),
            );
        },
        error: (error) => {
          this.logger.error(error?.response?.data?.message ?? error);
        },
      });
  }
}
