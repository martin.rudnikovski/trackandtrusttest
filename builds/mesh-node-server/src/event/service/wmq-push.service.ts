import {Injectable, Logger} from '@nestjs/common';
import {WmqService} from './wmq.service';
import {FetchActionEventService} from './action-event/fetch-action-event.service';
import {FetchDeletionEventService} from './deletion-event/fetch-deletion-event.service';
import {forkJoin} from 'rxjs';
import {FetchMeshNodeStatusEventService} from './mesh-node-status-event/fetch-mesh-node-status-event.service';
import {Event} from '../domain/dto/event.dto';
import {OnEvent} from '@nestjs/event-emitter';
import {EventConstants} from '../../shared/constants/event.constants';

@Injectable()
export class WmqPushService {
    constructor(
        private wmqService: WmqService,
        private fetchActionEventService: FetchActionEventService,
        private fetchDeletionEventService: FetchDeletionEventService,
        private fetchMeshNodeStatusEventService: FetchMeshNodeStatusEventService,
    ) {
    }

    private readonly logger = new Logger(WmqPushService.name)

    @OnEvent(EventConstants.PUSH_MESSAGES)
    pushEventMessages() {
        //You're using a forkjoin with promises. I don't know how good a practice this is.
        forkJoin([
            this.fetchActionEventService.fetchActionEvents(),
            this.fetchDeletionEventService.fetchDeletionEvents(),
            this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
        ]).subscribe({
            next: ([actionEvents, deletionEvents, memoryEvents]) => {
                const combinedSignedEvents = this.combineEvents(
                    actionEvents,
                    memoryEvents,
                );
                return forkJoin([
                        this.wmqService.sendSignedActionEvents(combinedSignedEvents),
                        this.wmqService.sendEvents(deletionEvents)
                    ]
                )
            },
            error: (err) => {
                this.logger.error(err)
            }
        });
    }

    private combineEvents(
        actionEvents: Event[],
        memoryEvents: Event[],
    ) {
        return actionEvents.concat(memoryEvents);
    }
}
