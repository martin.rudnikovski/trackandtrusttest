import { Injectable } from '@nestjs/common';
import { FetchActionEventService } from './action-event/fetch-action-event.service';
import { forkJoin } from 'rxjs';
import { FetchMeshNodeStatusEventService } from './mesh-node-status-event/fetch-mesh-node-status-event.service';
import { EventRouterService } from './event-router.service';
import { Event } from '../domain/dto/event.dto';

@Injectable()
export class EventManagementService {
  constructor(
    private fetchActionEventService: FetchActionEventService,
    private fetchMeshNodeStatusEventService: FetchMeshNodeStatusEventService,
    private eventRouterService: EventRouterService,
  ) {}

  sendEventsToMainServer = async () => {
    forkJoin([
      this.fetchActionEventService.fetchActionEvents(),
      this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
    ]).subscribe(([actionEvents, memoryEvents]) => {
      [...actionEvents, ...memoryEvents].forEach((it) => {
        this.eventRouterService.postEvents([it]);
      });
    });
  };
  sendEventsToMainServerInBulk = async () => {
    forkJoin([
      this.fetchActionEventService.fetchActionEvents(),
      this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
    ]).subscribe(([actionEvents, memoryEvents]) => {
      const combinedEvents: Event[] = (actionEvents as Event[]).concat(
        memoryEvents as Event[],
      );
      this.eventRouterService.postEvents(combinedEvents);
    });
  };
}
