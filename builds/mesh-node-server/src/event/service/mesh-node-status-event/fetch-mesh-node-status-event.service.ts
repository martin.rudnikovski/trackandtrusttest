import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';

@Injectable()
export class FetchMeshNodeStatusEventService {
  constructor(
    @InjectRepository(MeshNodeStatusEvent)
    private readonly meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>,
  ) {}

  fetchMeshNodeStatusEvents(): Promise<MeshNodeStatusEvent[]> {
    return this.meshNodeStatusEventRepository.find({
      order: {
        timestamp: 'ASC',
      },
    });
  }
}
