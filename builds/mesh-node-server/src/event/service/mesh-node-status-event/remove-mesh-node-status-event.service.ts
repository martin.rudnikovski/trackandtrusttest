import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, In, Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';

@Injectable()
export class RemoveMeshNodeStatusEventService {
  constructor(
    @InjectRepository(MeshNodeStatusEvent)
    private readonly meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>,
  ) {}

  removeMeshNodeStatusEvents = (ids: string[]): Promise<DeleteResult> =>
    this.meshNodeStatusEventRepository.delete({ id: In(ids) });

  removeMeshNodeStatusEvent = (id: string): Promise<DeleteResult> =>
    this.meshNodeStatusEventRepository.delete(id);
}
