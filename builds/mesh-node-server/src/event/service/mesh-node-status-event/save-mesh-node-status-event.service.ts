import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';

@Injectable()
export class SaveMeshNodeStatusEventService {
  private readonly logger = new Logger(SaveMeshNodeStatusEventService.name);

  constructor(
    @InjectRepository(MeshNodeStatusEvent)
    private readonly meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>,
  ) {}

  saveMeshNodeStatusEvent = (
    meshNodeStatusEvent: MeshNodeStatusEvent,
  ): Promise<MeshNodeStatusEvent> =>
    this.meshNodeStatusEventRepository.save(meshNodeStatusEvent).then((it) => {
      this.logger.log(`Created MeshNodeStatus Event: ${JSON.stringify(it)}`);
      return it;
    });

  saveAllMeshNodeStatusEvents = (
    meshNodeStatusEvents: MeshNodeStatusEvent[],
  ): Promise<MeshNodeStatusEvent[]> =>
    this.meshNodeStatusEventRepository.save(meshNodeStatusEvents).then((it) => {
      this.logger.log(`Created MeshNodeStatus Events: ${JSON.stringify(it)}`);
      return it;
    });
}
