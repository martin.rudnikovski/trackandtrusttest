import { Injectable } from '@nestjs/common';
import { RemoveDeletionEventService } from '../deletion-event/remove-deletion-event.service';
import { OnEvent } from '@nestjs/event-emitter';
import { EventConstants } from '../../../shared/constants/event.constants';
import { FetchDeletionEventService } from '../deletion-event/fetch-deletion-event.service';

@Injectable()
export class CleanUpStorageService {
  constructor(
    private removeDeletionEventService: RemoveDeletionEventService,
    private fetchDeletionEventService: FetchDeletionEventService,
  ) {}

  @OnEvent(EventConstants.STORAGE_CLEAN_UP)
  cleanUpStorage(diskSize: number) {
    this.fetchDeletionEventService
      .fetchDeletionEvents()
      .then(async (deletionEvents) => {
        const deletionEventsSize = JSON.stringify(deletionEvents).length;
        const twentyPercentage = diskSize / 0.2;
        const tenPercentage = diskSize / 0.1;

        if (deletionEventsSize > twentyPercentage) {
          const percentageDecrease =
            ((deletionEventsSize - tenPercentage) / deletionEventsSize) * 100;

          if (percentageDecrease > 0 && percentageDecrease < 100)
            await this.removeDeletionEventService.removeLastNPercentageDeletionEvents(
              percentageDecrease,
            );
        }
      });
  }
}
