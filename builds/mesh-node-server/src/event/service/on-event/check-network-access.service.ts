import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { OnEvent } from '@nestjs/event-emitter';
import { EventConstants } from '../../../shared/constants/event.constants';
import { EventManagementService } from '../event-management.service';

@Injectable()
export class CheckNetworkAccessService {
  constructor(
    private httpService: HttpService,
    private eventManagementService: EventManagementService,
  ) {}

  @OnEvent(EventConstants.CHECK_NETWORK_STATUS)
  checkForNetworkAccess() {
    this.httpService.get('http://www.google.com').subscribe({
      next: async () => {
        await this.eventManagementService.sendEventsToMainServerInBulk();
      },
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      error: () => {},
    });
  }
}
