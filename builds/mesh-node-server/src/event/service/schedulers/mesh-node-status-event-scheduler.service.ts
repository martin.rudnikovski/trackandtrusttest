import { Injectable } from '@nestjs/common';
import { SaveMeshNodeStatusEventService } from '../mesh-node-status-event/save-mesh-node-status-event.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import checkDiskSpace from 'check-disk-space';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';
import { ActionType } from '../../domain/actions/action-type.enum';
import { FetchConfigurationService } from '../../../configuration/services/fetch-configuration.service';
import { ConfigurationConstants } from '../../../shared/constants/configuration.constants';
import { WmqService } from '../wmq.service';
import { SigningService } from '../signing.service';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EventConstants } from '../../../shared/constants/event.constants';

@Injectable()
export class MeshNodeStatusEventSchedulerService {
  constructor(
    private saveMeshNodeStatusEventService: SaveMeshNodeStatusEventService,
    private configurationFetchService: FetchConfigurationService,
    private readonly wmqService: WmqService,
    private readonly signingService: SigningService,
    private eventEmitter: EventEmitter2,
  ) {}

  @Cron(CronExpression.EVERY_6_HOURS)
  createMeshNodeStatusEvent() {
    checkDiskSpace('/').then(async (diskSpace) => {
      this.eventEmitter.emit(EventConstants.STORAGE_CLEAN_UP, diskSpace.size);

      const meshNodeId =
        await this.configurationFetchService.findConfigurationEntityById(
          ConfigurationConstants.MESH_NODE_ID,
        );

      const batteryValue = await this.wmqService.getBatteryLife();

      if (meshNodeId) {
        const event = new MeshNodeStatusEvent(
          new Date(),
          ActionType.MeshNodeStatus,
          diskSpace.free,
          batteryValue,
          meshNodeId?.value,
        );

        event.signature = this.signingService.sign(JSON.stringify(event));

        await this.saveMeshNodeStatusEventService
          .saveMeshNodeStatusEvent(event)
          .then((memoryEvent) => this.wmqService.sendSignedActionEvent(memoryEvent));
      }
      // Note: `free` and `size` are in bytes
    });
  }
}
