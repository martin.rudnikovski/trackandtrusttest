import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EventConstants } from '../../../shared/constants/event.constants';

@Injectable()
export class NetworkAccessSchedulerService {
  constructor(private eventEmitter: EventEmitter2) {}

  // @Cron(CronExpression.EVERY_MINUTE)
  checkForNetworkAccess() {
    this.eventEmitter.emit(EventConstants.CHECK_NETWORK_STATUS);
  }
}
