import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { EventConstants } from '../../../shared/constants/event.constants';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class WmqReceiveSchedulerService {
  constructor(private eventEmitter: EventEmitter2) {}

  @Cron(CronExpression.EVERY_3_HOURS)
  initSynchronization() {
    this.eventEmitter.emit(EventConstants.PULL_MESSAGES);
  }
}
