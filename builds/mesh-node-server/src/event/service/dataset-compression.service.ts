import {Injectable} from '@nestjs/common';
import {Dataset} from '../domain/model/dataset.model';
import * as zlib from "zlib";


@Injectable()
export class DatasetCompressionService {
    compress(dataset: Dataset): Buffer {
        const stringifiedDataset = JSON.stringify(dataset)
        return zlib.deflateSync(stringifiedDataset, {
            level: zlib.constants.Z_BEST_COMPRESSION
        })
    }

    decompress(blob: Buffer): Dataset {
        const decompressedData = zlib.inflateSync(blob);
        return JSON.parse(decompressedData.toString('utf-8'));
    }
}
