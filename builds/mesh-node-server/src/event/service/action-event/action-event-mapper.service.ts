import { Injectable } from '@nestjs/common';
import { DatasetCompressionService } from '../dataset-compression.service';
import { ActionEventDTO } from '../../domain/payload/action-event-dto';
import { ActionEvent } from '../../domain/entities/action-event.entity';
import { GpsLocation } from '../../../shared/model/location.class';

@Injectable()
export class ActionEventMapper {
  constructor(
    private readonly datasetCompressionService: DatasetCompressionService,
  ) {}

  mapActionEvent(payload: ActionEventDTO): ActionEvent {
    const compressedDataset = this.datasetCompressionService.compress(
      payload.dataset,
    );
    return new ActionEvent(
      payload.meshId,
      payload.timestamp,
      new GpsLocation(payload.latitude, payload.longitude),
      payload.actionType,
      payload.action,
      compressedDataset,
    );
  }
}
