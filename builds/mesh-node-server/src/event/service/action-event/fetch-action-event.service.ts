import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ActionEvent } from '../../domain/entities/action-event.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FetchActionEventService {
  constructor(
    @InjectRepository(ActionEvent)
    private readonly actionEventRepository: Repository<ActionEvent>,
  ) {
  }

  fetchActionEvents(): Promise<ActionEvent[]> {
    return this.actionEventRepository.find({
      order: {
        timestamp: 'ASC',
      },
    });
  }
}
