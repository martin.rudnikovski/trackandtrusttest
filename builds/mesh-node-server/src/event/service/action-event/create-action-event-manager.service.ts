import { Injectable } from '@nestjs/common';
import { ActionEvent } from '../../domain/entities/action-event.entity';
import { ActionEventDTO } from '../../domain/payload/action-event-dto';
import { FetchDeletionEventService } from '../deletion-event/fetch-deletion-event.service';
import { WmqService } from '../wmq.service';
import { SigningService } from '../signing.service';
import { SaveActionEventService } from './save-action-event.service';
import { ActionEventMapper } from './action-event-mapper.service';

@Injectable()
export class CreateActionEventManagerService {
  constructor(
    private readonly saveActionEventService: SaveActionEventService,
    private readonly actionEventMapper: ActionEventMapper,
    private readonly signingService: SigningService,
    private readonly fetchDeletionEventService: FetchDeletionEventService,
    private readonly wmqService: WmqService,
  ) {}

  createActionEvent(payload: ActionEventDTO): Promise<ActionEvent | void> {
    const actionEvent =
      this.actionEventMapper.mapActionEvent(payload);

    return this.fetchDeletionEventService
      .fetchDeletionEventById(actionEvent.id)
      .then(async (it) => {
        if (it) return;

        actionEvent.signature =
          payload.signature ??
          this.signingService.sign(JSON.stringify(actionEvent));

        if (!payload.signature) await this.wmqService.sendSignedActionEvent(actionEvent);
        return this.saveActionEventService.saveActionEvent(actionEvent);
      });
  }
}
