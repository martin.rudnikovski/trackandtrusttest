import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ActionEvent } from '../../domain/entities/action-event.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SaveActionEventService {
  private readonly logger = new Logger(SaveActionEventService.name);

  constructor(
    @InjectRepository(ActionEvent)
    private readonly actionEventRepository: Repository<ActionEvent>,
  ) {}

  saveActionEvent = (actionEvent: ActionEvent): Promise<ActionEvent> =>
    this.actionEventRepository.save(actionEvent).then((it) => {
      this.logger.log(`Created Action Event: ${JSON.stringify(it)}`);
      return it;
    })
}
