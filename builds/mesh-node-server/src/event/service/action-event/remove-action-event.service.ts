import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ActionEvent } from '../../domain/entities/action-event.entity';
import { DeleteResult, In, Repository } from 'typeorm';

@Injectable()
export class RemoveActionEventService {
  constructor(
    @InjectRepository(ActionEvent)
    private readonly actionEventRepository: Repository<ActionEvent>,
  ) {}

  removeActionEvents = (actionEventsIds: string[]): Promise<DeleteResult> =>
    this.actionEventRepository.delete({ id: In(actionEventsIds) });

  removeActionEvent = (id: string): Promise<DeleteResult> =>
    this.actionEventRepository.delete(id);
}
