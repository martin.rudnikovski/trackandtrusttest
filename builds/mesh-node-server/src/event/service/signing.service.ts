import { Injectable } from '@nestjs/common';
import { EncryptionService } from '../../encryption/service/encryption.service';
import { FileService } from '../../configuration/services/file.service';
import * as process from 'process';

@Injectable()
export class SigningService {
  constructor(
    private readonly encryptionService: EncryptionService,
    private readonly fileService: FileService,
  ) {}

  sign(data: string): string {
    const privateKeyPath = `${process.env.HOME}/.mesh_node/keys`;
    const encryptedPrivateKey =
      this.fileService.readKeysFromFile(privateKeyPath);
    const privateKey = this.encryptionService.decrypt(encryptedPrivateKey);

    return this.encryptionService.sign(data, privateKey);
  }
}
