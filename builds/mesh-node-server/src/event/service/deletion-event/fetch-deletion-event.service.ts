import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';

@Injectable()
export class FetchDeletionEventService {
  constructor(
    @InjectRepository(DeletionEvent)
    private readonly deletionEventRepository: Repository<DeletionEvent>,
  ) {}

  fetchDeletionEvents(): Promise<DeletionEvent[]> {
    return this.deletionEventRepository.find({
      order: {
        timestamp: 'ASC',
      },
    });
  }

  fetchDeletionEventById (id: string): Promise<DeletionEvent | null> {
    return this.deletionEventRepository.findOne({ where: { id: id } });
  }
}
