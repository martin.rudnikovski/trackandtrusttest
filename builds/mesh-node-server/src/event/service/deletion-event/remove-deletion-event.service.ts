import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RemoveDeletionEventService {
  constructor(
    @InjectRepository(DeletionEvent)
    private readonly deletionEventRepository: Repository<DeletionEvent>,
  ) {}

  async removeLastNDeletionEvents(n: number): Promise<DeletionEvent[]> {
    const rowsToDelete = await this.deletionEventRepository
      .createQueryBuilder()
      .orderBy('timestamp', 'ASC')
      .take(n)
      .getMany();
    return await this.deletionEventRepository.remove(rowsToDelete);
  }

  async removeLastNPercentageDeletionEvents(
    percentage: number,
  ): Promise<DeletionEvent[]> {
    const totalRows = await this.deletionEventRepository.count();
    const n = Math.floor((totalRows * percentage) / 100);
    const rowsToDelete = await this.deletionEventRepository
      .createQueryBuilder()
      .orderBy('timestamp', 'ASC')
      .take(n)
      .getMany();
    return await this.deletionEventRepository.remove(rowsToDelete);
  }
}
