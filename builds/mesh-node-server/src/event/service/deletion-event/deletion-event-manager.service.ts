import { Injectable } from '@nestjs/common';
import { RemoveMeshNodeStatusEventService } from '../mesh-node-status-event/remove-mesh-node-status-event.service';
import { RemoveActionEventService } from '../action-event/remove-action-event.service';
import { SaveDeletionEventService } from './save-deletion-event.service';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
import { Action } from '../../domain/actions/action.enum';

@Injectable()
export class DeletionEventManagerService {
  constructor(
    private readonly removeMeshNodeStatusEventService: RemoveMeshNodeStatusEventService,
    private readonly removeActionEventService: RemoveActionEventService,
    private readonly saveDeletionEventService: SaveDeletionEventService,
  ) {}

  async executeAndSaveDeletionEvent(
    event: DeletionEvent,
  ): Promise<DeletionEvent> {
    if (event.action === Action.DeleteActionEvent) {
      await this.removeActionEventService.removeActionEvent(event.id);
    } else if (event.action === Action.DeleteMeshNodeStatusEvent) {
      await this.removeMeshNodeStatusEventService.removeMeshNodeStatusEvent(event.id);
    }

    return await this.saveDeletionEventService.saveDeletionEvent(event);
  }

  async executeAndSaveDeletionEvents(
    events: DeletionEvent[],
  ): Promise<DeletionEvent[]> {
    const deleteActionEventIds = events
      .filter((event) => event.action === Action.DeleteActionEvent)
      .map((it) => it.id);
    const deleteMemoryEvents = events
      .filter((event) => event.action === Action.DeleteMeshNodeStatusEvent)
      .map((it) => it.id);

    await this.removeActionEventService.removeActionEvents(
      deleteActionEventIds,
    );
    await this.removeMeshNodeStatusEventService.removeMeshNodeStatusEvents(deleteMemoryEvents);

    return await this.saveDeletionEventService.saveAllDeletionEvents(events);
  }
}
