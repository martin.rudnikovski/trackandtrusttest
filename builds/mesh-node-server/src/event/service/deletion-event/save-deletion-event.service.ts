import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SaveDeletionEventService {
  private readonly logger = new Logger(SaveDeletionEventService.name);

  constructor(
    @InjectRepository(DeletionEvent)
    private readonly deletionEventRepository: Repository<DeletionEvent>,
  ) {}

  saveDeletionEvent = (deletionEvent: DeletionEvent): Promise<DeletionEvent> =>
    this.deletionEventRepository.save(deletionEvent).then((it) => {
      this.logger.log(`Created Deletion Event: ${JSON.stringify(it)}`);
      return it;
    });

  saveAllDeletionEvents = (
    deletionEvents: DeletionEvent[],
  ): Promise<DeletionEvent[]> =>
    this.deletionEventRepository.save(deletionEvents).then((it) => {
      this.logger.log(`Created Deletion Events: ${JSON.stringify(it)}`);
      return it;
    });
}
