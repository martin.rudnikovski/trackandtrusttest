import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ActionEvent } from './domain/entities/action-event.entity';
import { CreateActionEventManagerService } from './service/action-event/create-action-event-manager.service';
import { DatasetCompressionService } from './service/dataset-compression.service';
import { FetchActionEventService } from './service/action-event/fetch-action-event.service';
import { ActionEventController } from './controller/action-event.controller';
import { WmqService } from './service/wmq.service';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';
import { WmqPushService } from './service/wmq-push.service';
import { KeyConfigurationModule } from '../key-configuration/key-configuration.module';
import { EventManagementService } from './service/event-management.service';
import { ConfigModule } from '@nestjs/config';
import { EncryptionModule } from '../encryption/encryption.module';
import { DeletionEvent } from './domain/entities/deletion-event.entity';
import { SaveDeletionEventService } from './service/deletion-event/save-deletion-event.service';
import { FetchDeletionEventService } from './service/deletion-event/fetch-deletion-event.service';
import { RemoveActionEventService } from './service/action-event/remove-action-event.service';
import { MeshNodeStatusEvent } from './domain/entities/mesh-node-status-event.entity';
import { RemoveDeletionEventService } from './service/deletion-event/remove-deletion-event.service';
import { FetchMeshNodeStatusEventService } from './service/mesh-node-status-event/fetch-mesh-node-status-event.service';
import { SaveMeshNodeStatusEventService } from './service/mesh-node-status-event/save-mesh-node-status-event.service';
import { RemoveMeshNodeStatusEventService } from './service/mesh-node-status-event/remove-mesh-node-status-event.service';
import { MeshNodeStatusEventSchedulerService } from './service/schedulers/mesh-node-status-event-scheduler.service';
import { DeletionEventManagerService } from './service/deletion-event/deletion-event-manager.service';
import { SigningService } from './service/signing.service';
import { SaveActionEventService } from './service/action-event/save-action-event.service';
import { ActionEventMapper } from './service/action-event/action-event-mapper.service';
import { EventRouterService } from './service/event-router.service';
import { EventRequestMapper } from './service/event-request-mapper.service';
import { NetworkAccessSchedulerService } from './service/schedulers/network-access-scheduler.service';
import { CheckNetworkAccessService } from './service/on-event/check-network-access.service';
import { CleanUpStorageService } from './service/on-event/clean-up-storage.service';
import { WmqReceiveSchedulerService } from './service/schedulers/wmq-receive-scheduler.service';

const deletionServices = [
  SaveDeletionEventService,
  FetchDeletionEventService,
  RemoveDeletionEventService,
];

const memoryServices = [
  FetchMeshNodeStatusEventService,
  SaveMeshNodeStatusEventService,
  RemoveMeshNodeStatusEventService,
];

const actionServices = [
  FetchActionEventService,
  CreateActionEventManagerService,
  ActionEventMapper,
  EventManagementService,
  SaveActionEventService,
];

const services = [
  DatasetCompressionService,
  WmqPushService,
  MeshNodeStatusEventSchedulerService,
  WmqService,
  RemoveActionEventService,
  ...deletionServices,
  ...memoryServices,
  ...actionServices,
  DeletionEventManagerService,
  SigningService,
  EventRouterService,
  EventRequestMapper,
  NetworkAccessSchedulerService,
  CheckNetworkAccessService,
  CleanUpStorageService,
  WmqReceiveSchedulerService,
];

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    KeyConfigurationModule,
    TypeOrmModule.forFeature([ActionEvent, DeletionEvent, MeshNodeStatusEvent]),
    ScheduleModule.forRoot(),
    EncryptionModule,
  ],
  providers: [...services],
  controllers: [ActionEventController],
  exports: [
    FetchActionEventService,
    DatasetCompressionService,
    CreateActionEventManagerService,
    SaveDeletionEventService,
    SaveMeshNodeStatusEventService,
    WmqService,
    RemoveActionEventService,
    RemoveMeshNodeStatusEventService,
    DeletionEventManagerService,
  ],
})
export class EventModule {}
