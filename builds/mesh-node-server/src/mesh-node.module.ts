import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {UserModule} from './user/user.module';
import {EventModule} from './event/event.module';
import {EventEmitterModule} from '@nestjs/event-emitter';
import {LoggerModule} from './logger/logger.module';
import {GossipModule} from './gossip/gossip.module';
import {PhotoModule} from './photo/photo.module';
import {UnitModule} from './unit/unit.module';
import {ConfigurationModule} from './configuration/configuration.module';
import {KeyModule} from './key/key.module';
import {KeyConfigurationModule} from './key-configuration/key-configuration.module';
import {EncryptionModule} from './encryption/encryption.module';

@Module({
    imports: [
        ConfigurationModule,
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'better-sqlite3',
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                database: configService.get<string>('DATABASE')!,
                autoLoadEntities: true,
            }),
        }),
        EventEmitterModule.forRoot({
            wildcard: true,
        }),
        EventModule,
        UserModule,
        UnitModule,
        LoggerModule,
        GossipModule,
        PhotoModule,
        ConfigurationModule,
        KeyModule,
        KeyConfigurationModule,
        EncryptionModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class MeshNodeModule {
}
