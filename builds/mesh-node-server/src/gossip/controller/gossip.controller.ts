import {Controller, Get} from "@nestjs/common";
import {GossipSynchronizationService} from "../service/gossip-synchronization.service";

@Controller('gossip')
export class GossipController {

    constructor(private readonly gossipSynchronizationService: GossipSynchronizationService) {
    }

    @Get('/init')
    initSynchronization(): Promise<any> {
        return this.gossipSynchronizationService.synchronize()
    }
}