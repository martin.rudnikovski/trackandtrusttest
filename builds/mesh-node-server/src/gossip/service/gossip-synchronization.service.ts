import {Injectable} from '@nestjs/common';
import {DatasetCompressionService} from '../../event/service/dataset-compression.service';
import {UserSynchronizationService} from '../../user/service/user-synchronization.service';
import {ActionType} from '../../event/domain/actions/action-type.enum';
import {PhotoSynchronizationService} from '../../photo/service/photo-synchronization.service';
import {UnitSynchronizationService} from '../../unit/service/unit-synchronization.service';
import {WmqService} from '../../event/service/wmq.service';
import {ActionEvent} from '../../event/domain/entities/action-event.entity';
import {DeletionEvent} from '../../event/domain/entities/deletion-event.entity';
import {
    SaveMeshNodeStatusEventService
} from '../../event/service/mesh-node-status-event/save-mesh-node-status-event.service';
import {MeshNodeStatusEvent} from '../../event/domain/entities/mesh-node-status-event.entity';
import {DeletionEventManagerService} from '../../event/service/deletion-event/deletion-event-manager.service';
import {NodeToNodeEvent} from '../../event/domain/dto/event.dto';
import {EventEmitter2, OnEvent} from '@nestjs/event-emitter';
import {EventConstants} from '../../shared/constants/event.constants';

@Injectable()
export class GossipSynchronizationService {
    constructor(
        private wmqService: WmqService,
        private readonly datasetCompressionService: DatasetCompressionService,
        private readonly userSynchronizationService: UserSynchronizationService,
        private readonly unitSynchronizationService: UnitSynchronizationService,
        private readonly photoSynchronizationService: PhotoSynchronizationService,
        private readonly saveMeshNodeStatusEventService: SaveMeshNodeStatusEventService,
        private readonly deletionEventManagerService: DeletionEventManagerService,
        private readonly eventEmitter: EventEmitter2,
    ) {
    }

    @OnEvent(EventConstants.PULL_MESSAGES)
    async synchronize(): Promise<void> {
        const events = await this.wmqService.fetchEvents();
        if (events) {
            const flatMappedEvents = events.flatMap(it => it)

            for (const event of flatMappedEvents) {
                switch (event.actionType) {
                    case ActionType.Delete: {
                        await this.deletionEventManagerService.executeAndSaveDeletionEvent(
                            event as DeletionEvent,
                        );
                        break;
                    }
                    case ActionType.Unit: {
                        await this.unitSynchronizationService.synchronize(
                            ...this.createActionEventTuple(event),
                        );
                        break;
                    }
                    case ActionType.Photo: {
                        await this.photoSynchronizationService.synchronize(
                            ...this.createActionEventTuple(event),
                        );
                        break;
                    }
                    case ActionType.User: {
                        await this.userSynchronizationService.synchronize(
                            ...this.createActionEventTuple(event),
                        );
                        break;
                    }
                    case ActionType.MeshNodeStatus: {
                        const meshNodeStatusEvent = event as MeshNodeStatusEvent;
                        await this.saveMeshNodeStatusEventService.saveMeshNodeStatusEvent(
                            meshNodeStatusEvent,
                        );
                        break;
                    }
                    default: //TODO:(actions: MESH, STATUS)
                }
            }

            this.eventEmitter.emit(EventConstants.PUSH_MESSAGES);
        }
    }

    private createActionEventTuple(event: NodeToNodeEvent) {
        const actionEvent = event as ActionEvent;
        const decompressedDataset = this.datasetCompressionService.decompress(
            Buffer.from(actionEvent.dataset),
        );
        return [
            actionEvent.meshId,
            actionEvent.location.latitude,
            actionEvent.location.longitude,
            actionEvent.timestamp,
            actionEvent.action,
            decompressedDataset,
            actionEvent.signature,
        ] as const;
    }
}
