import { Module } from '@nestjs/common';
import { GossipSynchronizationService } from './service/gossip-synchronization.service';
import { EventModule } from '../event/event.module';
import { GossipController } from './controller/gossip.controller';
import { UserModule } from '../user/user.module';
import { UnitModule } from "../unit/unit.module";
import { PhotoModule } from '../photo/photo.module';

const controllers = [GossipController];

const services = [GossipSynchronizationService];

@Module({
  imports: [EventModule, UserModule, PhotoModule,UnitModule],
  controllers: [...controllers],
  providers: [...services],
})
export class GossipModule {}
