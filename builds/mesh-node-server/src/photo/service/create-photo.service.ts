import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Photo } from '../domain/model/photo.entity';
import { Repository } from 'typeorm';
import { CreateActionEventManagerService } from '../../event/service/action-event/create-action-event-manager.service';
import { ActionType } from '../../event/domain/actions/action-type.enum';
import { Action } from '../../event/domain/actions/action.enum';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';

@Injectable()
export class CreatePhotoService implements Synchronizable<Photo> {
  private readonly logger = new Logger(CreatePhotoService.name);

  constructor(
    private readonly createActionEventManagerService: CreateActionEventManagerService,
    @InjectRepository(Photo)
    private readonly photoRepository: Repository<Photo>,
  ) {}

  createPhoto(params: {
    photo: Buffer;
    userId: string;
    meshId: string;
    latitude: number;
    longitude: number;
    unitId?: string;
    message?: string;
    id?: string;
    signature?: string;
  }): Promise<Photo> {
    const newPhoto = new Photo(
      params.photo,
      params.userId,
      params.unitId,
      params.message,
      params.id,
    );

    return this.photoRepository.save(newPhoto).then(async (photo) => {
      this.logger.log(
          `Created new Photo instance with ID [${photo.id}] by user [${params.userId}]`,
      );
      await this.createActionEventManagerService.createActionEvent({
        meshId: params.meshId,
        latitude: params.latitude,
        longitude: params.longitude,
        timestamp: new Date(),
        action: Action.PhotoTaken,
        actionType: ActionType.Photo,
        dataset: {
          userId: params.userId,
          unitId: params.unitId,
          photoId: photo.id,
          value: {
            photo: photo.photo,
            message: params.message,
          },
        },
        signature: params.signature,
      });

      return photo;
    });
  }

  synchronize(
    meshId: string,
    latitude: number,
    longitude: number,
    timestamp: Date,
    action: Action,
    dataset: Dataset,
    signature: string
  ): Promise<Photo> {
    return this.createPhoto({
      meshId,
      latitude,
      longitude,
      photo: Buffer.from(dataset.value?.photo.toString()),
      userId: dataset.userId!,
      unitId: dataset.unitId!,
      id: dataset.photoId!,
      message: dataset.value?.message?.toString() ?? undefined,
      signature: signature
    });
  }
}
