import { Injectable } from '@nestjs/common';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Action } from '../../event/domain/actions/action.enum';
import { CreatePhotoService } from './create-photo.service';
import { Photo } from '../domain/model/photo.entity';
import { InvalidPhotoActionError } from '../domain/error/invalid-photo-action.error';

@Injectable()
export class PhotoSynchronizationService implements Synchronizable<Photo> {
  constructor(private readonly createPhotoService: CreatePhotoService) {}

  async synchronize(
    meshId: string,
    latitude: number,
    longitude: number,
    timestamp: Date,
    action: Action,
    dataset: Dataset,
    signature: string
  ): Promise<Photo> {
    switch (action) {
      case Action.PhotoTaken: {
        return await this.createPhotoService.synchronize(
          meshId,
          latitude,
          longitude,
          timestamp,
          action,
          dataset,
          signature,
        );
      }
      default:
        throw new InvalidPhotoActionError(
          `Invalid photo action specified for: meshID: ${meshId}, Action: ${action}`,
        );
    }
  }
}
