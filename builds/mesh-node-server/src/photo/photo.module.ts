import { Module } from '@nestjs/common';
import { CreatePhotoService } from './service/create-photo.service';
import { PhotoController } from './controller/photo.controller';
import { EventModule } from '../event/event.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Photo } from './domain/model/photo.entity';
import { PhotoSynchronizationService } from './service/photo-synchronization.service';

@Module({
  imports: [EventModule, TypeOrmModule.forFeature([Photo])],
  controllers: [PhotoController],
  providers: [CreatePhotoService, PhotoSynchronizationService],
  exports: [PhotoSynchronizationService],
})
export class PhotoModule {}
