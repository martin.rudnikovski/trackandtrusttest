import { BadRequestException } from '@nestjs/common';

export class InvalidPhotoActionError extends BadRequestException {
  constructor(message = 'Invalid photo action send.') {
    super(message);
  }
}
