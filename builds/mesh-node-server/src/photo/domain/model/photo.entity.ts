import { Column, Entity } from 'typeorm';
import { BaseAuditedEntity } from '../../../shared/model/base-audited-entity.class';
import { v4 as uuid4 } from 'uuid';

@Entity('photos')
export class Photo extends BaseAuditedEntity<string> {
  @Column('blob')
  photo: Buffer;

  @Column({
    name: 'user_id',
  })
  userId: string;

  @Column({
    nullable: true,
    name: 'unit_id',
  })
  unitId?: string;

  @Column({
    nullable: true,
  })
  message?: string;

  constructor(
    photo: Buffer,
    userId: string,
    unitId?: string,
    message?: string,
    id: string = uuid4(),
  ) {
    super(id);
    this.userId = userId;
    this.photo = photo;
    this.unitId = unitId;
    this.message = message;
  }
}
