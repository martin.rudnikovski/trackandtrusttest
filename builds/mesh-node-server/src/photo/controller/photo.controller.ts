import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreatePhotoRequest } from './request/create-photo.request';
import { Photo } from '../domain/model/photo.entity';
import { CreatePhotoService } from '../service/create-photo.service';

@Controller('/photo')
@UseInterceptors(ClassSerializerInterceptor)
@UsePipes(new ValidationPipe({ transform: true }))
export class PhotoController {
  constructor(private readonly createPhotoService: CreatePhotoService) {}

  @Post()
  createNewPhoto(
    @Body() createPhotoRequest: CreatePhotoRequest,
  ): Promise<Photo> {
    return this.createPhotoService.createPhoto({
      photo: createPhotoRequest.photo,
      userId: createPhotoRequest.userId,
      unitId: createPhotoRequest.unitId,
      message: createPhotoRequest.message,
      meshId: createPhotoRequest.meshId,
      latitude: createPhotoRequest.location.latitude,
      longitude: createPhotoRequest.location.longitude,
    });
  }
}
