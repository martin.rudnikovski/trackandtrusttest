import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { BaseRequest } from '../../../shared/request/base.request';

export class UpdatePhotoRequest extends BaseRequest {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  unitId?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  message?: string;
}
