import { IsBase64, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { BaseRequest } from '../../../shared/request/base.request';

export class CreatePhotoRequest extends BaseRequest {

  @IsString()
  @IsNotEmpty()
  userId: string;

  @IsNotEmpty()
  photo: Buffer;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  unitId?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  message?: string;

}