import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './domain/model/user.entity';
import { EventModule } from '../event/event.module';
import { UsersController } from './controller/users.controller';
import { UpdateUserService } from './service/update-user.service';
import { CreateUserService } from './service/create-user.service';
import { UserSynchronizationService } from './service/user-synchronization.service';

const services = [
  UpdateUserService,
  CreateUserService,
  UserSynchronizationService,
];

@Module({
  imports: [
    EventModule,
    TypeOrmModule.forFeature([
      User,
    ]),
  ],
  providers: [
    ...services,
  ],
  controllers: [
    UsersController,
  ],
  exports: [
    UserSynchronizationService,
  ],
})
export class UserModule {
}
