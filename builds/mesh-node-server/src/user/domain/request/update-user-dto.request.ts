import {Exclude} from 'class-transformer';
import {CreateUserRequest} from './create-user-dto.request';
import {Action} from "../../../event/domain/actions/action.enum";

export class UpdateUserRequest extends CreateUserRequest {

    action?: Action

    id: string;

    @Exclude()
    timeCreated: Date;

    @Exclude()
    timeUpdated: Date;

}