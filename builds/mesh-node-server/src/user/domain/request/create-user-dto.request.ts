import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { BaseRequest } from '../../../shared/request/base.request';

export class CreateUserRequest extends BaseRequest {

  id: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  role?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  organisation?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  name?: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  phoneNumber?: string;

  editableFields(): string[] {
    return Object.getOwnPropertyNames(this)
      .filter(it => !(['id', ...super.editableFields()])
          .includes(it));
  }

}