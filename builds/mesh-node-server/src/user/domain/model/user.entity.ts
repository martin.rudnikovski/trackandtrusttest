import {Column, Entity} from 'typeorm';
import {BaseAuditedEntity} from '../../../shared/model/base-audited-entity.class';

@Entity('users')
export class User extends BaseAuditedEntity<string> {

    @Column()
    role: string;

    @Column()
    organisation: string;

    @Column()
    name: string;

    @Column({
        name: 'phone_number',
    })
    phoneNumber: string;

    @Column()
    message: string;

    constructor(id: string, role: string, organisation: string, name: string, phoneNumber: string) {
        super(id);
        this.role = role;
        this.organisation = organisation;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

}