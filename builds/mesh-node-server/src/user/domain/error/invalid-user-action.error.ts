import {BadRequestException} from "@nestjs/common";

export class InvalidUserActionError extends BadRequestException {
    constructor(message = 'Invalid user action send.') {
        super(message);
    }
}