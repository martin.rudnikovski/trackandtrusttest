import {Injectable} from '@nestjs/common';
import { CreateUserService } from './create-user.service';
import { UpdateUserService } from './update-user.service';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { User } from '../domain/model/user.entity';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Action } from '../../event/domain/actions/action.enum';
import {InvalidUserActionError} from "../domain/error/invalid-user-action.error";

@Injectable()
export class UserSynchronizationService implements Synchronizable<User> {
  constructor(
    private readonly createUserService: CreateUserService,
    private readonly updateUserService: UpdateUserService,
  ) {
  }

  async synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User> {
    switch (action) {
      case Action.UserInformationUpdated: {
        return await this.updateUserService.synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature);
      }
      case Action.UserCreated: {
        return await this.createUserService.synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature);
      }
      default:
        throw new InvalidUserActionError(
          `Invalid user action specified for: meshID: ${meshId}, Action: ${action}`,
        );
    }
  }


}
