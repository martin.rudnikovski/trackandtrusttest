import {Injectable, Logger} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {User} from '../domain/model/user.entity';
import {EntityByIdNotFound} from '../../shared/errors/entity-by-id-not-found.error';
import {objectToString} from '../../shared/functions/object-to-string.function';
import {objectDifferences} from '../../shared/functions/object-differences.function';
import {LocationRequest} from '../../shared/request/location.request';
import {Action} from '../../event/domain/actions/action.enum';
import {Synchronizable} from '../../shared/interface/synchronizable.interface';
import {Dataset} from '../../event/domain/model/dataset.model';
import {ActionType} from '../../event/domain/actions/action-type.enum';
import {UpdateUserRequest} from "../domain/request/update-user-dto.request";
import {CreateActionEventManagerService} from "../../event/service/action-event/create-action-event-manager.service";

@Injectable()
export class UpdateUserService implements Synchronizable<User> {

    private readonly logger = new Logger(UpdateUserService.name);

    constructor(
        private readonly createActionEventManagerService: CreateActionEventManagerService,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
    ) {
    }

    async updateUser(id: string, meshId: string, location: LocationRequest, updateUserDto: UpdateUserRequest): Promise<User> {
        const user = await this.userRepository.findOneBy({id});

        if (!user) {
            throw new EntityByIdNotFound(User.name, id);
        }

        const changedProperties = objectDifferences(updateUserDto, user);

        if (changedProperties.size == 0) return user;

        return await this.userRepository
            .save({
                ...user,
                ...updateUserDto,
            })
            .then((updatedUser) => {
                this.logger.log(
                    `Updated User with ID [${id}] with information [${objectToString(
                        updateUserDto,
                    )}]`,
                );

                const updateAction = updateUserDto.action ?? Action.UserInformationUpdated

                this.createActionEventManagerService.createActionEvent({
                    meshId,
                    latitude: location.latitude,
                    longitude: location.longitude,
                    timestamp: new Date(),
                    action: updateAction,
                    actionType: ActionType.User,
                    dataset: {
                        userId: id,
                        value: Array.from(changedProperties).reduce((obj, [key, value]) => (
                            Object.assign(obj, {[key]: value})
                        ), {}),
                    },
                });

                return updatedUser;
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }

    async synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User> {
        const user = await this.userRepository.findOneBy({id: dataset.userId});

        if (!user) {
            throw new EntityByIdNotFound(User.name, dataset.userId);
        }

        return await this.userRepository
            .save({
                ...user,
                ...dataset.value,
            })
            .then(async (user) => {
                await this.createActionEventManagerService.createActionEvent({
                    meshId,
                    actionType: ActionType.User,
                    action,
                    dataset,
                    timestamp,
                    latitude,
                    longitude,
                    signature,
                });
                return user;
            });
    }

}
