import {Injectable, Logger} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {User} from '../domain/model/user.entity';
import {CreateUserRequest} from '../domain/request/create-user-dto.request';
import {Action} from '../../event/domain/actions/action.enum';
import {Dataset} from '../../event/domain/model/dataset.model';
import {Synchronizable} from '../../shared/interface/synchronizable.interface';
import {ActionType} from '../../event/domain/actions/action-type.enum';
import {sha256Hash} from "../../shared/functions/sha256-map.function";
import {CreateActionEventManagerService} from "../../event/service/action-event/create-action-event-manager.service";

@Injectable()
export class CreateUserService implements Synchronizable<User> {

    private readonly logger = new Logger(CreateUserService.name);

    constructor(
        private readonly createActionEventManagerService: CreateActionEventManagerService,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
    ) {
    }

    createUserId = (createUserRequest: CreateUserRequest) =>
        sha256Hash({
                id: createUserRequest.id,
                phoneNumber: createUserRequest?.phoneNumber
            }
        )


    async createUser(createUserDto: CreateUserRequest): Promise<User> {
        const existingUser = await this.userRepository.findOneBy({
            id: createUserDto.id,
        });

        if (existingUser) {
            return existingUser;
        }

        const currentTimestamp = new Date();

        return await this.userRepository.save({
            id: this.createUserId(createUserDto),
            timeCreated: currentTimestamp,
            timeUpdated: currentTimestamp,
        })
            .then(async newUser => {
                await this.createActionEventManagerService.createActionEvent({
                        meshId: createUserDto.meshId,
                        timestamp: currentTimestamp,
                        action: Action.UserCreated,
                        actionType: ActionType.User,
                        latitude: createUserDto.location.latitude,
                        longitude: createUserDto.location.longitude,
                        dataset: {
                            userId: newUser.id,
                        },
                    },
                );

                return newUser;
            })
            .catch((err) => {
                this.logger.error(err);
                return err;
            });
    }


    async synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User> {
        return await this.userRepository.save({
            id: dataset.userId,
        })
            .then(async user => {
                await this.createActionEventManagerService.createActionEvent({
                    meshId,
                    actionType: ActionType.User,
                    action,
                    dataset,
                    timestamp,
                    latitude,
                    longitude,
                    signature,
                });
                return user;
            });
    }

}
