import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Patch,
  Post,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {UpdateUserService} from '../service/update-user.service';
import {CreateUserRequest} from '../domain/request/create-user-dto.request';
import {User} from '../domain/model/user.entity';
import {CreateUserService} from '../service/create-user.service';
import {UpdateUserRequest} from '../domain/request/update-user-dto.request';

@Controller('/users')
@UseInterceptors(ClassSerializerInterceptor)
@UsePipes(new ValidationPipe({transform: true}))
export class UsersController {

    constructor(private readonly updateUserService: UpdateUserService,
                private readonly createUserService: CreateUserService) {
    }

    @Post()
    createNewUser(@Body() createUserDto: CreateUserRequest): Promise<User> {
        return this.createUserService.createUser(createUserDto);
    }

    @Patch()
    updateUser(@Body() updateUser: UpdateUserRequest): Promise<User> {
        return this.updateUserService.updateUser(updateUser.id, updateUser.meshId, updateUser.location, updateUser);
    }

}