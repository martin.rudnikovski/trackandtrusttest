import {Module} from '@nestjs/common';
import {configurationProvider} from "./providers/configuration.provider";
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigurationEntity} from "./model/entity/configuration.entity";
import {KeyConfigurationModule} from "../key-configuration/key-configuration.module";
import {ConfigModule} from "@nestjs/config";
import {HttpModule} from "@nestjs/axios";

const providers = [configurationProvider];


@Module({
    providers: [...providers],
    imports: [
        TypeOrmModule.forFeature([ConfigurationEntity]),
        KeyConfigurationModule,
        ConfigModule,
        HttpModule
    ],
    exports: [
        configurationProvider
    ]
})
export class ConfigurationModule {
}
