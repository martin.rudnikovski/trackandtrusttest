import {Injectable, Logger} from "@nestjs/common";
import {chmodSync, mkdirSync, readFileSync, writeFileSync} from "fs";
import {fileExistsSync} from "tsconfig-paths/lib/filesystem";
import {KeyTypeEnum} from "../../key/model/enum/key-type.enum";

@Injectable()
export class FileService {
    private readonly logger = new Logger(FileService.name)

    writeKeysToFile(path: string, data: string): boolean {
        mkdirSync(path, {recursive: true})
        return this.writeToFile(`${path}/private.key`, data, KeyTypeEnum.PrivateKey)
    }

    private writeToFile(path: string, data: string, keyType: KeyTypeEnum): boolean {
        if (!fileExistsSync(path)) {
            writeFileSync(path, data, {flag: 'wx'})
            chmodSync(path, 0o600)
            this.logger.log(`${keyType} has been stored.`)
            return true
        } else {
            this.logger.log(`${keyType} already exists`)
            return false
        }
    }

    readKeysFromFile(path: string): string {
        const privateKeyPath = `${path}/private.key`

        const privateKeyRead = this.readFromFile(privateKeyPath, KeyTypeEnum.PrivateKey)

        return privateKeyRead ? privateKeyRead : ''
    }

    private readFromFile(path: string, keyType: KeyTypeEnum) {
        if (fileExistsSync(path)) {
            const key = readFileSync(path, 'utf-8')
            this.logger.log(`Fetched ${keyType}`)
            return key
        } else
            this.logger.error(`This file does not exist ${path}`)
    }
}