import {Injectable, Logger} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {ConfigurationEntity} from "../model/entity/configuration.entity";
import {Repository} from "typeorm";
import {ConfigurationDto} from "../../key/controller/request/configuration.dto";
import {EntityByIdNotFound} from "../../shared/errors/entity-by-id-not-found.error";

@Injectable()
export class UpdateConfigurationService {
    private readonly logger = new Logger(UpdateConfigurationService.name)

    constructor(@InjectRepository(ConfigurationEntity)
                private readonly configurationEntityRepository: Repository<ConfigurationEntity>) {
    }

    updateConfigurationEntity = async (configurationDto: ConfigurationDto): Promise<ConfigurationEntity> => {

        await this.configurationEntityRepository.update(configurationDto.id, {...configurationDto})
        const configurationEntity = await this.configurationEntityRepository.findOneBy({
            id: configurationDto.id
        })

        if (!configurationEntity)
            throw new EntityByIdNotFound(ConfigurationEntity.name, configurationDto.id)

        this.logger.log(`Updated configuration entity: [${configurationEntity.id}]`)
        return configurationEntity
    }
}