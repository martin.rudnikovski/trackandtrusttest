import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {ConfigurationEntity} from "../model/entity/configuration.entity";
import {Repository} from "typeorm";

@Injectable()
export class FetchConfigurationService {
    constructor(@InjectRepository(ConfigurationEntity)
                private readonly configurationEntityRepository: Repository<ConfigurationEntity>) {
    }

    findConfigurationEntityById = async (id: string): Promise<ConfigurationEntity | null> =>
        await this.configurationEntityRepository.findOneBy({id: id})

}