import {Injectable, Logger} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {ConfigurationEntity} from "../model/entity/configuration.entity";
import {ConfigurationDto} from "../../key/controller/request/configuration.dto";

@Injectable()
export class SaveConfigurationService {
    private readonly logger = new Logger(SaveConfigurationService.name)

    constructor(@InjectRepository(ConfigurationEntity)
                private readonly configurationEntityRepository: Repository<ConfigurationEntity>) {
    }

    saveConfigurationEntity = async (configurationDto: ConfigurationDto): Promise<ConfigurationEntity> => {
        const configurationEntity = await this.configurationEntityRepository.save(
            new ConfigurationEntity(
                configurationDto.id,
                configurationDto.value)
        )
        this.logger.log(`Created configuration entity: [${configurationEntity.id}]`)

        return configurationEntity
    }
}