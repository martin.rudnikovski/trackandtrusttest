import {Provider} from '@nestjs/common';
import {generateKeys} from "../../key/functions/generate-keys";
import * as process from "process";
import {FileService} from "../services/file.service";
import {SaveConfigurationService} from "../services/save-configuration.service";
import {ConfigurationEntity} from "../model/entity/configuration.entity";
import {FetchConfigurationService} from "../services/fetch-configuration.service";
import {ConfigurationConstants} from "../../shared/constants/configuration.constants";
import {KeyManagementService} from "../../key/service/key-management.service";
import {EncryptionService} from "../../encryption/service/encryption.service";
import {ConfigService} from "@nestjs/config";
import {ApplicationConstants} from "../../shared/constants/application.constants";
import {HttpService} from "@nestjs/axios";
import {lastValueFrom, map} from "rxjs";
import {NodeInfoDto} from "../model/entity/node-info.dto";


export const configurationProvider: Provider = {
    provide: 'KEYS',
    inject: [
        FileService,
        SaveConfigurationService,
        FetchConfigurationService,
        KeyManagementService,
        EncryptionService,
        ConfigService,
        HttpService
    ],
    useFactory: async (
        fileService: FileService,
        configurationSaveService: SaveConfigurationService,
        configurationFetchService: FetchConfigurationService,
        keyManagementService: KeyManagementService,
        encryptionService: EncryptionService,
        configService: ConfigService,
        httpService: HttpService
    ) => {
        const keysDirectory = `${process.env.HOME}/.mesh_node/keys`
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const cellServiceUrl = configService.get<string>(
            ApplicationConstants.CELL_SERVICE_URL,
        )!;

        await registerMeshNodeId(configurationFetchService, configurationSaveService, httpService, cellServiceUrl)

        const privateKeyExists = fileService.readKeysFromFile(keysDirectory)

        if (privateKeyExists === '') {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const passphrase = configService.get<string>(ApplicationConstants.PASS_PHRASE)!

            const keys = generateKeys(passphrase);
            const encryptedPrivateKey = encryptionService.encrypt(keys.privateKey)

            fileService.writeKeysToFile(keysDirectory, encryptedPrivateKey)

            await configurationSaveService.saveConfigurationEntity(
                new ConfigurationEntity(
                    ConfigurationConstants.MESH_NODE_PUBLIC_KEY,
                    keys.publicKey
                ))

            await keyManagementService.exchangePublicKeys()
        }

    },
};

const registerMeshNodeId = async (fetchConfigurationService: FetchConfigurationService,
                                  saveConfigurationService: SaveConfigurationService,
                                  httpService: HttpService,
                                  cellServiceUrl: string) => {
    const meshNodeId = ConfigurationConstants.MESH_NODE_ID


    const meshNodeIdExists = await fetchConfigurationService.findConfigurationEntityById(meshNodeId)
    if (!meshNodeIdExists) {
        const meshNodeInfoDto = await lastValueFrom(httpService.get<NodeInfoDto>(`${cellServiceUrl}/capi/v1/node/node_info`)
            .pipe(
                map((response) => response.data)
            )
        )

        await saveConfigurationService.saveConfigurationEntity(new ConfigurationEntity(
                meshNodeId,
                meshNodeInfoDto.mesh_id
            )
        )
    }

    return Promise.resolve(meshNodeIdExists !== undefined)
}