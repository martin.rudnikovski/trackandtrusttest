import {Column, CreateDateColumn, Entity, PrimaryColumn, UpdateDateColumn} from 'typeorm';

@Entity({
    name: 'configuration',
})
export class ConfigurationEntity {
    @PrimaryColumn({
        name: 'id',
        type: 'varchar',
    })
    id: string;

    @Column({
        name: 'value',
        type: 'varchar',
    })
    value: string;

    @CreateDateColumn({
        type: 'datetime',
        name: 'time_created'
    })
    timeCreated: Date;

    @UpdateDateColumn({
        type: 'datetime',
        name: 'time_updated',
    })
    timeUpdated: Date;

    constructor(id: string, value: string) {
        this.id = id;
        this.value = value;
    }
}
