export interface NodeInfoDto {
    mesh_id: string,
    uptime: string,
    services: string[]
}