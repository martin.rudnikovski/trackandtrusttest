"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configurationProvider = void 0;
const generate_keys_1 = require("../../key/functions/generate-keys");
const process = require("process");
const file_service_1 = require("../services/file.service");
const save_configuration_service_1 = require("../services/save-configuration.service");
const configuration_entity_1 = require("../model/entity/configuration.entity");
const fetch_configuration_service_1 = require("../services/fetch-configuration.service");
const configuration_constants_1 = require("../../shared/constants/configuration.constants");
const key_management_service_1 = require("../../key/service/key-management.service");
const encryption_service_1 = require("../../encryption/service/encryption.service");
const config_1 = require("@nestjs/config");
const application_constants_1 = require("../../shared/constants/application.constants");
const axios_1 = require("@nestjs/axios");
const rxjs_1 = require("rxjs");
exports.configurationProvider = {
    provide: 'KEYS',
    inject: [
        file_service_1.FileService,
        save_configuration_service_1.SaveConfigurationService,
        fetch_configuration_service_1.FetchConfigurationService,
        key_management_service_1.KeyManagementService,
        encryption_service_1.EncryptionService,
        config_1.ConfigService,
        axios_1.HttpService
    ],
    useFactory: async (fileService, configurationSaveService, configurationFetchService, keyManagementService, encryptionService, configService, httpService) => {
        const keysDirectory = `${process.env.HOME}/.mesh_node/keys`;
        const cellServiceUrl = configService.get(application_constants_1.ApplicationConstants.CELL_SERVICE_URL);
        await registerMeshNodeId(configurationFetchService, configurationSaveService, httpService, cellServiceUrl);
        const privateKeyExists = fileService.readKeysFromFile(keysDirectory);
        if (privateKeyExists === '') {
            const passphrase = configService.get(application_constants_1.ApplicationConstants.PASS_PHRASE);
            const keys = (0, generate_keys_1.generateKeys)(passphrase);
            const encryptedPrivateKey = encryptionService.encrypt(keys.privateKey);
            fileService.writeKeysToFile(keysDirectory, encryptedPrivateKey);
            await configurationSaveService.saveConfigurationEntity(new configuration_entity_1.ConfigurationEntity(configuration_constants_1.ConfigurationConstants.MESH_NODE_PUBLIC_KEY, keys.publicKey));
            await keyManagementService.exchangePublicKeys();
        }
    },
};
const registerMeshNodeId = async (fetchConfigurationService, saveConfigurationService, httpService, cellServiceUrl) => {
    const meshNodeId = configuration_constants_1.ConfigurationConstants.MESH_NODE_ID;
    const meshNodeIdExists = await fetchConfigurationService.findConfigurationEntityById(meshNodeId);
    if (!meshNodeIdExists) {
        const meshNodeInfoDto = await (0, rxjs_1.lastValueFrom)(httpService.get(`${cellServiceUrl}/capi/v1/node/node_info`)
            .pipe((0, rxjs_1.map)((response) => response.data)));
        await saveConfigurationService.saveConfigurationEntity(new configuration_entity_1.ConfigurationEntity(meshNodeId, meshNodeInfoDto.mesh_id));
    }
    return Promise.resolve(meshNodeIdExists !== undefined);
};
//# sourceMappingURL=configuration.provider.js.map