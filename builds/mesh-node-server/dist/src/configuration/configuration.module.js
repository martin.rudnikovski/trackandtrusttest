"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigurationModule = void 0;
const common_1 = require("@nestjs/common");
const configuration_provider_1 = require("./providers/configuration.provider");
const typeorm_1 = require("@nestjs/typeorm");
const configuration_entity_1 = require("./model/entity/configuration.entity");
const key_configuration_module_1 = require("../key-configuration/key-configuration.module");
const config_1 = require("@nestjs/config");
const axios_1 = require("@nestjs/axios");
const providers = [configuration_provider_1.configurationProvider];
let ConfigurationModule = class ConfigurationModule {
};
ConfigurationModule = __decorate([
    (0, common_1.Module)({
        providers: [...providers],
        imports: [
            typeorm_1.TypeOrmModule.forFeature([configuration_entity_1.ConfigurationEntity]),
            key_configuration_module_1.KeyConfigurationModule,
            config_1.ConfigModule,
            axios_1.HttpModule
        ],
        exports: [
            configuration_provider_1.configurationProvider
        ]
    })
], ConfigurationModule);
exports.ConfigurationModule = ConfigurationModule;
//# sourceMappingURL=configuration.module.js.map