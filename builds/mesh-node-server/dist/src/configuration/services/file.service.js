"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var FileService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileService = void 0;
const common_1 = require("@nestjs/common");
const fs_1 = require("fs");
const filesystem_1 = require("tsconfig-paths/lib/filesystem");
const key_type_enum_1 = require("../../key/model/enum/key-type.enum");
let FileService = FileService_1 = class FileService {
    constructor() {
        this.logger = new common_1.Logger(FileService_1.name);
    }
    writeKeysToFile(path, data) {
        (0, fs_1.mkdirSync)(path, { recursive: true });
        return this.writeToFile(`${path}/private.key`, data, key_type_enum_1.KeyTypeEnum.PrivateKey);
    }
    writeToFile(path, data, keyType) {
        if (!(0, filesystem_1.fileExistsSync)(path)) {
            (0, fs_1.writeFileSync)(path, data, { flag: 'wx' });
            (0, fs_1.chmodSync)(path, 0o600);
            this.logger.log(`${keyType} has been stored.`);
            return true;
        }
        else {
            this.logger.log(`${keyType} already exists`);
            return false;
        }
    }
    readKeysFromFile(path) {
        const privateKeyPath = `${path}/private.key`;
        const privateKeyRead = this.readFromFile(privateKeyPath, key_type_enum_1.KeyTypeEnum.PrivateKey);
        return privateKeyRead ? privateKeyRead : '';
    }
    readFromFile(path, keyType) {
        if ((0, filesystem_1.fileExistsSync)(path)) {
            const key = (0, fs_1.readFileSync)(path, 'utf-8');
            this.logger.log(`Fetched ${keyType}`);
            return key;
        }
        else
            this.logger.error(`This file does not exist ${path}`);
    }
};
FileService = FileService_1 = __decorate([
    (0, common_1.Injectable)()
], FileService);
exports.FileService = FileService;
//# sourceMappingURL=file.service.js.map