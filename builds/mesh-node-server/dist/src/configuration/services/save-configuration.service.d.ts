import { Repository } from "typeorm";
import { ConfigurationEntity } from "../model/entity/configuration.entity";
import { ConfigurationDto } from "../../key/controller/request/configuration.dto";
export declare class SaveConfigurationService {
    private readonly configurationEntityRepository;
    private readonly logger;
    constructor(configurationEntityRepository: Repository<ConfigurationEntity>);
    saveConfigurationEntity: (configurationDto: ConfigurationDto) => Promise<ConfigurationEntity>;
}
