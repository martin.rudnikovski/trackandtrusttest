import { ConfigurationEntity } from "../model/entity/configuration.entity";
import { Repository } from "typeorm";
import { ConfigurationDto } from "../../key/controller/request/configuration.dto";
export declare class UpdateConfigurationService {
    private readonly configurationEntityRepository;
    private readonly logger;
    constructor(configurationEntityRepository: Repository<ConfigurationEntity>);
    updateConfigurationEntity: (configurationDto: ConfigurationDto) => Promise<ConfigurationEntity>;
}
