"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var SaveConfigurationService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveConfigurationService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const configuration_entity_1 = require("../model/entity/configuration.entity");
let SaveConfigurationService = SaveConfigurationService_1 = class SaveConfigurationService {
    constructor(configurationEntityRepository) {
        this.configurationEntityRepository = configurationEntityRepository;
        this.logger = new common_1.Logger(SaveConfigurationService_1.name);
        this.saveConfigurationEntity = async (configurationDto) => {
            const configurationEntity = await this.configurationEntityRepository.save(new configuration_entity_1.ConfigurationEntity(configurationDto.id, configurationDto.value));
            this.logger.log(`Created configuration entity: [${configurationEntity.id}]`);
            return configurationEntity;
        };
    }
};
SaveConfigurationService = SaveConfigurationService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(configuration_entity_1.ConfigurationEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SaveConfigurationService);
exports.SaveConfigurationService = SaveConfigurationService;
//# sourceMappingURL=save-configuration.service.js.map