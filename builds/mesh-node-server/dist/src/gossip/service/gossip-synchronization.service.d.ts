import { DatasetCompressionService } from '../../event/service/dataset-compression.service';
import { UserSynchronizationService } from '../../user/service/user-synchronization.service';
import { PhotoSynchronizationService } from '../../photo/service/photo-synchronization.service';
import { UnitSynchronizationService } from '../../unit/service/unit-synchronization.service';
import { WmqService } from '../../event/service/wmq.service';
import { SaveMeshNodeStatusEventService } from '../../event/service/mesh-node-status-event/save-mesh-node-status-event.service';
import { DeletionEventManagerService } from '../../event/service/deletion-event/deletion-event-manager.service';
import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class GossipSynchronizationService {
    private wmqService;
    private readonly datasetCompressionService;
    private readonly userSynchronizationService;
    private readonly unitSynchronizationService;
    private readonly photoSynchronizationService;
    private readonly saveMeshNodeStatusEventService;
    private readonly deletionEventManagerService;
    private readonly eventEmitter;
    constructor(wmqService: WmqService, datasetCompressionService: DatasetCompressionService, userSynchronizationService: UserSynchronizationService, unitSynchronizationService: UnitSynchronizationService, photoSynchronizationService: PhotoSynchronizationService, saveMeshNodeStatusEventService: SaveMeshNodeStatusEventService, deletionEventManagerService: DeletionEventManagerService, eventEmitter: EventEmitter2);
    synchronize(): Promise<void>;
    private createActionEventTuple;
}
