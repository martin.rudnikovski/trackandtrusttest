"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GossipSynchronizationService = void 0;
const common_1 = require("@nestjs/common");
const dataset_compression_service_1 = require("../../event/service/dataset-compression.service");
const user_synchronization_service_1 = require("../../user/service/user-synchronization.service");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const photo_synchronization_service_1 = require("../../photo/service/photo-synchronization.service");
const unit_synchronization_service_1 = require("../../unit/service/unit-synchronization.service");
const wmq_service_1 = require("../../event/service/wmq.service");
const save_mesh_node_status_event_service_1 = require("../../event/service/mesh-node-status-event/save-mesh-node-status-event.service");
const deletion_event_manager_service_1 = require("../../event/service/deletion-event/deletion-event-manager.service");
const event_emitter_1 = require("@nestjs/event-emitter");
const event_constants_1 = require("../../shared/constants/event.constants");
let GossipSynchronizationService = class GossipSynchronizationService {
    constructor(wmqService, datasetCompressionService, userSynchronizationService, unitSynchronizationService, photoSynchronizationService, saveMeshNodeStatusEventService, deletionEventManagerService, eventEmitter) {
        this.wmqService = wmqService;
        this.datasetCompressionService = datasetCompressionService;
        this.userSynchronizationService = userSynchronizationService;
        this.unitSynchronizationService = unitSynchronizationService;
        this.photoSynchronizationService = photoSynchronizationService;
        this.saveMeshNodeStatusEventService = saveMeshNodeStatusEventService;
        this.deletionEventManagerService = deletionEventManagerService;
        this.eventEmitter = eventEmitter;
    }
    async synchronize() {
        const events = await this.wmqService.fetchEvents();
        if (events) {
            const flatMappedEvents = events.flatMap(it => it);
            for (const event of flatMappedEvents) {
                switch (event.actionType) {
                    case action_type_enum_1.ActionType.Delete: {
                        await this.deletionEventManagerService.executeAndSaveDeletionEvent(event);
                        break;
                    }
                    case action_type_enum_1.ActionType.Unit: {
                        await this.unitSynchronizationService.synchronize(...this.createActionEventTuple(event));
                        break;
                    }
                    case action_type_enum_1.ActionType.Photo: {
                        await this.photoSynchronizationService.synchronize(...this.createActionEventTuple(event));
                        break;
                    }
                    case action_type_enum_1.ActionType.User: {
                        await this.userSynchronizationService.synchronize(...this.createActionEventTuple(event));
                        break;
                    }
                    case action_type_enum_1.ActionType.MeshNodeStatus: {
                        const meshNodeStatusEvent = event;
                        await this.saveMeshNodeStatusEventService.saveMeshNodeStatusEvent(meshNodeStatusEvent);
                        break;
                    }
                    default:
                }
            }
            this.eventEmitter.emit(event_constants_1.EventConstants.PUSH_MESSAGES);
        }
    }
    createActionEventTuple(event) {
        const actionEvent = event;
        const decompressedDataset = this.datasetCompressionService.decompress(Buffer.from(actionEvent.dataset));
        return [
            actionEvent.meshId,
            actionEvent.location.latitude,
            actionEvent.location.longitude,
            actionEvent.timestamp,
            actionEvent.action,
            decompressedDataset,
            actionEvent.signature,
        ];
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(event_constants_1.EventConstants.PULL_MESSAGES),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GossipSynchronizationService.prototype, "synchronize", null);
GossipSynchronizationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [wmq_service_1.WmqService,
        dataset_compression_service_1.DatasetCompressionService,
        user_synchronization_service_1.UserSynchronizationService,
        unit_synchronization_service_1.UnitSynchronizationService,
        photo_synchronization_service_1.PhotoSynchronizationService,
        save_mesh_node_status_event_service_1.SaveMeshNodeStatusEventService,
        deletion_event_manager_service_1.DeletionEventManagerService,
        event_emitter_1.EventEmitter2])
], GossipSynchronizationService);
exports.GossipSynchronizationService = GossipSynchronizationService;
//# sourceMappingURL=gossip-synchronization.service.js.map