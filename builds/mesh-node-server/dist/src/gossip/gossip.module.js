"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GossipModule = void 0;
const common_1 = require("@nestjs/common");
const gossip_synchronization_service_1 = require("./service/gossip-synchronization.service");
const event_module_1 = require("../event/event.module");
const gossip_controller_1 = require("./controller/gossip.controller");
const user_module_1 = require("../user/user.module");
const unit_module_1 = require("../unit/unit.module");
const photo_module_1 = require("../photo/photo.module");
const controllers = [gossip_controller_1.GossipController];
const services = [gossip_synchronization_service_1.GossipSynchronizationService];
let GossipModule = class GossipModule {
};
GossipModule = __decorate([
    (0, common_1.Module)({
        imports: [event_module_1.EventModule, user_module_1.UserModule, photo_module_1.PhotoModule, unit_module_1.UnitModule],
        controllers: [...controllers],
        providers: [...services],
    })
], GossipModule);
exports.GossipModule = GossipModule;
//# sourceMappingURL=gossip.module.js.map