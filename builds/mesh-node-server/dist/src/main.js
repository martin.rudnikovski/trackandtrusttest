"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const mesh_node_module_1 = require("./mesh-node.module");
const application_logger_service_1 = require("./logger/service/application-logger.service");
async function bootstrap() {
    const app = await core_1.NestFactory.create(mesh_node_module_1.MeshNodeModule, {
        bufferLogs: true,
        cors: true
    });
    app.useLogger(app.get(application_logger_service_1.ApplicationLogger));
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map