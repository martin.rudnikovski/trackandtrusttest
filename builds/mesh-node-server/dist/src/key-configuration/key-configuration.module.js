"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeyConfigurationModule = void 0;
const common_1 = require("@nestjs/common");
const key_management_service_1 = require("../key/service/key-management.service");
const save_configuration_service_1 = require("../configuration/services/save-configuration.service");
const fetch_configuration_service_1 = require("../configuration/services/fetch-configuration.service");
const axios_1 = require("@nestjs/axios");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const configuration_entity_1 = require("../configuration/model/entity/configuration.entity");
const update_configuration_service_1 = require("../configuration/services/update-configuration.service");
const file_service_1 = require("../configuration/services/file.service");
const services = [
    key_management_service_1.KeyManagementService,
    save_configuration_service_1.SaveConfigurationService,
    fetch_configuration_service_1.FetchConfigurationService,
    update_configuration_service_1.UpdateConfigurationService,
    file_service_1.FileService,
];
let KeyConfigurationModule = class KeyConfigurationModule {
};
KeyConfigurationModule = __decorate([
    (0, common_1.Module)({
        providers: [...services],
        imports: [
            axios_1.HttpModule,
            config_1.ConfigModule,
            typeorm_1.TypeOrmModule.forFeature([configuration_entity_1.ConfigurationEntity])
        ],
        exports: [...services]
    })
], KeyConfigurationModule);
exports.KeyConfigurationModule = KeyConfigurationModule;
//# sourceMappingURL=key-configuration.module.js.map