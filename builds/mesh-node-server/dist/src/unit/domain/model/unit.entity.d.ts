import { BaseAuditedEntity } from '../../../shared/model/base-audited-entity.class';
import { UnitType } from '../enum/unit.type.enum';
export declare class Unit extends BaseAuditedEntity<string> {
    userId: string;
    meshId: string;
    unitType: UnitType;
    message: string;
    sealNumber?: string;
    constructor(id: string, userID: string, meshId: string, unitType: UnitType, sealNumber?: string);
}
