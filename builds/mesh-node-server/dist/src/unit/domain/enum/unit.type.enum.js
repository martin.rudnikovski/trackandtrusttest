"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitTypeUtil = exports.UnitType = void 0;
const common_1 = require("@nestjs/common");
var UnitType;
(function (UnitType) {
    UnitType["PALLET"] = "PALLET";
    UnitType["CONTAINER"] = "CONTAINER";
    UnitType["OTHER"] = "OTHER";
})(UnitType = exports.UnitType || (exports.UnitType = {}));
class UnitTypeUtil {
    static parseString(unitTypeValue) {
        const unitType = UnitType[unitTypeValue.toUpperCase()];
        if (!unitType)
            throw new common_1.BadRequestException('Invalid unit type');
        return unitType;
    }
}
exports.UnitTypeUtil = UnitTypeUtil;
//# sourceMappingURL=unit.type.enum.js.map