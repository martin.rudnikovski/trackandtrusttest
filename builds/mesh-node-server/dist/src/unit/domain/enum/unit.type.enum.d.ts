export declare enum UnitType {
    PALLET = "PALLET",
    CONTAINER = "CONTAINER",
    OTHER = "OTHER"
}
export declare class UnitTypeUtil {
    static parseString(unitTypeValue: string): UnitType;
}
