import { BaseRequest } from '../../../shared/request/base.request';
export declare class CreateUnitDtoRequest extends BaseRequest {
    id: string;
    userId?: string;
}
