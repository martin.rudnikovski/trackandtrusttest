import { BadRequestException } from "@nestjs/common";
export declare class InvalidUnitActionError extends BadRequestException {
    constructor(message?: string);
}
