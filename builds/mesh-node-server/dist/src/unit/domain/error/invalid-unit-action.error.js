"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidUnitActionError = void 0;
const common_1 = require("@nestjs/common");
class InvalidUnitActionError extends common_1.BadRequestException {
    constructor(message = 'Invalid unit action send.') {
        super(message);
    }
}
exports.InvalidUnitActionError = InvalidUnitActionError;
//# sourceMappingURL=invalid-unit-action.error.js.map