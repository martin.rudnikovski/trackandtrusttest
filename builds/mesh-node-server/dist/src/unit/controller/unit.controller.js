"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitController = void 0;
const common_1 = require("@nestjs/common");
const update_unit_dto_request_1 = require("../domain/request/update-unit-dto.request");
const create_unit_service_1 = require("../service/create-unit.service");
const create_unit_dto_request_1 = require("../domain/request/create-unit-dto.request");
const update_unit_service_1 = require("../service/update-unit.service");
let UnitController = class UnitController {
    constructor(createUnitService, updateUnitService) {
        this.createUnitService = createUnitService;
        this.updateUnitService = updateUnitService;
    }
    createNewUnit(createUnitDtoRequest) {
        return this.createUnitService.createUnit(createUnitDtoRequest);
    }
    updateUnitType(unitId, updateUnitDto) {
        return this.updateUnitService.updateUnit(unitId, updateUnitDto);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_unit_dto_request_1.CreateUnitDtoRequest]),
    __metadata("design:returntype", Promise)
], UnitController.prototype, "createNewUnit", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_unit_dto_request_1.UpdateUnitDtoRequest]),
    __metadata("design:returntype", Promise)
], UnitController.prototype, "updateUnitType", null);
UnitController = __decorate([
    (0, common_1.Controller)('/unit'),
    (0, common_1.UseInterceptors)(common_1.ClassSerializerInterceptor),
    (0, common_1.UsePipes)(new common_1.ValidationPipe({ transform: true })),
    __metadata("design:paramtypes", [create_unit_service_1.CreateUnitService,
        update_unit_service_1.UpdateUnitService])
], UnitController);
exports.UnitController = UnitController;
//# sourceMappingURL=unit.controller.js.map