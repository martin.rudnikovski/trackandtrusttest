import { UpdateUnitDtoRequest } from '../domain/request/update-unit-dto.request';
import { CreateUnitService } from '../service/create-unit.service';
import { CreateUnitDtoRequest } from '../domain/request/create-unit-dto.request';
import { Unit } from '../domain/model/unit.entity';
import { UpdateUnitService } from "../service/update-unit.service";
export declare class UnitController {
    private readonly createUnitService;
    private readonly updateUnitService;
    constructor(createUnitService: CreateUnitService, updateUnitService: UpdateUnitService);
    createNewUnit(createUnitDtoRequest: CreateUnitDtoRequest): Promise<Unit>;
    updateUnitType(unitId: string, updateUnitDto: UpdateUnitDtoRequest): Promise<Unit>;
}
