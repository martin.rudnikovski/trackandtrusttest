"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var UpdateUnitService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUnitService = void 0;
const common_1 = require("@nestjs/common");
const unit_entity_1 = require("../domain/model/unit.entity");
const create_action_event_manager_service_1 = require("../../event/service/action-event/create-action-event-manager.service");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entity_by_id_not_found_error_1 = require("../../shared/errors/entity-by-id-not-found.error");
const object_differences_function_1 = require("../../shared/functions/object-differences.function");
const object_to_string_function_1 = require("../../shared/functions/object-to-string.function");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const unit_type_enum_1 = require("../domain/enum/unit.type.enum");
let UpdateUnitService = UpdateUnitService_1 = class UpdateUnitService {
    constructor(createActionEventManagerService, unitRepository) {
        this.createActionEventManagerService = createActionEventManagerService;
        this.unitRepository = unitRepository;
        this.logger = new common_1.Logger(UpdateUnitService_1.name);
    }
    async updateUnit(unitId, updateUnitDto) {
        const unit = await this.unitRepository.findOneBy({ id: unitId });
        if (!unit) {
            throw new entity_by_id_not_found_error_1.EntityByIdNotFound(unit_entity_1.Unit.name, unitId);
        }
        const changedProperties = (0, object_differences_function_1.objectDifferences)(updateUnitDto, unit);
        if (changedProperties.size == 0 && updateUnitDto.action == undefined)
            return unit;
        if (updateUnitDto.unitType != undefined)
            updateUnitDto.unitType = unit_type_enum_1.UnitTypeUtil.parseString(updateUnitDto.unitType);
        return await this.unitRepository
            .save(Object.assign(Object.assign({}, unit), updateUnitDto))
            .then((updatedUnit) => {
            var _a;
            this.logger.log(`Updated Unit action with ID [${unitId}] with information [${(0, object_to_string_function_1.objectToString)(updateUnitDto)}]`);
            const unitAction = (_a = updateUnitDto.action) !== null && _a !== void 0 ? _a : action_enum_1.Action.UnitDefined;
            this.createActionEventManagerService.createActionEvent({
                meshId: updateUnitDto.meshId,
                latitude: updateUnitDto.location.latitude,
                longitude: updateUnitDto.location.longitude,
                timestamp: new Date(),
                action: unitAction,
                actionType: action_type_enum_1.ActionType.Unit,
                dataset: {
                    unitId: unitId,
                    value: Array.from(changedProperties).reduce((obj, [key, value]) => (Object.assign(obj, { [key]: value })), {}),
                },
            });
            return updatedUnit;
        })
            .catch((err) => {
            this.logger.error(err);
            return err;
        });
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        const unit = await this.unitRepository.findOneBy({ id: dataset.unitId });
        if (!unit) {
            throw new entity_by_id_not_found_error_1.EntityByIdNotFound(unit_entity_1.Unit.name, dataset.unitId);
        }
        return await this.unitRepository
            .save(Object.assign(Object.assign({}, unit), dataset.value))
            .then(async (unit) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId,
                actionType: action_type_enum_1.ActionType.Unit,
                action,
                dataset,
                timestamp,
                latitude,
                longitude,
                signature,
            });
            return unit;
        });
    }
};
UpdateUnitService = UpdateUnitService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(unit_entity_1.Unit)),
    __metadata("design:paramtypes", [create_action_event_manager_service_1.CreateActionEventManagerService,
        typeorm_2.Repository])
], UpdateUnitService);
exports.UpdateUnitService = UpdateUnitService;
//# sourceMappingURL=update-unit.service.js.map