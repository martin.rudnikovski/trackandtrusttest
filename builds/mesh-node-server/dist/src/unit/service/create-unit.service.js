"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CreateUnitService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUnitService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const create_action_event_manager_service_1 = require("../../event/service/action-event/create-action-event-manager.service");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const unit_entity_1 = require("../domain/model/unit.entity");
let CreateUnitService = CreateUnitService_1 = class CreateUnitService {
    constructor(createActionEventManagerService, unitRepository) {
        this.createActionEventManagerService = createActionEventManagerService;
        this.unitRepository = unitRepository;
        this.logger = new common_1.Logger(CreateUnitService_1.name);
    }
    async createUnit(createUnitDto) {
        const existingUnit = await this.unitRepository.findOneBy({
            id: createUnitDto.id,
        });
        if (existingUnit && existingUnit.userId === createUnitDto.userId) {
            return existingUnit;
        }
        const currentTimestamp = new Date();
        return await this.unitRepository.save({
            id: createUnitDto.id,
            userId: createUnitDto.userId,
            meshId: createUnitDto.meshId,
            timeCreated: currentTimestamp,
            timeUpdated: currentTimestamp,
        })
            .then(async (newUnit) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId: createUnitDto.meshId,
                timestamp: currentTimestamp,
                action: action_enum_1.Action.UnitScanned,
                actionType: action_type_enum_1.ActionType.Unit,
                latitude: createUnitDto.location.latitude,
                longitude: createUnitDto.location.longitude,
                dataset: {
                    unitId: newUnit.id,
                    userId: newUnit.userId
                },
            });
            return newUnit;
        })
            .catch((err) => {
            this.logger.error(err);
            return err;
        });
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        return await this.unitRepository.save({
            id: dataset.unitId,
            userId: dataset.userId,
        })
            .then(async (unit) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId,
                actionType: action_type_enum_1.ActionType.Unit,
                action,
                dataset,
                timestamp,
                latitude,
                longitude,
                signature
            });
            return unit;
        });
    }
};
CreateUnitService = CreateUnitService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(unit_entity_1.Unit)),
    __metadata("design:paramtypes", [create_action_event_manager_service_1.CreateActionEventManagerService,
        typeorm_2.Repository])
], CreateUnitService);
exports.CreateUnitService = CreateUnitService;
//# sourceMappingURL=create-unit.service.js.map