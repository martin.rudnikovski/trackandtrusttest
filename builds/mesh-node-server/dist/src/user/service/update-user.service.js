"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var UpdateUserService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../domain/model/user.entity");
const entity_by_id_not_found_error_1 = require("../../shared/errors/entity-by-id-not-found.error");
const object_to_string_function_1 = require("../../shared/functions/object-to-string.function");
const object_differences_function_1 = require("../../shared/functions/object-differences.function");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const create_action_event_manager_service_1 = require("../../event/service/action-event/create-action-event-manager.service");
let UpdateUserService = UpdateUserService_1 = class UpdateUserService {
    constructor(createActionEventManagerService, userRepository) {
        this.createActionEventManagerService = createActionEventManagerService;
        this.userRepository = userRepository;
        this.logger = new common_1.Logger(UpdateUserService_1.name);
    }
    async updateUser(id, meshId, location, updateUserDto) {
        const user = await this.userRepository.findOneBy({ id });
        if (!user) {
            throw new entity_by_id_not_found_error_1.EntityByIdNotFound(user_entity_1.User.name, id);
        }
        const changedProperties = (0, object_differences_function_1.objectDifferences)(updateUserDto, user);
        if (changedProperties.size == 0)
            return user;
        return await this.userRepository
            .save(Object.assign(Object.assign({}, user), updateUserDto))
            .then((updatedUser) => {
            var _a;
            this.logger.log(`Updated User with ID [${id}] with information [${(0, object_to_string_function_1.objectToString)(updateUserDto)}]`);
            const updateAction = (_a = updateUserDto.action) !== null && _a !== void 0 ? _a : action_enum_1.Action.UserInformationUpdated;
            this.createActionEventManagerService.createActionEvent({
                meshId,
                latitude: location.latitude,
                longitude: location.longitude,
                timestamp: new Date(),
                action: updateAction,
                actionType: action_type_enum_1.ActionType.User,
                dataset: {
                    userId: id,
                    value: Array.from(changedProperties).reduce((obj, [key, value]) => (Object.assign(obj, { [key]: value })), {}),
                },
            });
            return updatedUser;
        })
            .catch((err) => {
            this.logger.error(err);
            return err;
        });
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        const user = await this.userRepository.findOneBy({ id: dataset.userId });
        if (!user) {
            throw new entity_by_id_not_found_error_1.EntityByIdNotFound(user_entity_1.User.name, dataset.userId);
        }
        return await this.userRepository
            .save(Object.assign(Object.assign({}, user), dataset.value))
            .then(async (user) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId,
                actionType: action_type_enum_1.ActionType.User,
                action,
                dataset,
                timestamp,
                latitude,
                longitude,
                signature,
            });
            return user;
        });
    }
};
UpdateUserService = UpdateUserService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __metadata("design:paramtypes", [create_action_event_manager_service_1.CreateActionEventManagerService,
        typeorm_2.Repository])
], UpdateUserService);
exports.UpdateUserService = UpdateUserService;
//# sourceMappingURL=update-user.service.js.map