import { Repository } from 'typeorm';
import { User } from '../domain/model/user.entity';
import { CreateUserRequest } from '../domain/request/create-user-dto.request';
import { Action } from '../../event/domain/actions/action.enum';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { CreateActionEventManagerService } from "../../event/service/action-event/create-action-event-manager.service";
export declare class CreateUserService implements Synchronizable<User> {
    private readonly createActionEventManagerService;
    private readonly userRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, userRepository: Repository<User>);
    createUserId: (createUserRequest: CreateUserRequest) => string;
    createUser(createUserDto: CreateUserRequest): Promise<User>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User>;
}
