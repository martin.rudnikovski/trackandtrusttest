"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CreateUserService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../domain/model/user.entity");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const sha256_map_function_1 = require("../../shared/functions/sha256-map.function");
const create_action_event_manager_service_1 = require("../../event/service/action-event/create-action-event-manager.service");
let CreateUserService = CreateUserService_1 = class CreateUserService {
    constructor(createActionEventManagerService, userRepository) {
        this.createActionEventManagerService = createActionEventManagerService;
        this.userRepository = userRepository;
        this.logger = new common_1.Logger(CreateUserService_1.name);
        this.createUserId = (createUserRequest) => (0, sha256_map_function_1.sha256Hash)({
            id: createUserRequest.id,
            phoneNumber: createUserRequest === null || createUserRequest === void 0 ? void 0 : createUserRequest.phoneNumber
        });
    }
    async createUser(createUserDto) {
        const existingUser = await this.userRepository.findOneBy({
            id: createUserDto.id,
        });
        if (existingUser) {
            return existingUser;
        }
        const currentTimestamp = new Date();
        return await this.userRepository.save({
            id: this.createUserId(createUserDto),
            timeCreated: currentTimestamp,
            timeUpdated: currentTimestamp,
        })
            .then(async (newUser) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId: createUserDto.meshId,
                timestamp: currentTimestamp,
                action: action_enum_1.Action.UserCreated,
                actionType: action_type_enum_1.ActionType.User,
                latitude: createUserDto.location.latitude,
                longitude: createUserDto.location.longitude,
                dataset: {
                    userId: newUser.id,
                },
            });
            return newUser;
        })
            .catch((err) => {
            this.logger.error(err);
            return err;
        });
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        return await this.userRepository.save({
            id: dataset.userId,
        })
            .then(async (user) => {
            await this.createActionEventManagerService.createActionEvent({
                meshId,
                actionType: action_type_enum_1.ActionType.User,
                action,
                dataset,
                timestamp,
                latitude,
                longitude,
                signature,
            });
            return user;
        });
    }
};
CreateUserService = CreateUserService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __metadata("design:paramtypes", [create_action_event_manager_service_1.CreateActionEventManagerService,
        typeorm_2.Repository])
], CreateUserService);
exports.CreateUserService = CreateUserService;
//# sourceMappingURL=create-user.service.js.map