import { CreateUserService } from './create-user.service';
import { UpdateUserService } from './update-user.service';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { User } from '../domain/model/user.entity';
import { Dataset } from '../../event/domain/model/dataset.model';
import { Action } from '../../event/domain/actions/action.enum';
export declare class UserSynchronizationService implements Synchronizable<User> {
    private readonly createUserService;
    private readonly updateUserService;
    constructor(createUserService: CreateUserService, updateUserService: UpdateUserService);
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<User>;
}
