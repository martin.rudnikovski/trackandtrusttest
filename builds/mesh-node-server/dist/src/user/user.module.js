"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./domain/model/user.entity");
const event_module_1 = require("../event/event.module");
const users_controller_1 = require("./controller/users.controller");
const update_user_service_1 = require("./service/update-user.service");
const create_user_service_1 = require("./service/create-user.service");
const user_synchronization_service_1 = require("./service/user-synchronization.service");
const services = [
    update_user_service_1.UpdateUserService,
    create_user_service_1.CreateUserService,
    user_synchronization_service_1.UserSynchronizationService,
];
let UserModule = class UserModule {
};
UserModule = __decorate([
    (0, common_1.Module)({
        imports: [
            event_module_1.EventModule,
            typeorm_1.TypeOrmModule.forFeature([
                user_entity_1.User,
            ]),
        ],
        providers: [
            ...services,
        ],
        controllers: [
            users_controller_1.UsersController,
        ],
        exports: [
            user_synchronization_service_1.UserSynchronizationService,
        ],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map