"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidUserActionError = void 0;
const common_1 = require("@nestjs/common");
class InvalidUserActionError extends common_1.BadRequestException {
    constructor(message = 'Invalid user action send.') {
        super(message);
    }
}
exports.InvalidUserActionError = InvalidUserActionError;
//# sourceMappingURL=invalid-user-action.error.js.map