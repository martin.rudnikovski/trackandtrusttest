import { BadRequestException } from "@nestjs/common";
export declare class InvalidUserActionError extends BadRequestException {
    constructor(message?: string);
}
