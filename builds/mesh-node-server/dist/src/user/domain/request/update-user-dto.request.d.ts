import { CreateUserRequest } from './create-user-dto.request';
import { Action } from "../../../event/domain/actions/action.enum";
export declare class UpdateUserRequest extends CreateUserRequest {
    action?: Action;
    id: string;
    timeCreated: Date;
    timeUpdated: Date;
}
