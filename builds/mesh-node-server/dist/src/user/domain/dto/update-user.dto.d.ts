import { Action } from "../../../event/domain/actions/action.enum";
export interface UpdateUserDto {
    role?: string;
    organisation?: string;
    name?: string;
    phoneNumber?: string;
    message?: string;
    action?: Action;
}
