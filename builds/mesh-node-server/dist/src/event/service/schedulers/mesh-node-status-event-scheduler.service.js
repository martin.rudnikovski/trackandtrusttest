"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MeshNodeStatusEventSchedulerService = void 0;
const common_1 = require("@nestjs/common");
const save_mesh_node_status_event_service_1 = require("../mesh-node-status-event/save-mesh-node-status-event.service");
const schedule_1 = require("@nestjs/schedule");
const check_disk_space_1 = require("check-disk-space");
const mesh_node_status_event_entity_1 = require("../../domain/entities/mesh-node-status-event.entity");
const action_type_enum_1 = require("../../domain/actions/action-type.enum");
const fetch_configuration_service_1 = require("../../../configuration/services/fetch-configuration.service");
const configuration_constants_1 = require("../../../shared/constants/configuration.constants");
const wmq_service_1 = require("../wmq.service");
const signing_service_1 = require("../signing.service");
const event_emitter_1 = require("@nestjs/event-emitter");
const event_constants_1 = require("../../../shared/constants/event.constants");
let MeshNodeStatusEventSchedulerService = class MeshNodeStatusEventSchedulerService {
    constructor(saveMeshNodeStatusEventService, configurationFetchService, wmqService, signingService, eventEmitter) {
        this.saveMeshNodeStatusEventService = saveMeshNodeStatusEventService;
        this.configurationFetchService = configurationFetchService;
        this.wmqService = wmqService;
        this.signingService = signingService;
        this.eventEmitter = eventEmitter;
    }
    createMeshNodeStatusEvent() {
        (0, check_disk_space_1.default)('/').then(async (diskSpace) => {
            this.eventEmitter.emit(event_constants_1.EventConstants.STORAGE_CLEAN_UP, diskSpace.size);
            const meshNodeId = await this.configurationFetchService.findConfigurationEntityById(configuration_constants_1.ConfigurationConstants.MESH_NODE_ID);
            const batteryValue = await this.wmqService.getBatteryLife();
            if (meshNodeId) {
                const event = new mesh_node_status_event_entity_1.MeshNodeStatusEvent(new Date(), action_type_enum_1.ActionType.MeshNodeStatus, diskSpace.free, batteryValue, meshNodeId === null || meshNodeId === void 0 ? void 0 : meshNodeId.value);
                event.signature = this.signingService.sign(JSON.stringify(event));
                await this.saveMeshNodeStatusEventService
                    .saveMeshNodeStatusEvent(event)
                    .then((memoryEvent) => this.wmqService.sendSignedActionEvent(memoryEvent));
            }
        });
    }
};
__decorate([
    (0, schedule_1.Cron)(schedule_1.CronExpression.EVERY_6_HOURS),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], MeshNodeStatusEventSchedulerService.prototype, "createMeshNodeStatusEvent", null);
MeshNodeStatusEventSchedulerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [save_mesh_node_status_event_service_1.SaveMeshNodeStatusEventService,
        fetch_configuration_service_1.FetchConfigurationService,
        wmq_service_1.WmqService,
        signing_service_1.SigningService,
        event_emitter_1.EventEmitter2])
], MeshNodeStatusEventSchedulerService);
exports.MeshNodeStatusEventSchedulerService = MeshNodeStatusEventSchedulerService;
//# sourceMappingURL=mesh-node-status-event-scheduler.service.js.map