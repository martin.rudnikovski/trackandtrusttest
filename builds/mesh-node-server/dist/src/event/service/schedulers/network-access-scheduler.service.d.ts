import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class NetworkAccessSchedulerService {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    checkForNetworkAccess(): void;
}
