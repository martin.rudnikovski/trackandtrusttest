import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class WmqReceiveSchedulerService {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    initSynchronization(): void;
}
