import { SaveMeshNodeStatusEventService } from '../mesh-node-status-event/save-mesh-node-status-event.service';
import { FetchConfigurationService } from '../../../configuration/services/fetch-configuration.service';
import { WmqService } from '../wmq.service';
import { SigningService } from '../signing.service';
import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class MeshNodeStatusEventSchedulerService {
    private saveMeshNodeStatusEventService;
    private configurationFetchService;
    private readonly wmqService;
    private readonly signingService;
    private eventEmitter;
    constructor(saveMeshNodeStatusEventService: SaveMeshNodeStatusEventService, configurationFetchService: FetchConfigurationService, wmqService: WmqService, signingService: SigningService, eventEmitter: EventEmitter2);
    createMeshNodeStatusEvent(): void;
}
