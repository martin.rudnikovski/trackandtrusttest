"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var WmqService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WmqService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("@nestjs/axios");
const rxjs_1 = require("rxjs");
const gossip_const_1 = require("../gossip.const");
let WmqService = WmqService_1 = class WmqService {
    constructor(httpService) {
        this.httpService = httpService;
        this.logger = new common_1.Logger(WmqService_1.name);
    }
    fetchEvents() {
        return (0, rxjs_1.firstValueFrom)(this.httpService
            .get(gossip_const_1.GossipConst.BaseUrl + '/capi/v1/wmq/receive')
            .pipe((0, rxjs_1.map)((it) => it.data), (0, rxjs_1.catchError)(() => {
            this.logger.error('Error occurred during fetching events from wmq service.');
            return [[]];
        })));
    }
    sendEvents(events) {
        return (0, rxjs_1.firstValueFrom)(this.httpService
            .post(gossip_const_1.GossipConst.BaseUrl + '/capi/v1/wmq/send', events)
            .pipe((0, rxjs_1.map)((it) => it.data), (0, rxjs_1.catchError)(() => {
            this.logger.error('Error occurred during sending events to wmqService.');
            return (0, rxjs_1.of)([]);
        })));
    }
    sendSignedActionEvent(event) {
        return (0, rxjs_1.firstValueFrom)(this.httpService
            .post(gossip_const_1.GossipConst.BaseUrl + '/capi/v1/wmq/send', [event])
            .pipe((0, rxjs_1.map)((it) => it.data), (0, rxjs_1.catchError)(err => {
            this.logger.error('Error occurred during sending event to wmqService.');
            return (0, rxjs_1.of)([]);
        })));
    }
    sendSignedActionEvents(events) {
        return (0, rxjs_1.firstValueFrom)(this.httpService
            .post(gossip_const_1.GossipConst.BaseUrl + '/capi/v1/wmq/send', events)
            .pipe((0, rxjs_1.map)((it) => it.data), (0, rxjs_1.catchError)(err => {
            this.logger.error('Error occurred during sending event to wmqService.');
            return (0, rxjs_1.of)([]);
        })));
    }
    getBatteryLife() {
        return (0, rxjs_1.firstValueFrom)(this.httpService
            .post(gossip_const_1.GossipConst.BaseUrl + '/capi/v1/battery/get_life')
            .pipe((0, rxjs_1.map)((it) => it.data.value), (0, rxjs_1.catchError)(() => {
            this.logger.error('Error occurred during fetching of battery life info from wmqService');
            return (0, rxjs_1.of)([]);
        })));
    }
};
WmqService = WmqService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [axios_1.HttpService])
], WmqService);
exports.WmqService = WmqService;
//# sourceMappingURL=wmq.service.js.map