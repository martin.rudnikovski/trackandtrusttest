import { Event } from "../domain/dto/event.dto";
import { EventRequest } from "../domain/dto/event.request";
export declare class EventRequestMapper {
    mapEventRequest(event: Event): EventRequest;
    mapEventRequests(events: Event[]): EventRequest[];
}
