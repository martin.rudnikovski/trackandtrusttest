import { DeleteResult, Repository } from 'typeorm';
import { MeshNodeStatusEvent } from '../../domain/entities/mesh-node-status-event.entity';
export declare class RemoveMeshNodeStatusEventService {
    private readonly meshNodeStatusEventRepository;
    constructor(meshNodeStatusEventRepository: Repository<MeshNodeStatusEvent>);
    removeMeshNodeStatusEvents: (ids: string[]) => Promise<DeleteResult>;
    removeMeshNodeStatusEvent: (id: string) => Promise<DeleteResult>;
}
