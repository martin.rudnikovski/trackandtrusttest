/// <reference types="node" />
import { Dataset } from '../domain/model/dataset.model';
export declare class DatasetCompressionService {
    compress(dataset: Dataset): Buffer;
    decompress(blob: Buffer): Dataset;
}
