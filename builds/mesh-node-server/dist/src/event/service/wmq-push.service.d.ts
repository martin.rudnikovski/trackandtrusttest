import { WmqService } from './wmq.service';
import { FetchActionEventService } from './action-event/fetch-action-event.service';
import { FetchDeletionEventService } from './deletion-event/fetch-deletion-event.service';
import { FetchMeshNodeStatusEventService } from './mesh-node-status-event/fetch-mesh-node-status-event.service';
export declare class WmqPushService {
    private wmqService;
    private fetchActionEventService;
    private fetchDeletionEventService;
    private fetchMeshNodeStatusEventService;
    constructor(wmqService: WmqService, fetchActionEventService: FetchActionEventService, fetchDeletionEventService: FetchDeletionEventService, fetchMeshNodeStatusEventService: FetchMeshNodeStatusEventService);
    private readonly logger;
    pushEventMessages(): void;
    private combineEvents;
}
