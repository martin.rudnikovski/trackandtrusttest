import { ActionEvent } from '../../domain/entities/action-event.entity';
import { DeleteResult, Repository } from 'typeorm';
export declare class RemoveActionEventService {
    private readonly actionEventRepository;
    constructor(actionEventRepository: Repository<ActionEvent>);
    removeActionEvents: (actionEventsIds: string[]) => Promise<DeleteResult>;
    removeActionEvent: (id: string) => Promise<DeleteResult>;
}
