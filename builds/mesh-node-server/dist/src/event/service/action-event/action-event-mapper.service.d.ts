import { DatasetCompressionService } from '../dataset-compression.service';
import { ActionEventDTO } from '../../domain/payload/action-event-dto';
import { ActionEvent } from '../../domain/entities/action-event.entity';
export declare class ActionEventMapper {
    private readonly datasetCompressionService;
    constructor(datasetCompressionService: DatasetCompressionService);
    mapActionEvent(payload: ActionEventDTO): ActionEvent;
}
