import { ActionEvent } from '../../domain/entities/action-event.entity';
import { Repository } from 'typeorm';
export declare class FetchActionEventService {
    private readonly actionEventRepository;
    constructor(actionEventRepository: Repository<ActionEvent>);
    fetchActionEvents(): Promise<ActionEvent[]>;
}
