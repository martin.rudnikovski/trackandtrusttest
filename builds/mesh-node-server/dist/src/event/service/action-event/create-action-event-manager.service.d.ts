import { ActionEvent } from '../../domain/entities/action-event.entity';
import { ActionEventDTO } from '../../domain/payload/action-event-dto';
import { FetchDeletionEventService } from '../deletion-event/fetch-deletion-event.service';
import { WmqService } from '../wmq.service';
import { SigningService } from '../signing.service';
import { SaveActionEventService } from './save-action-event.service';
import { ActionEventMapper } from './action-event-mapper.service';
export declare class CreateActionEventManagerService {
    private readonly saveActionEventService;
    private readonly actionEventMapper;
    private readonly signingService;
    private readonly fetchDeletionEventService;
    private readonly wmqService;
    constructor(saveActionEventService: SaveActionEventService, actionEventMapper: ActionEventMapper, signingService: SigningService, fetchDeletionEventService: FetchDeletionEventService, wmqService: WmqService);
    createActionEvent(payload: ActionEventDTO): Promise<ActionEvent | void>;
}
