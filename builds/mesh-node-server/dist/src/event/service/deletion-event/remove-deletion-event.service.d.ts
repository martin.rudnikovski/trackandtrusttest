import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
import { Repository } from 'typeorm';
export declare class RemoveDeletionEventService {
    private readonly deletionEventRepository;
    constructor(deletionEventRepository: Repository<DeletionEvent>);
    removeLastNDeletionEvents(n: number): Promise<DeletionEvent[]>;
    removeLastNPercentageDeletionEvents(percentage: number): Promise<DeletionEvent[]>;
}
