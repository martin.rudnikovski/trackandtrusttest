"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var SaveDeletionEventService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveDeletionEventService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const deletion_event_entity_1 = require("../../domain/entities/deletion-event.entity");
const typeorm_2 = require("typeorm");
let SaveDeletionEventService = SaveDeletionEventService_1 = class SaveDeletionEventService {
    constructor(deletionEventRepository) {
        this.deletionEventRepository = deletionEventRepository;
        this.logger = new common_1.Logger(SaveDeletionEventService_1.name);
        this.saveDeletionEvent = (deletionEvent) => this.deletionEventRepository.save(deletionEvent).then((it) => {
            this.logger.log(`Created Deletion Event: ${JSON.stringify(it)}`);
            return it;
        });
        this.saveAllDeletionEvents = (deletionEvents) => this.deletionEventRepository.save(deletionEvents).then((it) => {
            this.logger.log(`Created Deletion Events: ${JSON.stringify(it)}`);
            return it;
        });
    }
};
SaveDeletionEventService = SaveDeletionEventService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(deletion_event_entity_1.DeletionEvent)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SaveDeletionEventService);
exports.SaveDeletionEventService = SaveDeletionEventService;
//# sourceMappingURL=save-deletion-event.service.js.map