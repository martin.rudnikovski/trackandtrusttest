import { Repository } from 'typeorm';
import { DeletionEvent } from '../../domain/entities/deletion-event.entity';
export declare class FetchDeletionEventService {
    private readonly deletionEventRepository;
    constructor(deletionEventRepository: Repository<DeletionEvent>);
    fetchDeletionEvents(): Promise<DeletionEvent[]>;
    fetchDeletionEventById(id: string): Promise<DeletionEvent | null>;
}
