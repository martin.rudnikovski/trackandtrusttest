"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeletionEventManagerService = void 0;
const common_1 = require("@nestjs/common");
const remove_mesh_node_status_event_service_1 = require("../mesh-node-status-event/remove-mesh-node-status-event.service");
const remove_action_event_service_1 = require("../action-event/remove-action-event.service");
const save_deletion_event_service_1 = require("./save-deletion-event.service");
const action_enum_1 = require("../../domain/actions/action.enum");
let DeletionEventManagerService = class DeletionEventManagerService {
    constructor(removeMeshNodeStatusEventService, removeActionEventService, saveDeletionEventService) {
        this.removeMeshNodeStatusEventService = removeMeshNodeStatusEventService;
        this.removeActionEventService = removeActionEventService;
        this.saveDeletionEventService = saveDeletionEventService;
    }
    async executeAndSaveDeletionEvent(event) {
        if (event.action === action_enum_1.Action.DeleteActionEvent) {
            await this.removeActionEventService.removeActionEvent(event.id);
        }
        else if (event.action === action_enum_1.Action.DeleteMeshNodeStatusEvent) {
            await this.removeMeshNodeStatusEventService.removeMeshNodeStatusEvent(event.id);
        }
        return await this.saveDeletionEventService.saveDeletionEvent(event);
    }
    async executeAndSaveDeletionEvents(events) {
        const deleteActionEventIds = events
            .filter((event) => event.action === action_enum_1.Action.DeleteActionEvent)
            .map((it) => it.id);
        const deleteMemoryEvents = events
            .filter((event) => event.action === action_enum_1.Action.DeleteMeshNodeStatusEvent)
            .map((it) => it.id);
        await this.removeActionEventService.removeActionEvents(deleteActionEventIds);
        await this.removeMeshNodeStatusEventService.removeMeshNodeStatusEvents(deleteMemoryEvents);
        return await this.saveDeletionEventService.saveAllDeletionEvents(events);
    }
};
DeletionEventManagerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [remove_mesh_node_status_event_service_1.RemoveMeshNodeStatusEventService,
        remove_action_event_service_1.RemoveActionEventService,
        save_deletion_event_service_1.SaveDeletionEventService])
], DeletionEventManagerService);
exports.DeletionEventManagerService = DeletionEventManagerService;
//# sourceMappingURL=deletion-event-manager.service.js.map