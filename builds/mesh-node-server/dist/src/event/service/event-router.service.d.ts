import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { DeletionEventManagerService } from './deletion-event/deletion-event-manager.service';
import { WmqService } from './wmq.service';
import { Event } from '../domain/dto/event.dto';
export declare class EventRouterService {
    private configService;
    private httpService;
    private deletionEventManagerService;
    private wmqService;
    private readonly logger;
    constructor(configService: ConfigService, httpService: HttpService, deletionEventManagerService: DeletionEventManagerService, wmqService: WmqService);
    postEvents(events: Event[]): void;
}
