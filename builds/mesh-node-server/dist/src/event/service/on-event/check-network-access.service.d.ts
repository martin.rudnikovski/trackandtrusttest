import { HttpService } from '@nestjs/axios';
import { EventManagementService } from '../event-management.service';
export declare class CheckNetworkAccessService {
    private httpService;
    private eventManagementService;
    constructor(httpService: HttpService, eventManagementService: EventManagementService);
    checkForNetworkAccess(): void;
}
