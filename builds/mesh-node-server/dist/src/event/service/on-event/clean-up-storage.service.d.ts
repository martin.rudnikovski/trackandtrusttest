import { RemoveDeletionEventService } from '../deletion-event/remove-deletion-event.service';
import { FetchDeletionEventService } from '../deletion-event/fetch-deletion-event.service';
export declare class CleanUpStorageService {
    private removeDeletionEventService;
    private fetchDeletionEventService;
    constructor(removeDeletionEventService: RemoveDeletionEventService, fetchDeletionEventService: FetchDeletionEventService);
    cleanUpStorage(diskSize: number): void;
}
