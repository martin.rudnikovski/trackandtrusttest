"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckNetworkAccessService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("@nestjs/axios");
const event_emitter_1 = require("@nestjs/event-emitter");
const event_constants_1 = require("../../../shared/constants/event.constants");
const event_management_service_1 = require("../event-management.service");
let CheckNetworkAccessService = class CheckNetworkAccessService {
    constructor(httpService, eventManagementService) {
        this.httpService = httpService;
        this.eventManagementService = eventManagementService;
    }
    checkForNetworkAccess() {
        this.httpService.get('http://www.google.com').subscribe({
            next: async () => {
                await this.eventManagementService.sendEventsToMainServerInBulk();
            },
            error: () => { },
        });
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(event_constants_1.EventConstants.CHECK_NETWORK_STATUS),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CheckNetworkAccessService.prototype, "checkForNetworkAccess", null);
CheckNetworkAccessService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [axios_1.HttpService,
        event_management_service_1.EventManagementService])
], CheckNetworkAccessService);
exports.CheckNetworkAccessService = CheckNetworkAccessService;
//# sourceMappingURL=check-network-access.service.js.map