import { HttpService } from '@nestjs/axios';
import { Event, NodeToNodeEvent } from '../domain/dto/event.dto';
export declare class WmqService {
    private readonly httpService;
    private readonly logger;
    constructor(httpService: HttpService);
    fetchEvents(): Promise<NodeToNodeEvent[][]>;
    sendEvents(events: NodeToNodeEvent[]): Promise<any>;
    sendSignedActionEvent(event: Event): Promise<any>;
    sendSignedActionEvents(events: Event[]): Promise<any>;
    getBatteryLife(): Promise<number>;
}
