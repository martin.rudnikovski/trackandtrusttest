import { EncryptionService } from '../../encryption/service/encryption.service';
import { FileService } from '../../configuration/services/file.service';
export declare class SigningService {
    private readonly encryptionService;
    private readonly fileService;
    constructor(encryptionService: EncryptionService, fileService: FileService);
    sign(data: string): string;
}
