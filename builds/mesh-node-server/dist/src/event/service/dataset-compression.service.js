"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatasetCompressionService = void 0;
const common_1 = require("@nestjs/common");
const zlib = require("zlib");
let DatasetCompressionService = class DatasetCompressionService {
    compress(dataset) {
        const stringifiedDataset = JSON.stringify(dataset);
        return zlib.deflateSync(stringifiedDataset, {
            level: zlib.constants.Z_BEST_COMPRESSION
        });
    }
    decompress(blob) {
        const decompressedData = zlib.inflateSync(blob);
        return JSON.parse(decompressedData.toString('utf-8'));
    }
};
DatasetCompressionService = __decorate([
    (0, common_1.Injectable)()
], DatasetCompressionService);
exports.DatasetCompressionService = DatasetCompressionService;
//# sourceMappingURL=dataset-compression.service.js.map