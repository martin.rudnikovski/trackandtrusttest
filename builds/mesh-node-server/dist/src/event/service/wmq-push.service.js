"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var WmqPushService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WmqPushService = void 0;
const common_1 = require("@nestjs/common");
const wmq_service_1 = require("./wmq.service");
const fetch_action_event_service_1 = require("./action-event/fetch-action-event.service");
const fetch_deletion_event_service_1 = require("./deletion-event/fetch-deletion-event.service");
const rxjs_1 = require("rxjs");
const fetch_mesh_node_status_event_service_1 = require("./mesh-node-status-event/fetch-mesh-node-status-event.service");
const event_emitter_1 = require("@nestjs/event-emitter");
const event_constants_1 = require("../../shared/constants/event.constants");
let WmqPushService = WmqPushService_1 = class WmqPushService {
    constructor(wmqService, fetchActionEventService, fetchDeletionEventService, fetchMeshNodeStatusEventService) {
        this.wmqService = wmqService;
        this.fetchActionEventService = fetchActionEventService;
        this.fetchDeletionEventService = fetchDeletionEventService;
        this.fetchMeshNodeStatusEventService = fetchMeshNodeStatusEventService;
        this.logger = new common_1.Logger(WmqPushService_1.name);
    }
    pushEventMessages() {
        (0, rxjs_1.forkJoin)([
            this.fetchActionEventService.fetchActionEvents(),
            this.fetchDeletionEventService.fetchDeletionEvents(),
            this.fetchMeshNodeStatusEventService.fetchMeshNodeStatusEvents(),
        ]).subscribe({
            next: ([actionEvents, deletionEvents, memoryEvents]) => {
                const combinedSignedEvents = this.combineEvents(actionEvents, memoryEvents);
                return (0, rxjs_1.forkJoin)([
                    this.wmqService.sendSignedActionEvents(combinedSignedEvents),
                    this.wmqService.sendEvents(deletionEvents)
                ]);
            },
            error: (err) => {
                this.logger.error(err);
            }
        });
    }
    combineEvents(actionEvents, memoryEvents) {
        return actionEvents.concat(memoryEvents);
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(event_constants_1.EventConstants.PUSH_MESSAGES),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], WmqPushService.prototype, "pushEventMessages", null);
WmqPushService = WmqPushService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [wmq_service_1.WmqService,
        fetch_action_event_service_1.FetchActionEventService,
        fetch_deletion_event_service_1.FetchDeletionEventService,
        fetch_mesh_node_status_event_service_1.FetchMeshNodeStatusEventService])
], WmqPushService);
exports.WmqPushService = WmqPushService;
//# sourceMappingURL=wmq-push.service.js.map