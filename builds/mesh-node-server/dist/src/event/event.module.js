"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const action_event_entity_1 = require("./domain/entities/action-event.entity");
const create_action_event_manager_service_1 = require("./service/action-event/create-action-event-manager.service");
const dataset_compression_service_1 = require("./service/dataset-compression.service");
const fetch_action_event_service_1 = require("./service/action-event/fetch-action-event.service");
const action_event_controller_1 = require("./controller/action-event.controller");
const wmq_service_1 = require("./service/wmq.service");
const axios_1 = require("@nestjs/axios");
const schedule_1 = require("@nestjs/schedule");
const wmq_push_service_1 = require("./service/wmq-push.service");
const key_configuration_module_1 = require("../key-configuration/key-configuration.module");
const event_management_service_1 = require("./service/event-management.service");
const config_1 = require("@nestjs/config");
const encryption_module_1 = require("../encryption/encryption.module");
const deletion_event_entity_1 = require("./domain/entities/deletion-event.entity");
const save_deletion_event_service_1 = require("./service/deletion-event/save-deletion-event.service");
const fetch_deletion_event_service_1 = require("./service/deletion-event/fetch-deletion-event.service");
const remove_action_event_service_1 = require("./service/action-event/remove-action-event.service");
const mesh_node_status_event_entity_1 = require("./domain/entities/mesh-node-status-event.entity");
const remove_deletion_event_service_1 = require("./service/deletion-event/remove-deletion-event.service");
const fetch_mesh_node_status_event_service_1 = require("./service/mesh-node-status-event/fetch-mesh-node-status-event.service");
const save_mesh_node_status_event_service_1 = require("./service/mesh-node-status-event/save-mesh-node-status-event.service");
const remove_mesh_node_status_event_service_1 = require("./service/mesh-node-status-event/remove-mesh-node-status-event.service");
const mesh_node_status_event_scheduler_service_1 = require("./service/schedulers/mesh-node-status-event-scheduler.service");
const deletion_event_manager_service_1 = require("./service/deletion-event/deletion-event-manager.service");
const signing_service_1 = require("./service/signing.service");
const save_action_event_service_1 = require("./service/action-event/save-action-event.service");
const action_event_mapper_service_1 = require("./service/action-event/action-event-mapper.service");
const event_router_service_1 = require("./service/event-router.service");
const event_request_mapper_service_1 = require("./service/event-request-mapper.service");
const network_access_scheduler_service_1 = require("./service/schedulers/network-access-scheduler.service");
const check_network_access_service_1 = require("./service/on-event/check-network-access.service");
const clean_up_storage_service_1 = require("./service/on-event/clean-up-storage.service");
const wmq_receive_scheduler_service_1 = require("./service/schedulers/wmq-receive-scheduler.service");
const deletionServices = [
    save_deletion_event_service_1.SaveDeletionEventService,
    fetch_deletion_event_service_1.FetchDeletionEventService,
    remove_deletion_event_service_1.RemoveDeletionEventService,
];
const memoryServices = [
    fetch_mesh_node_status_event_service_1.FetchMeshNodeStatusEventService,
    save_mesh_node_status_event_service_1.SaveMeshNodeStatusEventService,
    remove_mesh_node_status_event_service_1.RemoveMeshNodeStatusEventService,
];
const actionServices = [
    fetch_action_event_service_1.FetchActionEventService,
    create_action_event_manager_service_1.CreateActionEventManagerService,
    action_event_mapper_service_1.ActionEventMapper,
    event_management_service_1.EventManagementService,
    save_action_event_service_1.SaveActionEventService,
];
const services = [
    dataset_compression_service_1.DatasetCompressionService,
    wmq_push_service_1.WmqPushService,
    mesh_node_status_event_scheduler_service_1.MeshNodeStatusEventSchedulerService,
    wmq_service_1.WmqService,
    remove_action_event_service_1.RemoveActionEventService,
    ...deletionServices,
    ...memoryServices,
    ...actionServices,
    deletion_event_manager_service_1.DeletionEventManagerService,
    signing_service_1.SigningService,
    event_router_service_1.EventRouterService,
    event_request_mapper_service_1.EventRequestMapper,
    network_access_scheduler_service_1.NetworkAccessSchedulerService,
    check_network_access_service_1.CheckNetworkAccessService,
    clean_up_storage_service_1.CleanUpStorageService,
    wmq_receive_scheduler_service_1.WmqReceiveSchedulerService,
];
let EventModule = class EventModule {
};
EventModule = __decorate([
    (0, common_1.Module)({
        imports: [
            axios_1.HttpModule,
            config_1.ConfigModule,
            key_configuration_module_1.KeyConfigurationModule,
            typeorm_1.TypeOrmModule.forFeature([action_event_entity_1.ActionEvent, deletion_event_entity_1.DeletionEvent, mesh_node_status_event_entity_1.MeshNodeStatusEvent]),
            schedule_1.ScheduleModule.forRoot(),
            encryption_module_1.EncryptionModule,
        ],
        providers: [...services],
        controllers: [action_event_controller_1.ActionEventController],
        exports: [
            fetch_action_event_service_1.FetchActionEventService,
            dataset_compression_service_1.DatasetCompressionService,
            create_action_event_manager_service_1.CreateActionEventManagerService,
            save_deletion_event_service_1.SaveDeletionEventService,
            save_mesh_node_status_event_service_1.SaveMeshNodeStatusEventService,
            wmq_service_1.WmqService,
            remove_action_event_service_1.RemoveActionEventService,
            remove_mesh_node_status_event_service_1.RemoveMeshNodeStatusEventService,
            deletion_event_manager_service_1.DeletionEventManagerService,
        ],
    })
], EventModule);
exports.EventModule = EventModule;
//# sourceMappingURL=event.module.js.map