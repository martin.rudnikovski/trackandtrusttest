/// <reference types="node" />
import { GpsLocation } from '../../../shared/model/location.class';
import { ActionType } from '../actions/action-type.enum';
import { Action } from '../actions/action.enum';
export declare class ActionEvent {
    id: string;
    meshId: string;
    timestamp: Date;
    location: GpsLocation;
    actionType: ActionType;
    action: Action;
    dataset: Buffer;
    signature: string;
    constructor(meshId: string, timestamp: Date, location: GpsLocation, actionType: ActionType, action: Action, dataset: Buffer, signature?: string);
    private generateId;
}
