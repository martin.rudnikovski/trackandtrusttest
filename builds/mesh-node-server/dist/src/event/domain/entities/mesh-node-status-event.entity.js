"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MeshNodeStatusEvent = void 0;
const typeorm_1 = require("typeorm");
const action_type_enum_1 = require("../actions/action-type.enum");
const crypto = require("crypto");
let MeshNodeStatusEvent = class MeshNodeStatusEvent {
    constructor(timestamp, actionType, memory, battery, meshId, signature) {
        this.id = crypto.randomUUID();
        this.timestamp = timestamp;
        this.actionType = actionType;
        this.memory = memory;
        this.battery = battery;
        this.meshId = meshId;
        if (signature)
            this.signature = signature;
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid', {
        name: 'id',
    }),
    __metadata("design:type", String)
], MeshNodeStatusEvent.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'datetime',
        name: 'timestamp',
    }),
    __metadata("design:type", Date)
], MeshNodeStatusEvent.prototype, "timestamp", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'action_type',
    }),
    __metadata("design:type", String)
], MeshNodeStatusEvent.prototype, "actionType", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'integer',
        name: 'memory',
    }),
    __metadata("design:type", Number)
], MeshNodeStatusEvent.prototype, "memory", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'float',
        name: 'battery',
    }),
    __metadata("design:type", Number)
], MeshNodeStatusEvent.prototype, "battery", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'mesh_id',
    }),
    __metadata("design:type", String)
], MeshNodeStatusEvent.prototype, "meshId", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'text',
        name: 'signature',
    }),
    __metadata("design:type", String)
], MeshNodeStatusEvent.prototype, "signature", void 0);
MeshNodeStatusEvent = __decorate([
    (0, typeorm_1.Entity)('mesh_node_status_events'),
    __metadata("design:paramtypes", [Date, String, Number, Number, String, String])
], MeshNodeStatusEvent);
exports.MeshNodeStatusEvent = MeshNodeStatusEvent;
//# sourceMappingURL=mesh-node-status-event.entity.js.map