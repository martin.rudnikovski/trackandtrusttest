import { ActionEvent } from '../entities/action-event.entity';
import { MeshNodeStatusEvent } from '../entities/mesh-node-status-event.entity';
import { DeletionEvent } from '../entities/deletion-event.entity';
export declare type Event = ActionEvent | MeshNodeStatusEvent;
export declare type NodeToNodeEvent = ActionEvent | DeletionEvent | MeshNodeStatusEvent;
