import { ActionType } from '../actions/action-type.enum';
export declare class CreateEventResponse {
    id: string;
    actionType: ActionType;
}
