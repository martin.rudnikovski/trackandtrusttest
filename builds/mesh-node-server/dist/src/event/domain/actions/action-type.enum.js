"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionType = void 0;
var ActionType;
(function (ActionType) {
    ActionType["Mesh"] = "MESH_ACTION";
    ActionType["User"] = "USER_ACTION";
    ActionType["Unit"] = "UNIT_ACTION";
    ActionType["Photo"] = "PHOTO_ACTION";
    ActionType["Status"] = "STATUS_ACTION";
    ActionType["Delete"] = "DELETE_ACTION";
    ActionType["MeshNodeStatus"] = "MESH_NODE_STATUS_ACTION";
})(ActionType = exports.ActionType || (exports.ActionType = {}));
//# sourceMappingURL=action-type.enum.js.map