import { BaseRequest } from '../../../shared/request/base.request';
export declare class UpdatePhotoRequest extends BaseRequest {
    unitId?: string;
    message?: string;
}
