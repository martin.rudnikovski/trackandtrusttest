"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CreatePhotoService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePhotoService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const photo_entity_1 = require("../domain/model/photo.entity");
const typeorm_2 = require("typeorm");
const create_action_event_manager_service_1 = require("../../event/service/action-event/create-action-event-manager.service");
const action_type_enum_1 = require("../../event/domain/actions/action-type.enum");
const action_enum_1 = require("../../event/domain/actions/action.enum");
let CreatePhotoService = CreatePhotoService_1 = class CreatePhotoService {
    constructor(createActionEventManagerService, photoRepository) {
        this.createActionEventManagerService = createActionEventManagerService;
        this.photoRepository = photoRepository;
        this.logger = new common_1.Logger(CreatePhotoService_1.name);
    }
    createPhoto(params) {
        const newPhoto = new photo_entity_1.Photo(params.photo, params.userId, params.unitId, params.message, params.id);
        return this.photoRepository.save(newPhoto).then(async (photo) => {
            this.logger.log(`Created new Photo instance with ID [${photo.id}] by user [${params.userId}]`);
            await this.createActionEventManagerService.createActionEvent({
                meshId: params.meshId,
                latitude: params.latitude,
                longitude: params.longitude,
                timestamp: new Date(),
                action: action_enum_1.Action.PhotoTaken,
                actionType: action_type_enum_1.ActionType.Photo,
                dataset: {
                    userId: params.userId,
                    unitId: params.unitId,
                    photoId: photo.id,
                    value: {
                        photo: photo.photo,
                        message: params.message,
                    },
                },
                signature: params.signature,
            });
            return photo;
        });
    }
    synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        var _a, _b, _c, _d;
        return this.createPhoto({
            meshId,
            latitude,
            longitude,
            photo: Buffer.from((_a = dataset.value) === null || _a === void 0 ? void 0 : _a.photo.toString()),
            userId: dataset.userId,
            unitId: dataset.unitId,
            id: dataset.photoId,
            message: (_d = (_c = (_b = dataset.value) === null || _b === void 0 ? void 0 : _b.message) === null || _c === void 0 ? void 0 : _c.toString()) !== null && _d !== void 0 ? _d : undefined,
            signature: signature
        });
    }
};
CreatePhotoService = CreatePhotoService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(photo_entity_1.Photo)),
    __metadata("design:paramtypes", [create_action_event_manager_service_1.CreateActionEventManagerService,
        typeorm_2.Repository])
], CreatePhotoService);
exports.CreatePhotoService = CreatePhotoService;
//# sourceMappingURL=create-photo.service.js.map