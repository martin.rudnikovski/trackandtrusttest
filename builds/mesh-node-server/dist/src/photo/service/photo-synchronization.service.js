"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhotoSynchronizationService = void 0;
const common_1 = require("@nestjs/common");
const action_enum_1 = require("../../event/domain/actions/action.enum");
const create_photo_service_1 = require("./create-photo.service");
const invalid_photo_action_error_1 = require("../domain/error/invalid-photo-action.error");
let PhotoSynchronizationService = class PhotoSynchronizationService {
    constructor(createPhotoService) {
        this.createPhotoService = createPhotoService;
    }
    async synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature) {
        switch (action) {
            case action_enum_1.Action.PhotoTaken: {
                return await this.createPhotoService.synchronize(meshId, latitude, longitude, timestamp, action, dataset, signature);
            }
            default:
                throw new invalid_photo_action_error_1.InvalidPhotoActionError(`Invalid photo action specified for: meshID: ${meshId}, Action: ${action}`);
        }
    }
};
PhotoSynchronizationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [create_photo_service_1.CreatePhotoService])
], PhotoSynchronizationService);
exports.PhotoSynchronizationService = PhotoSynchronizationService;
//# sourceMappingURL=photo-synchronization.service.js.map