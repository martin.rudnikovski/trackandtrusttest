/// <reference types="node" />
import { Photo } from '../domain/model/photo.entity';
import { Repository } from 'typeorm';
import { CreateActionEventManagerService } from '../../event/service/action-event/create-action-event-manager.service';
import { Action } from '../../event/domain/actions/action.enum';
import { Synchronizable } from '../../shared/interface/synchronizable.interface';
import { Dataset } from '../../event/domain/model/dataset.model';
export declare class CreatePhotoService implements Synchronizable<Photo> {
    private readonly createActionEventManagerService;
    private readonly photoRepository;
    private readonly logger;
    constructor(createActionEventManagerService: CreateActionEventManagerService, photoRepository: Repository<Photo>);
    createPhoto(params: {
        photo: Buffer;
        userId: string;
        meshId: string;
        latitude: number;
        longitude: number;
        unitId?: string;
        message?: string;
        id?: string;
        signature?: string;
    }): Promise<Photo>;
    synchronize(meshId: string, latitude: number, longitude: number, timestamp: Date, action: Action, dataset: Dataset, signature: string): Promise<Photo>;
}
