/// <reference types="node" />
import { BaseAuditedEntity } from '../../../shared/model/base-audited-entity.class';
export declare class Photo extends BaseAuditedEntity<string> {
    photo: Buffer;
    userId: string;
    unitId?: string;
    message?: string;
    constructor(photo: Buffer, userId: string, unitId?: string, message?: string, id?: string);
}
