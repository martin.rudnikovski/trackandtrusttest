"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sha256Hash = void 0;
const crypto_1 = require("crypto");
function sha256Hash(object) {
    const hash = (0, crypto_1.createHash)('sha256');
    hash.update(JSON.stringify(object));
    return hash.digest('hex');
}
exports.sha256Hash = sha256Hash;
//# sourceMappingURL=sha256-map.function.js.map