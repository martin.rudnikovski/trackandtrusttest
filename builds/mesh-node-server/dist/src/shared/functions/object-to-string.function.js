"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectToString = void 0;
function objectToString(object) {
    return Object.entries(object)
        .map(([key, value]) => `${key}=[${JSON.stringify(value)}]`)
        .join(',');
}
exports.objectToString = objectToString;
//# sourceMappingURL=object-to-string.function.js.map