"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationEvents = void 0;
exports.ApplicationEvents = {
    User: 'event.user',
    Mesh: 'event.mesh',
    All: 'event.*'
};
//# sourceMappingURL=application-events.const.js.map