"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityByIdNotFound = void 0;
const common_1 = require("@nestjs/common");
class EntityByIdNotFound extends common_1.NotFoundException {
    constructor(entity, id) {
        super(`Not found entity ${entity} by ID [${id.toString()}]`);
    }
}
exports.EntityByIdNotFound = EntityByIdNotFound;
//# sourceMappingURL=entity-by-id-not-found.error.js.map