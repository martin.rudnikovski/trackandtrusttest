import { NotFoundException } from '@nestjs/common';
export declare class EntityByIdNotFound extends NotFoundException {
    constructor(entity: string, id: any);
}
