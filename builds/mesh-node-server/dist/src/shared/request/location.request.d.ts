export declare class LocationRequest {
    latitude: number;
    longitude: number;
}
