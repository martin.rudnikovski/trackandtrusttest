import { BaseEntity } from './base-entity.class';
export declare abstract class BaseAuditedEntity<ID> extends BaseEntity<ID> {
    timeUpdated: Date;
    protected constructor(id: ID);
}
