"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Copier = void 0;
class Copier {
    copyWith(modifyObject) {
        return Object.assign(Object.create(this.constructor.prototype), Object.assign(Object.assign({}, this), modifyObject));
    }
}
exports.Copier = Copier;
//# sourceMappingURL=copier.class.js.map