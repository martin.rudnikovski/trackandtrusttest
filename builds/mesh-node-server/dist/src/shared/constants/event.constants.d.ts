export declare const EventConstants: {
    readonly CHECK_NETWORK_STATUS: "network-status";
    readonly STORAGE_CLEAN_UP: "storage-clean-up";
    readonly PULL_MESSAGES: "pull-messages";
    readonly PUSH_MESSAGES: "push-messages";
};
