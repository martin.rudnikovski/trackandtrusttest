"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigurationConstants = void 0;
exports.ConfigurationConstants = {
    MAIN_DATABASE_PUBLIC_KEY: 'MAIN_DATABASE_PUBLIC_KEY',
    MESH_NODE_ID: 'MESH_NODE_ID',
    MESH_NODE_PUBLIC_KEY: 'MESH_NODE_PUBLIC_KEY'
};
//# sourceMappingURL=configuration.constants.js.map