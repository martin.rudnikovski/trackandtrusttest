"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventConstants = void 0;
exports.EventConstants = {
    CHECK_NETWORK_STATUS: 'network-status',
    STORAGE_CLEAN_UP: 'storage-clean-up',
    PULL_MESSAGES: 'pull-messages',
    PUSH_MESSAGES: 'push-messages',
};
//# sourceMappingURL=event.constants.js.map