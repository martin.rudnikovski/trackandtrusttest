export declare const ApplicationConstants: {
    readonly MAIN_SERVER_URL: "MAIN_SERVER_URL";
    readonly CELL_SERVICE_URL: "CELL_SERVICE_URL";
    readonly PASS_PHRASE: "PASS_PHRASE";
    readonly INIT_VECTOR: "INIT_VECTOR";
};
