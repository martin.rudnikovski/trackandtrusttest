export declare const ConfigurationConstants: {
    readonly MAIN_DATABASE_PUBLIC_KEY: "MAIN_DATABASE_PUBLIC_KEY";
    readonly MESH_NODE_ID: "MESH_NODE_ID";
    readonly MESH_NODE_PUBLIC_KEY: "MESH_NODE_PUBLIC_KEY";
};
