"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MeshNodeModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("@nestjs/config");
const user_module_1 = require("./user/user.module");
const event_module_1 = require("./event/event.module");
const event_emitter_1 = require("@nestjs/event-emitter");
const logger_module_1 = require("./logger/logger.module");
const gossip_module_1 = require("./gossip/gossip.module");
const photo_module_1 = require("./photo/photo.module");
const unit_module_1 = require("./unit/unit.module");
const configuration_module_1 = require("./configuration/configuration.module");
const key_module_1 = require("./key/key.module");
const key_configuration_module_1 = require("./key-configuration/key-configuration.module");
const encryption_module_1 = require("./encryption/encryption.module");
let MeshNodeModule = class MeshNodeModule {
};
MeshNodeModule = __decorate([
    (0, common_1.Module)({
        imports: [
            configuration_module_1.ConfigurationModule,
            config_1.ConfigModule.forRoot(),
            typeorm_1.TypeOrmModule.forRootAsync({
                imports: [config_1.ConfigModule],
                inject: [config_1.ConfigService],
                useFactory: (configService) => ({
                    type: 'better-sqlite3',
                    database: configService.get('DATABASE'),
                    autoLoadEntities: true,
                }),
            }),
            event_emitter_1.EventEmitterModule.forRoot({
                wildcard: true,
            }),
            event_module_1.EventModule,
            user_module_1.UserModule,
            unit_module_1.UnitModule,
            logger_module_1.LoggerModule,
            gossip_module_1.GossipModule,
            photo_module_1.PhotoModule,
            configuration_module_1.ConfigurationModule,
            key_module_1.KeyModule,
            key_configuration_module_1.KeyConfigurationModule,
            encryption_module_1.EncryptionModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], MeshNodeModule);
exports.MeshNodeModule = MeshNodeModule;
//# sourceMappingURL=mesh-node.module.js.map