"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var KeyManagementService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeyManagementService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("@nestjs/axios");
const config_1 = require("@nestjs/config");
const application_constants_1 = require("../../shared/constants/application.constants");
const rxjs_1 = require("rxjs");
const save_configuration_service_1 = require("../../configuration/services/save-configuration.service");
const fetch_configuration_service_1 = require("../../configuration/services/fetch-configuration.service");
const configuration_entity_1 = require("../../configuration/model/entity/configuration.entity");
const configuration_constants_1 = require("../../shared/constants/configuration.constants");
const update_configuration_service_1 = require("../../configuration/services/update-configuration.service");
let KeyManagementService = KeyManagementService_1 = class KeyManagementService {
    constructor(configurationSaveService, fetchKeyService, httpService, configService, updateConfigurationService) {
        this.configurationSaveService = configurationSaveService;
        this.fetchKeyService = fetchKeyService;
        this.httpService = httpService;
        this.configService = configService;
        this.updateConfigurationService = updateConfigurationService;
        this.logger = new common_1.Logger(KeyManagementService_1.name);
        this.exchangePublicKeys = async () => {
            const mainServerUrl = this.configService.get(application_constants_1.ApplicationConstants.MAIN_SERVER_URL);
            const meshNodePublicKey = await this.fetchKeyService.findConfigurationEntityById(configuration_constants_1.ConfigurationConstants.MESH_NODE_PUBLIC_KEY);
            const meshNodeId = await this.fetchKeyService.findConfigurationEntityById(configuration_constants_1.ConfigurationConstants.MESH_NODE_ID);
            const meshNodeRequest = {
                id: meshNodeId === null || meshNodeId === void 0 ? void 0 : meshNodeId.value,
                value: meshNodePublicKey === null || meshNodePublicKey === void 0 ? void 0 : meshNodePublicKey.value
            };
            try {
                await (0, rxjs_1.firstValueFrom)(this.httpService.post(`${mainServerUrl}/api/key`, meshNodeRequest)
                    .pipe((0, rxjs_1.map)(axiosResponse => axiosResponse.data), (0, rxjs_1.switchMap)(publicKeyResponse => (0, rxjs_1.forkJoin)([
                    (0, rxjs_1.from)(this.fetchKeyService.findConfigurationEntityById(configuration_constants_1.ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY)),
                    (0, rxjs_1.of)(publicKeyResponse)
                ])), (0, rxjs_1.switchMap)(([configurationEntity, publicKeyResponse]) => {
                    if (!configurationEntity)
                        return this.configurationSaveService.saveConfigurationEntity(new configuration_entity_1.ConfigurationEntity(configuration_constants_1.ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY, publicKeyResponse.publicKey));
                    else
                        return this.updateConfigurationService.updateConfigurationEntity(new configuration_entity_1.ConfigurationEntity(configuration_constants_1.ConfigurationConstants.MAIN_DATABASE_PUBLIC_KEY, publicKeyResponse.publicKey));
                }), (0, rxjs_1.catchError)(() => {
                    this.logger.error('Error occurred during exchangement of keys between mesh-node and main database.');
                    return (0, rxjs_1.of)([]);
                })));
            }
            catch (e) {
                this.logger.error(e);
            }
        };
    }
};
KeyManagementService = KeyManagementService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [save_configuration_service_1.SaveConfigurationService,
        fetch_configuration_service_1.FetchConfigurationService,
        axios_1.HttpService,
        config_1.ConfigService,
        update_configuration_service_1.UpdateConfigurationService])
], KeyManagementService);
exports.KeyManagementService = KeyManagementService;
//# sourceMappingURL=key-management.service.js.map