import { HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
import { SaveConfigurationService } from "../../configuration/services/save-configuration.service";
import { FetchConfigurationService } from "../../configuration/services/fetch-configuration.service";
import { UpdateConfigurationService } from "../../configuration/services/update-configuration.service";
export declare class KeyManagementService {
    private configurationSaveService;
    private fetchKeyService;
    private httpService;
    private configService;
    private updateConfigurationService;
    private readonly logger;
    constructor(configurationSaveService: SaveConfigurationService, fetchKeyService: FetchConfigurationService, httpService: HttpService, configService: ConfigService, updateConfigurationService: UpdateConfigurationService);
    exchangePublicKeys: () => Promise<void>;
}
