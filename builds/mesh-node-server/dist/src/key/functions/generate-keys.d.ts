import { KeyPairInterface } from "../model/interface/key-pair.interface";
export declare function generateKeys(passphrase: string): KeyPairInterface;
