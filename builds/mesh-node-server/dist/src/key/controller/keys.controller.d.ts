import { KeyManagementService } from "../service/key-management.service";
export declare class KeysController {
    private keyManagementService;
    constructor(keyManagementService: KeyManagementService);
    exchangePublicKeysWithExternalServer(): Promise<void>;
}
