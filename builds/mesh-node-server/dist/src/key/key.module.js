"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KeyModule = void 0;
const common_1 = require("@nestjs/common");
const keys_controller_1 = require("./controller/keys.controller");
const key_configuration_module_1 = require("../key-configuration/key-configuration.module");
const controllers = [keys_controller_1.KeysController];
let KeyModule = class KeyModule {
};
KeyModule = __decorate([
    (0, common_1.Module)({
        controllers: [...controllers],
        imports: [key_configuration_module_1.KeyConfigurationModule],
    })
], KeyModule);
exports.KeyModule = KeyModule;
//# sourceMappingURL=key.module.js.map