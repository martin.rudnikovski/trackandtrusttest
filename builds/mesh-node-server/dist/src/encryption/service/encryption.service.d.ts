/// <reference types="node" />
import { ConfigService } from "@nestjs/config";
export declare class EncryptionService {
    private configService;
    private readonly algorithm;
    private readonly initVector;
    private readonly key;
    private readonly passphrase;
    constructor(configService: ConfigService);
    encrypt: (data: string) => string;
    decrypt: (encryptedData: string) => string;
    sign: (data: string, key: string) => string;
    publicEncrypt: (key: string, data: Buffer) => Buffer;
    privateDecrypt: (key: string, data: Buffer) => Buffer;
    privateEncrypt: (key: string, data: Buffer) => Buffer;
}
