"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EncryptionService = void 0;
const common_1 = require("@nestjs/common");
const crypto = require("crypto");
const crypto_1 = require("crypto");
const config_1 = require("@nestjs/config");
const application_constants_1 = require("../../shared/constants/application.constants");
const constants = require("constants");
let EncryptionService = class EncryptionService {
    constructor(configService) {
        this.configService = configService;
        this.algorithm = 'aes-256-cbc';
        this.encrypt = (data) => {
            const cipher = (0, crypto_1.createCipheriv)(this.algorithm, this.key, this.initVector);
            let encryptedData = cipher.update(data, 'utf-8', 'hex');
            encryptedData += cipher.final('hex');
            return encryptedData;
        };
        this.decrypt = (encryptedData) => {
            const decipher = (0, crypto_1.createDecipheriv)(this.algorithm, this.key, this.initVector);
            let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
            decryptedData += decipher.final("utf8");
            return decryptedData;
        };
        this.sign = (data, key) => {
            const sign = crypto.createSign('SHA256');
            sign.write(data);
            sign.end();
            return sign.sign(key, 'base64');
        };
        this.publicEncrypt = (key, data) => crypto.publicEncrypt({
            key: key,
            padding: constants.RSA_NO_PADDING
        }, Buffer.from(data));
        this.privateDecrypt = (key, data) => crypto.privateDecrypt({
            key,
            passphrase: this.passphrase,
            padding: crypto.constants.RSA_PKCS1_PADDING
        }, data);
        this.privateEncrypt = (key, data) => crypto.privateEncrypt({
            key,
            passphrase: this.passphrase,
        }, data);
        this.passphrase = this.configService.get(application_constants_1.ApplicationConstants.PASS_PHRASE);
        this.initVector = Buffer.from(this.configService.get(application_constants_1.ApplicationConstants.INIT_VECTOR), 'base64');
        this.key = (0, crypto_1.createHash)('sha256').update(this.passphrase).digest();
    }
};
EncryptionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], EncryptionService);
exports.EncryptionService = EncryptionService;
//# sourceMappingURL=encryption.service.js.map