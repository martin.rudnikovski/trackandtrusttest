"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InitUser1668594831968 = void 0;
const insertQuery = `
    create table users
    (
        id           text primary key,
        role         text,
        organisation text,
        name         text,
        message      text,
        phone_number text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`;
class InitUser1668594831968 {
    async up(queryRunner) {
        return await queryRunner.query(insertQuery);
    }
    async down(queryRunner) {
        return await queryRunner.dropTable('users', true, true, true);
    }
}
exports.InitUser1668594831968 = InitUser1668594831968;
//# sourceMappingURL=1668594831968-InitUser.js.map