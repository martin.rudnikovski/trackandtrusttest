"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initConfiguration1676923915818 = void 0;
const typeorm_1 = require("typeorm");
class initConfiguration1676923915818 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'configuration',
            columns: [
                {
                    name: 'id',
                    type: 'varchar',
                    isPrimary: true
                },
                {
                    name: 'value',
                    isUnique: true,
                    isNullable: false,
                    type: 'varchar',
                },
                {
                    name: 'time_created',
                    default: 'current_timestamp',
                    isNullable: false,
                    type: 'datetime'
                },
                {
                    name: 'time_updated',
                    default: 'current_timestamp',
                    isNullable: false,
                    type: 'datetime'
                }
            ]
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('configuration');
    }
}
exports.initConfiguration1676923915818 = initConfiguration1676923915818;
//# sourceMappingURL=1676923915818-initConfiguration.js.map