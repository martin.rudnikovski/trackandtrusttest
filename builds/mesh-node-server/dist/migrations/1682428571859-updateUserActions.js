"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserActions1682428571859 = void 0;
class updateUserActions1682428571859 {
    async up(queryRunner) {
        await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into('actions')
            .values([{
                id: 'USER_DEFINE_ROLE',
                action_type: 'USER_ACTION'
            }, {
                id: 'USER_DEFINE_ORGANIZATION',
                action_type: 'USER_ACTION'
            }, {
                id: 'USER_DEFINE_NAME',
                action_type: 'USER_ACTION'
            }, {
                id: 'USER_DEFINE_PHONE_NUMBER',
                action_type: 'USER_ACTION'
            }]).execute();
    }
    async down(queryRunner) {
        await queryRunner.manager
            .createQueryBuilder()
            .delete()
            .from('actions')
            .where("id in ['USER_DEFINE_ORGANIZATION', 'USER_DEFINE_ROLE, USER_DEFINE_NAME, USER_DEFINE_PHONE_NUMBER]")
            .execute();
    }
}
exports.updateUserActions1682428571859 = updateUserActions1682428571859;
//# sourceMappingURL=1682428571859-updateUserActions.js.map