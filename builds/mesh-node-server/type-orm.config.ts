import { config } from 'dotenv';
import { ConfigService } from '@nestjs/config';
import { DataSource } from 'typeorm';
import { InitUser1668594831968 } from './migrations/1668594831968-InitUser';
import { InitEvents1668702116242 } from './migrations/1668702116242-InitEvents';
import { InitPhoto1670422408427 } from './migrations/1670422408427-InitPhoto';
import { InitUnit1670331688939 } from './migrations/1670331688939-InitUnit';
import { initConfiguration1676923915818 } from './migrations/1676923915818-initConfiguration';
import { initDeletionActionEvent1678363836440 } from './migrations/1678363836440-initDeletionActionEvent';
import {initMemoryAvailableEvent1678457677012} from "./migrations/1678457677012-initMemoryAvailableEvent";
import {updateUserActions1682428571859} from "./migrations/1682428571859-updateUserActions";

config();

const configService = new ConfigService();

export default new DataSource({
  type: 'better-sqlite3',
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  database: configService.get<string>('DATABASE')!,
  entities: [],
  migrations: [
    InitUser1668594831968,
    InitEvents1668702116242,
    InitUnit1670331688939,
    InitPhoto1670422408427,
    initConfiguration1676923915818,
    initDeletionActionEvent1678363836440,
    initMemoryAvailableEvent1678457677012,
    updateUserActions1682428571859
  ],
});
