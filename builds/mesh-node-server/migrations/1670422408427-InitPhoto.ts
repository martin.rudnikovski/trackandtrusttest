import { MigrationInterface, QueryRunner } from "typeorm"
import { SqlReader } from 'node-sql-reader';

const sqlQuery = `
    create table photos
    (
        id           text primary key,
        photo        blob                               not null,
        user_id      text                               not null,
        unit_id      text,
        message      text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`;

export class InitPhoto1670422408427 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const queries = SqlReader.parseSqlString(sqlQuery);

        for (const query of queries) {
            await queryRunner.query(query);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
