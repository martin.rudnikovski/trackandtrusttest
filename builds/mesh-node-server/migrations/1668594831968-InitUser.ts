import { MigrationInterface, QueryRunner } from "typeorm"

const insertQuery = `
    create table users
    (
        id           text primary key,
        role         text,
        organisation text,
        name         text,
        message      text,
        phone_number text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`

export class InitUser1668594831968 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        return await queryRunner.query(insertQuery)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        return await queryRunner.dropTable('users', true, true, true)
    }

}
