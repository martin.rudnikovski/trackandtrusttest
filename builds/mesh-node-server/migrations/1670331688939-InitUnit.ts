import {MigrationInterface, QueryRunner} from 'typeorm'
import {SqlReader} from 'node-sql-reader';

const insertQuery = `
    create table unit_types
    (
        id text primary key
    );

    insert into unit_types(id)
    values ('PALLET'),
           ('CONTAINER'),
           ('OTHER');

    create table units
    (
        id           text primary key,
        user_id      text,
        mesh_id      text,
        unit_type    text
            constraint units_unit_type_id_fk
                references unit_types
                on update cascade
                on delete restrict,
        message      text,
        seal_number  text,
        time_created datetime default current_timestamp not null,
        time_updated datetime default current_timestamp not null
    );
`

const removeQuery = `
    drop table unit_types;
    drop table units;
`

export class InitUnit1670331688939 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const queries = SqlReader.parseSqlString(insertQuery)

        for (const query of queries) {
            await queryRunner.query(query);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        return await queryRunner.query(removeQuery)
    }

}
