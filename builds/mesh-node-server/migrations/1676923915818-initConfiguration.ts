import {MigrationInterface, QueryRunner, Table} from "typeorm"

export class initConfiguration1676923915818 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'configuration',
            columns: [
                {
                    name: 'id',
                    type: 'varchar',
                    isPrimary: true
                },
                {
                    name: 'value',
                    isUnique: true,
                    isNullable: false,
                    type: 'varchar',
                },
                {
                    name: 'time_created',
                    default: 'current_timestamp',
                    isNullable: false,
                    type: 'datetime'
                },
                {
                    name: 'time_updated',
                    default: 'current_timestamp',
                    isNullable: false,
                    type: 'datetime'
                }
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('configuration')
    }

}
