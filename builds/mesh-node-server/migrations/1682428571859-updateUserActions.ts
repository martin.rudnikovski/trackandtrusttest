import {MigrationInterface, QueryRunner} from "typeorm"

export class updateUserActions1682428571859 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into('actions')
            .values([{
                    id: 'USER_DEFINE_ROLE',
                    action_type: 'USER_ACTION'
                }, {
                    id: 'USER_DEFINE_ORGANIZATION',
                    action_type: 'USER_ACTION'
                }, {
                    id: 'USER_DEFINE_NAME',
                    action_type: 'USER_ACTION'
                }, {
                    id: 'USER_DEFINE_PHONE_NUMBER',
                    action_type: 'USER_ACTION'
                }]
            ).execute()

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.manager
            .createQueryBuilder()
            .delete()
            .from('actions')
            .where("id in ['USER_DEFINE_ORGANIZATION', 'USER_DEFINE_ROLE, USER_DEFINE_NAME, USER_DEFINE_PHONE_NUMBER]")
            .execute()
    }

}
