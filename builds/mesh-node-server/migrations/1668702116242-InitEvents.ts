import { MigrationInterface, QueryRunner } from 'typeorm';
import { SqlReader } from 'node-sql-reader';

const sqlQuery = `
    create table action_types
    (
        id text primary key
    );

    insert into action_types(id)
    values ('MESH_ACTION'),
           ('USER_ACTION'),
           ('UNIT_ACTION'),
           ('PHOTO_ACTION'),
           ('DELETE_ACTION'),
           ('MESH_NODE_STATUS_ACTION');


    create table actions
    (
        id          text primary key,
        action_type text
            constraint actions_action_type_fk
                references action_types
                on update cascade
                on delete restrict
    );

    insert into actions(id, action_type)
    values ('MESH_NODE_ACCESS', 'MESH_ACTION'),
           ('USER_CREATED', 'USER_ACTION'),
           ('USER_INFORMATION_UPDATED', 'USER_ACTION'),
           ('USER_STATUS_UPDATE', 'USER_ACTION'),
           ('USER_MESSAGE_UPDATE', 'USER_ACTION'),
           ('UNIT_STATUS_UPDATE', 'UNIT_ACTION'),
           ('UNIT_SCANNED', 'UNIT_ACTION'),
           ('UNIT_DEFINED_CONTAINER_SEAL','UNIT_ACTION'),
           ('UNIT_DEFINED', 'UNIT_ACTION'),
           ('UNIT_PICKED_UP', 'UNIT_ACTION'),
           ('UNIT_PICKED_UP_MESSAGE', 'UNIT_ACTION'),
           ('UNIT_DROPPED_OFF', 'UNIT_ACTION'),
           ('UNIT_DROPPED_OFF_MESSAGE', 'UNIT_ACTION'),
           ('UNIT_MESSAGE_UPDATE', 'UNIT_ACTION'),
           ('UNIT_PICKUP_MESSAGE', 'UNIT_ACTION'),
           ('PHOTO_TAKEN', 'PHOTO_ACTION'),
           ('PHOTO_MESSAGE', 'PHOTO_ACTION'),
           ('PHOTO_UNIT_SET', 'PHOTO_ACTION'),
           ('DELETE_ACTION_EVENT', 'DELETE_ACTION'),
           ('DELETE_MESH_NODE_STATUS_EVENT', 'DELETE_ACTION');


    create table action_events
    (
        id          text,
        mesh_id     text,
        latitude    real,
        longitude   real,
        timestamp   datetime,
        action_type text
            constraint action_event_action_type_id_fk
                references action_types
                on update cascade,
        action      text
            constraint actions_events_action_event_type_id_fk
                references actions
                on update cascade,
        dataset     blob,
        signature   text not null ,
        constraint user_action_events_pk
            primary key (id)
    );
`;

export class InitEvents1668702116242 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const queries = SqlReader.parseSqlString(sqlQuery);

    for (const query of queries) {
      await queryRunner.query(query);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async down(queryRunner: QueryRunner): Promise<void> {
  }
}
