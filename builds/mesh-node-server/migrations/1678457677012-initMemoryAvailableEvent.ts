import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class initMemoryAvailableEvent1678457677012
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'mesh_node_status_events',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            default: 'gen_random_uuid()',
            isPrimary: true,
          },
          {
            name: 'timestamp',
            type: 'datetime',
          },
          {
            name: 'action_type',
            type: 'text',
            foreignKeyConstraintName: 'mesh_node_status_event_action_type_fk',
          },
          {
            name: 'memory',
            type: 'integer',
          },
          {
            name: 'battery',
            type: 'float',
          },
          {
            name: 'mesh_id',
            type: 'text',
          },
          {
            name: 'signature',
            type: 'text',
            isNullable: false,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'mesh_node_status_events',
      new TableForeignKey({
        columnNames: ['action_type'],
        referencedColumnNames: ['id'],
        referencedTableName: 'action_types',
        onUpdate: 'CASCADE',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('mesh_node_status_events');
  }
}
