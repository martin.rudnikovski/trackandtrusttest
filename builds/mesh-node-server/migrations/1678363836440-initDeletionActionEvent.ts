import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class initDeletionActionEvent1678363836440
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'deletion_events',
        columns: [
          {
            name: 'id',
            type: 'text',
            isPrimary: true,
          },
          {
            name: 'timestamp',
            type: 'datetime',
          },
          {
            name: 'action_type',
            type: 'text',
            foreignKeyConstraintName: 'deletion_action_event_action_type_fk',
          },
          {
            name: 'action',
            type: 'text',
            foreignKeyConstraintName: 'deletion_action_event_action_fk',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'deletion_events',
      new TableForeignKey({
        columnNames: ['action_type'],
        referencedColumnNames: ['id'],
        referencedTableName: 'action_types',
        onUpdate: 'CASCADE',
      }),
    );

    await queryRunner.createForeignKey(
      'deletion_events',
      new TableForeignKey({
        columnNames: ['action'],
        referencedColumnNames: ['id'],
        referencedTableName: 'actions',
        onUpdate: 'CASCADE',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('deletion_events')
  }
}
