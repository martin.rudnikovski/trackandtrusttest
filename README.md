# CD pipeline for Track and Trust

In order to setup the CD pipeline for your Pis, you first neet to flash your microSD card with the Raspbian Lite OS 64bit and setup the ssh configuration (reccomended to use the Raspberry Pi imager).

Check the ssh configuration of your Pi by running the following command:
```
ssh pi@raspberrypi.local
```
Next you need to clone this repository and change the inventory file for whatever ssh configuration you've set on your pi.

```
git clone https://gitlab.com/martin.rudnikovski/trackandtrusttest.git
cd trackandtrusttest
nano inventory
```
Now you're ready to run the playbook with the following command (make sure you're running it inside of trackandtrusttest/)

```
ansible-playbook -i inventory first_config.yaml 
```

This command runs the _first_config.yaml_ playbook using the inventory file. This command should take a while!

To check if the PWA is working, ssh into the pi and check for the IP address
```
ssh pi@raspberrypi.local
ip a
```
Take the IP address and using your browser look up http:<pis_ip_address>:4173

If you're having trouble connecting to the pi, ssh into the pi and manually run the ansible-pull command:
```

```